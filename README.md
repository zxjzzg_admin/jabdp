
官网地址： [http://jabdp.7yxx.com](http://jabdp.7yxx.com)

官方交流QQ群：801507856
# 简介
- 简搭（jabdp）低代码平台，是一款引擎模式的web快速开发平台，采用新颖的在线开发部署模式，配置式，低代码，使开发变得更加简单、纯粹，提高了开发的效率。

# 目录结构
- ae是基于activiti explorer的流程设计器。
- jabdp-designer是表单、业务设计器。
- jabdp-jwp是业务模型的数据结构。
- jabdp-platform是应用端。

# 开发环境
- 语言：Java 8
- IDE： IDEA / Eclipse
- 依赖管理：Maven
- 数据库脚本：MySQL5.7+ / Sqlserver2008+
- 服务器：tomcat 8.0+ 

# 部署步骤
- 1. 新建一个空的数据库，修改jabdp-platform工程目录WEB-INF\classes\application.properties文件：
     修改jdbc.url、jdbc.username、jdbc.password修改为新建数据库的相关属性。
     ![输入图片说明](%E5%9B%BE%E7%89%871.png)
- 2. maven install部署jabdp-jwp到本地repository；
- 3. maven package打包jabdp-designer为iDesigner.war；
- 4. maven package打包jabdp-platform为iPlatform.war；
- 5. 将sevenzipjbinding-9.20-2.00beta.jar和sevenzipjbinding-all-platforms-9.20-2.00beta.jar复制到tomcat/lib目录下；
- 6. 将ae、iDesigner.war、iPlatform.war解压后放到tomcat8.0下webapps目录。
- 7. 删除 webapps\iPlatform\upload目录下的所有文件。
- 8. 执行 webapps\iPlatform工程目录WEB-INF\init-db.bat文件，即可进行数据库初始化（包括建表及数据初始化）。
- 9. 启动tomcat。
- 10. 启动成功后，打开浏览器，输入127.0.0.1:9090/iDesigner（设计器）和127.0.0.1:9090/iPlatform（业务系统）分别浏览即可。

# Tips
- 简搭（jabdp）分为开源版和商业版。商业版支持一键部署，不需要安装上述开发环境提到的众多工具，也不需要上述繁琐的部署步骤。商用版只需官网下载解压后双击启动服务即可一键部署。