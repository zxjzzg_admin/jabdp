<#--列表和查询页面字段显示值格式 -->
<#macro getFieldView field  prefix=""  appendCommaFirst=false>
	<#if field.key?? && field.key!="">	
							<#assign editType = field.editProperties.editType/>
							<#if editType=="ComboBox" || editType=="ComboBoxSearch" >
							         <text class = "font-24" >{{item.${field.key}_caption}}</text>

						    <#else>
						             <text class = "font-24" >{{item.${field.key}}}</text>
							</#if>							
	</#if>					
</#macro>

<#-- 定义字段显示值 -->
<#macro initFieldCaption  field prefix="" appendCommaFirst=false>
  <#if field.key??&&field.key!="">
          <#assign  fieldKey=prefix + field.key/>
          <#assign editType=field.editProperties.editType/>
          <#if editType=="ComboBox" || editType=="ComboBoxSearch" >
                     if(typeof(this.viewItem.${field.key}_caption)=='undefined')  this.viewItem.${field.key}_caption='';          
          </#if>          
  </#if>
</#macro>

<#--表单校验属性-->
<#macro  validate field  >
	<#if field.dataProperties.notNull==true>
		:name="root['${field.i18nKey!field.caption}']"  v-validate="'required'" :class="{'input': true, 'is-danger': errors.has(root['${field.i18nKey!field.caption}']) }"
	</#if>
</#macro>

<#--添加必填的星号标识-->
<#macro addStar field>
							          <#if field.dataProperties.notNull==true>
									  	<text  class="font-24 red"  slot="label" >*</text>
									 <#else>
									    <text  class="font-24 light" slot="label" >*</text>
									  </#if>									  
</#macro>

<#--只读字段-->
<#macro  getRealOnlyFieldCell field>
		<wxc-cell >
						<label  slot="label"></label>
						<text   slot="title" >{{root['${field.i18nKey!field.caption}']}}</text>
						<div slot="value"  class = "edit-field" ><@getFieldView field = field /></div>
		</wxc-cell>
</#macro>

<#macro getEditPageFieldCell  form  field   prefix="" entityName=""  appendCommaFirst=false>
		<#if  field.enableFieldAuthCheck==true><#--开启权限检测，系统管理里的权限配置优先-->	
			<#--可见-->
			<div v-has="'field_${entityName}.${field.key}.visible'">
				<#--只读，v-has相当于v-if-->
				<div v-has="'field_${entityName}.${field.key}.readonly'">
					<@getRealOnlyFieldCell  field = field />
				</div>
				<#--非只读，v-remove相当于v-else-->
				<div v-remove="'field_${entityName}.${field.key}.readonly'">
					<@getFieldEdit form=form field=field  prefix=prefix  entityName=entityName/>
				</div>
			</div>
		<#elseif field.editProperties.visible==true><#--未开启权限检测，使用设计器配置的只读可见-->
			<#if  field.enableFieldAuthCheck==true>
				<div>
					<@getRealOnlyFieldCell field=field />
				</div>
			<#else>
				<div>
					<@getFieldEdit form=form field=field  prefix=prefix  entityName=entityName/>
				</div>
			</#if>	
		</#if>
</#macro>



<#macro initDataSource form  field type="query" prefix="" entityName="" formKey="" moduleTitle="" isAsync="false">
<#if field.key?? && field.key!="">
								<#assign fieldKey = prefix + field.key/>
								<#assign editType = field.editProperties.editType/>
								<#if field.editProperties.formula?? && field.editProperties.formula.type??>
					            <#if field.editProperties.formula.type==1> <#-- 固定值 -->
					                 <#assign itemList=field.editProperties.formula.itemList/>					         
				                     <#if itemList??&&itemList?size gt 0>
							         this.${form.lcKey}s_${fieldKey}DataSource =
							         [<#list itemList as item>
							          {"key":'${item.key}',"caption":'${item.caption}'} <#if item_has_next> ,</#if>
						             </#list> ];
						             </#if>
								<#elseif field.editProperties.formula.type==5> <#-- 模块 --> 
									 this.sendRequest({
										 url:'/gs/gs-mng!queryAjaxResult.action',
										 data:{
											 entityName: '${form.entityName}.${field.key}.module',
											 style: 'module',
											 rows: 20,
											 esBoxType: 'ComboBox'
										 },
										 success:data=>{
											 if(data&&data.length>0){
                                              	this.${form.lcKey}s_${fieldKey}DataSource = data;
											 }
										 }
									 });
								<#elseif field.editProperties.formula.type==2 &&  field.editProperties.formula.refKey?? && field.editProperties.formula.refKey!='' > <#-- sql --> 
									 this.sendRequest({
										 url:'/gs/gs-mng!queryAjaxResult.action',
										 data:{
											 entityName: '${form.entityName}.${field.key}.${field.editProperties.formula.refKey}',
											 style: 'sql',
											 rows: 20,
											 esBoxType: 'ComboBox'
										 },
										 success:data=>{
											 if(data&&data.length>0){
                                              	this.${form.lcKey}s_${fieldKey}DataSource = data;
											 }
										 }
									 });	 
						        </#if>
						        </#if>
</#if>
</#macro>


<#--编辑页面字段处理-->
<#macro getFieldEdit form   field   prefix="" entityName=""  appendCommaFirst=false>	
							<#assign fieldKey = prefix + field.key/>
							<#assign dataProperties = field.dataProperties/>
							<#assign editType = field.editProperties.editType/>
							<#assign dataType = field.dataProperties.dataType/>
																		
								<#if editType =="TextBox" >
										<#if dataType =="dtLong"> 
												<qx-cell  
												     :has-vertical-indent="false"
												    :title="root['${field.i18nKey!field.caption}']">	
													<@addStar  field=field/>											 							
													<wxc-stepper  slot="value"   default-value="0"  ref="${field.key}"  @wxcStepperValueChanged="stepperValueChange('${field.key}')"></wxc-stepper>
												</qx-cell >
										<#else >
												<qx-cell  
												     :has-vertical-indent="false"
												    :title="root['${field.i18nKey!field.caption}']">	
													<@addStar  field=field/>									     
													<input  slot="value"   class="input"  placeholder ="请输入"  type="text"  v-model="${prefix}item.${field.key}"/>
												</qx-cell >				                 
										</#if>
								<#elseif editType=="RichTextBox">
								            <div class="light-bg  padding-24 width border-bottom" >
												<div class="row justify-start">																
													<@addStar  field=field/>	
													<text class="font-30" >{{root['${field.i18nKey!field.caption}']}}</text>				                        
												</div>
												<div class="padding-top padding-bootom">
													<textarea      placeholder ="请输入"   v-model="${prefix}item.${field.key}" >	 </textarea>		           
												</div>
											</div>  	
								<#elseif editType=="ComboBox"  >
											<qx-cell   
											        :has-vertical-indent="true"
													  @wxcCellClicked="pickData({formlcName:'${form.lcKey}s',index:index,fieldKey:'${field.key}',curKey:${prefix}item.${field.key}})" 
											          :title="root['${field.i18nKey!field.caption}']"
													  :has-arrow="true"
													  placeholder="请选择"
													  :caption="${prefix}item.${field.key}_caption">	
													  <@addStar  field=field/>					 				
											</qx-cell >		
								<#elseif  editType=="ComboBoxSearch" >
										<#if field.editProperties.multiple==false>
													<qx-cell  
													  :has-vertical-indent="false"
													@wxcCellClicked="listSelectClick({formlcName:'${form.lcKey}s',index:index,fieldKey:'${field.key}'})" 
													:title="root['${field.i18nKey!field.caption}']" 
													:has-arrow="true">
													<@addStar  field=field/>
													<input slot="value" class="input" placeholder ="请选择"     v-model="${prefix}item.${field.key}_caption"/>
													</qx-cell >
											<#elseif  field.editProperties.multiple==true > 
													<qx-cell 
													 :has-vertical-indent="false"
													@wxcCellClicked="gridSelectClick({formlcName:'${form.lcKey}s',index:index,fieldKey:'${field.key}'})" 
													:title="root['${field.i18nKey!field.caption}']"
													:has-arrow="true">
													<@addStar  field=field/>
													<input placeholder ="请选择"  style = "flex:1"   v-model="${prefix}item.${field.key}_caption"/>
												</qx-cell >				        
											</#if>
								<#elseif editType=="DateBox">
														<qx-cell  
														 :has-vertical-indent="true"
														@wxcCellClicked="pickDate({formlcName:'${form.lcKey}s',index:index,fieldKey:'${field.key}'})" 
														:title="root['${field.i18nKey!field.caption}']"
														:has-arrow="true"
														placeholder="选择时间"
														:caption="${prefix}item.${field.key}">	
														<@addStar  field=field/>					 
														</qx-cell >																				 
								</#if>								
</#macro>

<#--筛选页面字段处理-->
<#macro getFilterFieldEdit form field   prefix=""  appendCommaFirst=false>
	<#if field.key?? && field.key!="">	
							<#assign fieldKey = prefix + field.key/>
							<#assign dataProperties = field.dataProperties>
							<#assign editType = field.editProperties.editType/>
							<#assign dataType = field.dataProperties.dataType/>
							
							        
							<#if editType =="TextBox">
							          <#if dataType =="dtLong"> 
									            <qx-cell  
												     :has-vertical-indent="false"
												    :title="root['${field.i18nKey!field.caption}']">											 							
													<wxc-stepper  v-model="${prefix}item.filter_EQL_${field.key}"  default-value="0"  ref="filter_EQL_${field.key}"  @wxcStepperValueChanged="stepperValueChange('filter_EQL_${field.key}')">
													</wxc-stepper>
												</qx-cell >
							          <#else>		
									              <qx-cell  
												     :has-vertical-indent="false"
												     :title="root['${field.i18nKey!field.caption}']">									     
													<input  slot="value"   class="input"  placeholder ="请输入"  type="text"  v-model="${prefix}item.filter_LIKES_${field.key}"/>
												  </qx-cell>
			                 
							          </#if>
							 <#elseif editType=="RichTextBox" > 
							            <div class="light-bg  padding-24 width border-bottom" >
												<div class="row justify-start">																											
													<text class="font-30" >{{root['${field.i18nKey!field.caption}']}}</text>				                        
												</div>
												<div class="padding-top padding-bootom">
													<textarea      placeholder ="请输入"   v-model="${prefix}item.filter_LIKES_${field.key}" >	 </textarea>		           
												</div>
										</div>  
			 		     
							 <#elseif editType=="ComboBox" || editType=="ComboBoxSearch" >
							                <qx-cell   
											        :has-vertical-indent="true"
													  @wxcCellClicked="pickData({formlcName:'${form.lcKey}s',index:index,fieldKey:'filter_EQS_${field.key}'})" 
											          :title="root['${field.i18nKey!field.caption}']"
													  :has-arrow="true"
													  placeholder="请选择"
													  :caption="${prefix}item.filter_EQS_${field.key}_caption">					 				
											</qx-cell >	

								        
							<#elseif editType=="DateBox">
													<qx-cell  
														 :has-vertical-indent="true"
														@wxcCellClicked="pickDate({formlcName:'${form.lcKey}s',index:index,fieldKey:'filter_GED_${field.key}'})" 
														:title="root['${field.i18nKey!field.caption}']"
														:has-arrow="true"
														placeholder="选择时间"
														:caption="${prefix}item.filter_GED_${field.key}">						 
													</qx-cell >	
                                                     <qx-cell  
														 :has-vertical-indent="true"
														@wxcCellClicked="pickDate({formlcName:'${form.lcKey}s',index:index,fieldKey:'filter_LED_${field.key}'})" 
														title="至"
														:has-arrow="true"
														placeholder="选择时间"
														:caption="${prefix}item.filter_LED_${field.key}">						 
													</qx-cell>																			 
							</#if>												
	</#if>					
</#macro>


<#--筛选页面字段处理-->
<#macro getInitFilterField  form  prefix=""  appendCommaFirst=false>
    ,item:{
		filter_INS_status:null
		<#list form.fieldList as field>
		<#if field.key?? && field.key!="">	
			<#assign fieldKey = prefix + field.key/>
			<#assign dataProperties = field.dataProperties>
			<#assign editType = field.editProperties.editType/>
			<#assign dataType = field.dataProperties.dataType/>										
				<#if editType =="TextBox">
					<#if dataType =="dtLong"> 												 
						,'filter_EQL_${field.key}':''			     														                 
					</#if>
				<#elseif editType=="RichTextBox"> 										 
					,'filter_LIKES_${field.key}':''			 		     
				<#elseif editType=="ComboBox" || editType=="ComboBoxSearch" >
					<#if dataProperties.dataType="dtString">
						,'filter_EQS_${field.key}':''
						,'filter_EQS_${field.key}_caption':''
					<#elseif dataProperties.dataType="dtLong">
						,'filter_EQL_${field.key}':''
						,'filter_EQL_${field.key}_caption':''
					</#if>
				<#elseif editType=="DateBox">						 
					,'filter_GED_${field.key}':''
					,'filter_LED_${field.key}':''																	 
				</#if>						
		</#if>	
		</#list>
	}				
</#macro>
