﻿<#import "common/app-field.ftl" as fieldftl> 
<#assign entityName=root.entityName />  
<#assign formList=root.dataSource.formList/>
<#assign titleName=root.moduleProperties.caption/>
<#assign i18nKey=root.moduleProperties.i18nKey/>
<#assign moduleKey=root.moduleProperties.key/>

<#-- 判断是否有流程 -->
<#assign flowList= root.flowList />
<#assign isSelfApprove=true />
<#if flowList??&&flowList?size gt 0>
<#assign isSelfApprove=false />
</#if>



<template>
       <div  class="wrapper" ref="view-page" :style="cityExtendStyle" >
        <wxc-minibar  v-if="item" :title="'查看'+root['${i18nKey!titleName}']+'-'+this.item.id"
                       background-color="white"
                       right-text="...."
                       @wxcMinibarLeftButtonClicked="wxcMinibarLeftButtonClicked"
                       @wxcMinibarRightButtonClicked="wxcMinibarRightButtonClicked"
                       :use-default-return="false"></wxc-minibar>

        
        <scroller  v-if="item"   class = "scroll-container">
        <#list formList as form>  
        	<#if form.isMaster || form.listType == "form">    
              <div  class="form-title"><text class="font-28">{{root['${form.i18nKey!form.caption}']}}</text></div> 
              <div  class="light-bg padding-top">  
		      <#list form.fieldList as field>
                 <#if field.key?? && field.key!="">	
                        <qx-cell 
                         :has-margin="true"
                         :has-bottom-border="false"
                         :has-vertical-indent="false"
                         :label="root['${field.i18nKey!field.caption}']"
						<#assign editType = field.editProperties.editType/>
						<#if editType=="ComboBox" || editType=="ComboBoxSearch" >
                            :title="item.${field.key}_caption">
						<#else>
						    :title="item.${field.key}">
						</#if>							
                        </qx-cell>
                </#if>
		      </#list>
              </div>
              <#else><#--子表-->
						<div >
							<div v-for="(item,index) in  ${form.lcKey}s"  :key="index">
                             <div  class="form-title"><text  class="font-28">{{root['${form.i18nKey!form.caption}']+'-'+(index+1)}}</text></div>
                             <div class= "light-bg">
								<#list form.fieldList as field>
									<#if field.key?? && field.key!="">	
                                            <qx-cell 
                                            :has-margin="true"
                                            :has-bottom-border="false"
                                            :has-vertical-indent="false"
                                            :label="root['${field.i18nKey!field.caption}']"
                                            <#assign editType = field.editProperties.editType/>
                                            <#if editType=="ComboBox" || editType=="ComboBoxSearch" >
                                                :title="item.${field.key}_caption">
                                            <#else>
                                                :title="item.${field.key}">
                                            </#if>							
                                            </qx-cell>
                                    </#if>
								</#list>
                             </div>
							</div>
						</div>
		      </#if>           
      </#list>
     </scroller>
    
     <div    v-if="item&&item.status=='20'&&item.taskId>0"  class="nav" ref="bottomNav" >
      <div class="nav-cell"  @click="agree(1)"><text class="nav-text">同意</text></div>
      <div class="nav-cell"><text class="nav-text">不同意</text></div>
      <div class="nav-cell"><text class="nav-text">代办描述</text></div>
      <div class="nav-cell"><text class="nav-text">任务转派</text></div>
    </div>

    <!--  气泡菜单  -->
	 <wxc-popover ref="wxc-popover"
                 :buttons="buttons"
                 :position="{x:-14,y:100}"
                 :arrowPosition="{pos:'top',x:-15}"
                 @wxcPopoverButtonClicked="popoverButtonClicked"></wxc-popover>
     
     <!--  流程图弹窗  -->
     <wxc-mask height="800"
              width="500"
              border-radius="0"
              duration="200"
              mask-bg-color="#FFFFFF"
              :has-animation="true"
              :has-overlay="true"
              :show-close="true"
              :show="showFlowMask"
              @wxcMaskSetHidden="flowMaskSetHidden">
               <scroller class="scroller-mask">
                <wxc-simple-flow :list="flowData" :themeColor="themeColor"></wxc-simple-flow>  
               </scroller>
    </wxc-mask>

    <!--  选择审批人弹窗  -->
    <wxc-mask height="600"
              width="500"
              border-radius="0"
              duration="200"
              mask-bg-color="#FFFFFF"
              :has-animation="true"
              :has-overlay="true"
              :show-close="false"
              :show="showApproversMask"
              @wxcMaskSetHidden="ApproversMaskSetHidden">
      <div class="content-mask">
        <div class="demo-title">
          <text class="title-mask">请选择审核人</text>
        </div>
         <scroller class="scroller-mask">
                <div class="margin">
                    <text  style="">角色：{{approvalRole}}</text>
                    <wxc-checkbox-list :list="approverList"
                                    @wxcCheckBoxListChecked="wxcCheckBoxListChecked"></wxc-checkbox-list>
                </div>
        </scroller>
      </div>
       <div  class="footer-mask">
              <div class="nav-cell"  @click="submitApprovers()"><text class="nav-text">启动</text></div>
              <div class="nav-cell"  @click="wxcMaskSetHidden()"><text class="nav-text">取消</text></div>
      </div>
    </wxc-mask>

    </div>
</template>

<script>
import  i18nMdJson from  '../../../i18n/module/${moduleKey}';
import  {WEEXUI_ICON}  from  '../../../icon/type';
import jwpf from '../../../components/jwpf';
import  QxCell   from  '../../../components/qx-cell';
import  {$curUserLoginName$,$curUserId$,$curOrgId$,$curOrgCode$,$curUserEmployeeId$,$curUserName$ }  from  '../../../components/systemVar'
import {WxcMinibar, WxcCell,Utils,WxcButton,WxcPopover,WxcMask,WxcCheckboxList,WxcSimpleFlow }  from  'weex-ui';
const picker = weex.requireModule('picker')
export default {
    data:()=>({
        resUrls:[],
        item:null,
        userIds:null,
        root:i18nMdJson,
        entityName:'${entityName}',
        <#list formList as form>
              <#if  form.isMaster>
					<#if ((form.referEntityTableName)!"")!="">
						referEntityTableName:'${form.referEntityTableName}',
					<#else>
						referEntityTableName:null,
					</#if>
              <#else>
                    ${form.lcKey}s:null,
              </#if>
        </#list>
         showApproversMask: false,
         showFlowMask:false,
         approverList:[],
         checkedList:[],
         approvalRole:null,
         themeColor: {
            lineColor: '#bf280b',
            pointInnerColor: '#b95048',
            pointBorderColor: '#bf280b',
            highlightTitleColor: '#bf280b',
            highlightPointInnerColor: '#bf280b',
            highlightPointBorderColor: '#d46262'
        },
        flowData: null
    }),
    components:{
       QxCell, WxcMinibar, WxcCell,WxcPopover,WxcMask,WxcCheckboxList ,WxcButton ,WxcSimpleFlow
    },
    props:{
        id:{
            type:Number,
            default:null
        },
        animationType:{
           type:String,
		   default:'push'
		}
    },
    created(){ 
        this.checkResourceUrl();
    	jwpf.setObj(this); 	   
    },
    mounted () {
    },
    methods:{
    <#list formList as form>
            	<#if form.isMaster >
            		<#if form.extension?? && form.extension.editPage??>
            			<#assign eventList =form.extension.editPage.eventList >
	            			<#if eventList?? && eventList?size gt 0>
	            				<#list eventList as event>
	            					<#if event?? && event.content?? && event.content!="">
	            					  	${event.key}(){
		            					 		try{
		            					 			${event.content}
		            					 		}catch(ex){
		            					 			this.alert("执行主表单事件${event.key}出现异常:" + ex);
		            					 		}
		            					  },	            					  		            					 		
	            					</#if>
	            				</#list>
	            			</#if>
            		</#if>
            	</#if>
   	</#list>
   	  	initTimer(){
		let  _wait_=null;
   	  	if(_wait_){
   	  	    clearTimeout(_wait_);
   	  	}
   	  	_wait_=setTimeout(
   	  		()=>{
			    if(this.popupType=='view')
   	  			if(this.onAfterLoadData){
   	  			     this.onAfterLoadData(this.item);
   	  			}
   	  		},
   	  	300)
   	  },
        wxcMinibarRightButtonClicked () {
            this.$refs['wxc-popover'].wxcPopoverShow();
        },
        wxcMinibarLeftButtonClicked(){
            this.popup(false);
            this.$emit('closePage',{});
        },
        popoverButtonClicked (e){
            if(this[e.key])  this[e.key]();
        },
        doSetFormToEdit(){
              this.$emit('toEditPage',{type:'modify',id:this.item.id});
        },
        doAdd(){
              this.alert('新增');
        },
        doSelfProcess() {
		this.confirm( '确定要审核通过该记录吗？审核通过后，该记录将不允许修改和删除！',
			r=> {
           		if(r) {
           			if(this.onBeforeStartProcess) {
			           let flag = this.onBeforeStartProcess();
			           if(flag === false) {
			           	   return;
			           }
			        }
                 	jwpf.doStartOrCancelApprove((this.referEntityTableName?this.referEntityTableName:this.entityName)+"&relevanceModule=", this.item.id, "startApprove", function() {});
					if(this.onAfterStartProcess) {
					   this.onAfterStartProcess();
					}
                } 
        	}
        );
	},
    doProcess(){
	 	this.confirm( '确定要执行该操作吗？',
			r=> {
           		if(r) {
           			if(this.onBeforeStartProcess) {
			           let flag = this.onBeforeStartProcess();
			           if(flag === false) {
			           	   return;
			           }
			        }
           			this.doStartProcess(this.item.id,(this.referEntityTableName?this.referEntityTableName:this.entityName),function(){});
           		}
           	}
        );
	},
    doStartProcess(id,tName,callFunc){
	let options = {
			url :'/gs/process!startPrepare.action',
			data : {
				"bname":tName,
				"bid":id
			},
			success : data=>{
				if(data.msg){//如果返回角色
                     this.initApprovers(data.msg);
                }
                this.$emit('doRefreshListPage',{});		 
			}
		};
	    this.sendRequest(options);
    },
    initApprovers(roles){
        let  options = {
            url:'/gs/process!getRolesList.action',
            data:{
                roles:roles
            },
            async:false,
            success:data=>{
                if(data.msg&&data.msg.length>0){
                    this.approverList=[];
                    this.checkedList=[];
                    data.msg.forEach(row=>{
                        if(row.nocheck){ //审批角色
                            this.approvalRole = row.name;
                        }else if(row.checked){//审批用户
                            row.value  = row.id;
                            row.title = row.name;
                            this.approverList.push(row);
                            this.checkedList.push(row.value);
                        }
                    })
                     this.openApproversMask();
                }
            }
        }
          this.sendRequest(options);
    },
	agree(bool){
        let   pId = this.item.flowInsId;
        let   taskId = this.item.taskId;
        let options = {
				url : '/gs/process!getRolesList.action',
				data : {
					"processInstanceId":pId,
					"taskId": taskId,
					"variableName" : 'approve',
					"value" :bool
				},
				async:false,
				success : data=> {
					if (data.msg){
						if(data.msg instanceof Array){
							if(data.msg.length>1){//多个用户
								   this.approverList=[];
                                    this.checkedList=[];
                                    data.msg.forEach(row=>{
                                        if(row.nocheck){ //审批角色
                                            this.approvalRole = row.name;
                                        }else if(row.checked){//审批用户
                                            row.value  = row.id;
                                            row.title = row.name;
                                            this.approverList.push(row);
                                            this.checkedList.push(row.value);
                                        }
                                    })
                                    this.openApproversMask();
							}else{//没有用户
								this.doComplete(bool);
							}
						} else {//一个用户
                            
								this.userIds.data.msg;
								this.doComplete(bool);
						}
					}else{//没有用户
						this.doComplete(bool);
					}				
				}
			};
			this.sendRequest(options);

    },
    //完成任务
    doComplete(bn){
	 
			var options = {
					url : '/gs/process!complete.action',
                    data　:{
                        'taskId': this.item.taskId,
                        'taskName': this.item.taskName,
                        'executionId': this.item.flowInsId,
                    　　'processInstanceId': this.item.flowInsId,
                        'activiti_approve': bn,
                        'activiti_resend':null, 
                        'userIds': this.userIds,
                        'reason':null
                    },
					success : data=> {
						if (data.msg) {	
                             let path =  this.$router.currentRoute.path;
                             this.jump(path);						
							 this.wxcMinibarLeftButtonClicked();
                             this.$emit('doRefreshListPage',{});
						}
					}
				};
			this.sendRequest(options);
		 
    },
    //打开弹出层
    openApproversMask (e) {
        this.showApproversMask = true;
    },
    //关闭弹出层
    ApproversMaskSetHidden () {
        this.showApproversMask = false;
    },
    wxcCheckBoxListChecked (e) {
        this.checkedList = e.checkedList;
    },
    submitApprovers(){
          let  options = {
              url:'/gs/process!start.action',
              data:{
                  businessId: this.item.id,
                  businessName: (this.referEntityTableName?this.referEntityTableName:this.entityName),
                  userIds: this.checkedList
              },
              async:false,
              success:data=>{
                  if(data.msg==true){
                     this.alert("启动流程成功！");
                     this.$emit("doRefreshListPage",{});
                     this.ApproversMaskSetHidden();
                  }
              }
          }
          this.sendRequest(options);
    },
    //查看流程
    doViewProcess(){
            let options = {
                url:'/gs/process!getProcessInfoListById.action?processInstanceId='+this.item.flowInsId+'&id='+this.item.id+'&entityName='+(this.referEntityTableName?this.referEntityTableName:this.entityName),
                data:{
                    'page':1,
                    'rows':20,
                    'sort':'id',
                    'order':'asc'
                },
                success:data=>{
                    if(data.rows instanceof Array){
                        this.flowData = [];
                        data.rows.forEach(row=>{
                              let  flowRow = {};
                              flowRow.date = row.dealTime;
                               flowRow.title = row.userName+'-'+row.taskName
                              if(row.isApprove=="1"){
                                   flowRow.title += '-同意';
                              }else if(row.isApprove =="0"){
                                   flowRow.title += '-不同意'
                              }else{
                              }                                                       
                              if(row.reason){
                                    flowRow.desc = '原因：'+row.reason;   
                              }
                              this.flowData.push(flowRow);                      
                        });
                        this.showFlowMask = true;
                    }
    
                }
            }
            this.sendRequest(options)
    },
    //关闭流程
    flowMaskSetHidden(){
               this.showFlowMask = false;
    },
    initMasterForm(id){
        if(id){
            let options = {
                url:'/gs/gs-mng!queryEntity.action?entityName='+(this.referEntityTableName?this.referEntityTableName:this.entityName)+'&relevanceModule=',
                data:{
                'id':id
                },
                success:data=>{
                    this.item = data.msg;                     
                }
            }
            this.sendRequest(options);
        }
    },
    //初始化子表
    initSubs(id){
            <#list  formList as form>
                 <#if  !form.isMaster>
                     <#if ((form.referEntityTableName)!"")!="">
							this.initSub(id,'${form.lcKey}s','${form.referEntityTableName}');
					 <#else>
                     		this.initSub(id,'${form.lcKey}s','${form.entityName}');
					 </#if>
                 </#if>
            </#list>
    },
    initSub(id,formlcName,entityName){
        if(id){
            let  options = {
                url:'/gs/gs-mng!querySubList.action?entityName='+entityName+'&id='+id,
                data:{
                    sort:'sortOrderNo',
                    order:'asc'
                },
                success:data=>{
                    if(data.rows&&data.rows.length>0){
                                this[formlcName] = data.rows;
                    }
                }
            };
            this.sendRequest(options);
        }
    },
    popup(status=true,callback=null){
        const ref = this.$refs['view-page'];
        if (this.animationType === 'push') {
          Utils.animation.pageTransitionAnimation(ref, `translateX(${r'${status ? -750 : 750}'}px)`, status, callback)
        } else if (this.animationType === 'model') {
          Utils.animation.pageTransitionAnimation(ref, `translateY(${r'${status ? -Utils.env.getScreenHeight() : Utils.env.getScreenHeight()}'}px)`, status, callback)
        }
    },
    show (e) {
        if(e.status){
            //初始化主表
            if(e.item){
                this.item = e.item;
            }else{
                this.initMasterForm(e.id);
            }
        }
            //初始化子表
            this.initSubs(e.id);
        this.popup(e.status);
        this.initTimer();
    },
    checkResourceUrl(){
         this.getCache("_resource_url_").then(res=>{
            if(res&&res.length>0){
              this.resUrls=res;
            }else{
              this.jump('/');
            }
         });
      }
    },
    computed:{
         cityExtendStyle () {
			return Utils.uiStyle.pageTransitionAnimationStyle(this.animationType)
		 },
         contentStyle(){
              const PageHeight = Utils.env.getPageHeight();
              let contentHeight;
              if(this.item&&this.item.status=="20") {
                contentHeight=PageHeight-100;
                }else{
                  contentHeight = PageHeight
               }
               return  { height: contentHeight + 'px' };                  
         },
         buttons(){
             const s10btns = [];
             const s20btns = [];
             const s31btns = [];
             const s40btns = [];
                if(this.resUrls.indexOf('/gs/gs-mng!add.action?entityName=${entityName}')>=0){
                        s10btns.push({
                            icon: WEEXUI_ICON,
                            text:'新增',
                            key:'doAdd',
                            resourceUrl:'/gs/gs-mng!add.action?entityName=${entityName}'
                        });
                        s31btns.push({
                            icon: WEEXUI_ICON,
                            text:'新增',
                            key:'doAdd',
                            resourceUrl:'/gs/gs-mng!add.action?entityName=${entityName}'
                        });
                }

                if(this.resUrls.indexOf('/gs/gs-mng!modify.action?entityName=${entityName}')>=0){
                        s10btns.push({
                            icon: WEEXUI_ICON,
                            text:'修改',
                            key:'doSetFormToEdit',
                            resourceUrl:'/gs/gs-mng!modify.action?entityName=${entityName}'
                        });
                }

                if(this.resUrls.indexOf('/gs/gs-mng!delete.action?entityName=${entityName}')>=0){
                        s10btns.push({
                            icon: WEEXUI_ICON,
                            text:'删除',
                            key:'doDel',
                            resourceUrl:'/gs/gs-mng!delete.action?entityName=${entityName}'
                        });
                }

                if(this.resUrls.indexOf('/gs/gs-mng!edit.action?entityName=${entityName}')>=0){
                        s10btns.push({
                            icon: WEEXUI_ICON,
                            text:'复制',
                            key:'doCopyMainTab',
                            resourceUrl:' /gs/gs-mng!edit.action?entityName=${entityName}'
                        });
                        s31btns.push({
                            icon: WEEXUI_ICON,
                            text:'复制',
                            key:'doCopyMainTab',
                            resourceUrl:' /gs/gs-mng!edit.action?entityName=${entityName}'
                        });
                }

                if(this.resUrls.indexOf('/sys/attach/attach.action?entityName=${entityName}')>=0){
                    s10btns.push({
                        icon: WEEXUI_ICON,
                        text:'附件',
                        key:'doAccessory',
                        resourceUrl:'/sys/attach/attach.action?entityName=${entityName}'
                    });
                    s31btns.push({
                        icon: WEEXUI_ICON,
                        text:'附件',
                        key:'doAccessory',
                        resourceUrl:'/sys/attach/attach.action?entityName=${entityName}'
                    });
                }
                <#if  isSelfApprove==true>
                if(this.resUrls.indexOf('/gs/process!startApprove.action?entityName=${entityName}')>=0){
                    s10btns.push({
                    icon: WEEXUI_ICON,
                    text:'审核通过',
                    key:'doSelfProcess',
                    resourceUrl:'/gs/process!startApprove.action?entityName=${entityName}'
                });
                }
    
                <#else>
                if(this.resUrls.indexOf('/gs/process!startApprove.action?entityName=${entityName}')>=0){
                    s10btns.push({
                    icon: WEEXUI_ICON,
                    text:'启动流程',
                    key:'doProcess',
                    resourceUrl:'/gs/process!startApprove.action?entityName=${entityName}'
                });
                }          
                </#if>

                if(this.resUrls.indexOf('/gs/gs-mng!view.action?entityName=${entityName}')>=0){//有查看权限，就算有查看审核历史权限
                        s10btns.push({
                            icon: WEEXUI_ICON,
                            text:'审核历史',
                            key:'doViewProcess',
                            resourceUrl:''
                        });
                        s31btns.push({
                            icon: WEEXUI_ICON,
                            text:'审核历史',
                            key:'doViewProcess',
                            resourceUrl:''
                        });
                }
                if(this.resUrls.indexOf('/sys/report/report.action?entityName=${entityName}')>=0){
                        s10btns.push({
                            icon: WEEXUI_ICON,
                            text:'报表',
                            key:'doViewReport',
                            resourceUrl:'/sys/report/report.action?entityName=${entityName}'
                        });
                        s31btns.push({
                            icon: WEEXUI_ICON,
                            text:'报表',
                            key:'doViewReport',
                            resourceUrl:'/sys/report/report.action?entityName=${entityName}'
                        });
                }
             
                
                if(this.resUrls.indexOf('/gs/gs-mng!revise.action?entityName=${entityName}')>=0)
                s31btns.push({
                    icon: WEEXUI_ICON,
                    text:'启用修订',
                    key:'doSetFormToRevise',
                    resourceUrl:'/gs/gs-mng!revise.action?entityName=${entityName}'
                });

                if(this.resUrls.indexOf('/sys/log/revise-log.action?mn=${entityName}')>=0)
                s31btns.push({
                    icon: WEEXUI_ICON,
                    text:'修订日志',
                    key:'doViewRevise()',
                    resourceUrl:'/sys/log/revise-log.action?mn=${entityName}'
                });
               
                if(this.resUrls.indexOf('/gs/gs-mng!revise.action?entityName=${entityName}')>=0)
                s31btns.push({
                    icon: WEEXUI_ICON,
                    text:'变更',
                    key:'doChange',
                    resourceUrl:'/gs/gs-mng!revise.action?entityName=${entityName}'
                });
                
                
                if(this.resUrls.indexOf('/gs/process!cancelApprove.action?entityName=${entityName}')>=0)            
                s31btns.push({
                    icon: WEEXUI_ICON,
                    text:'撤销审批',
                    key:'doCancelApproved',
                    resourceUrl:'/gs/process!cancelApprove.action?entityName=${entityName}'
                });

                if(this.resUrls.indexOf('/gs/process!invalidProcess.action?entityName=${entityName}')>=0)                     
                s31btns.push({
                    icon: WEEXUI_ICON,
                    text:'作废',
                    key:'openCancelWin',
                    resourceUrl:'/gs/process!invalidProcess.action?entityName=${entityName}'
                });
             let  result=[];  
             if(this.item){
                switch(this.item.status){
                    case '10': result = s10btns; break;
                    case '20': result = s20btns; break;
                    case '31': result = s31btns; break;
                    case '40': result = s40btns; break;
                }
            }
            return  result;
         }
    },
}
</script>

<style  scoped>
 .wrapper {
    position: fixed;
    background-color: #F2F3F4;
  }
 .nav {
    left: 0;
    flex-direction: row;
    align-items: center;
    bottom: 0;
  }
  .nav-cell {
    flex: 1;
    height:50px;
    background-color: #ffffff;
    align-items: center;
    justify-content: center;
  }
  .nav-cell:active{
     background-color:#00BDFF;
  }

  .scroll-container {
    flex: 1;
    border-top-color:#e0dddd;
    border-top-style:ridge ;
    border-top-width:2px;
    padding-bottom:30px;
  }
  .content-mask{
    background-color:white;
    height:300px;
  }
  .title-mask{
    background-color: #ffffff;
    align-items: center;
    justify-content: center;
     border-bottom-width:1px;
     border-bottom-color:gray;
  }
  .scroller-mask {
    height:600;
    flex: 1;
  }
  .margin {
    margin-top: 20px;
    margin-left:10px;
    margin-right:10px;
  }
  
  .footer-mask{
     border-top-width:1px;
     border-top-color:gray;
     flex-direction:row;
     justify-content:center;
  }
  .form-title{
	flex-direction:row;
	justify-content:space-between;
}
</style>
<style src='../../../style/main.css' />

