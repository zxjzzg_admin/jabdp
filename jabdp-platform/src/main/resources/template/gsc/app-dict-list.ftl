<#import "common/field.ftl" as fieldftl>  
<#assign entityName=root.entityName />  
<#assign formList=root.dataSource.formList/>
<#assign titleName=root.moduleProperties.caption/>
<#assign i18nKey=root.moduleProperties.i18nKey/>
<#assign moduleKey=root.moduleProperties.key/>


<template>
  <div class="wrapper" >
      <header v-if="popupType=='nopopup'" >
       <wxc-minibar :title="root['${i18nKey!titleName}']"
                       background-color="#009ff0"
                       text-color="#FFFFFF"
                       left-text = "关闭"
                       right-text="新增"
                       @wxcMinibarLeftButtonClicked="minibarLeftButtonClick"
                       @wxcMinibarRightButtonClicked="doAdd"></wxc-minibar>
      </header>
      <scroller class="scroll-container">
      <div class="body-container">
      <list  v-if="rows.length>0">
      <cell    v-for="(item,index) in rows" :key ='index' >
            <wxc-cell  @touchstart="doListEdit" @touchend="clearLoop" class  ="item-container"
            :has-bottom-borde="true"
            :has-vertical-indent="true"
            @wxcCellClicked="doView(item,index)" 
            >
            <div  slot ="title" >
               <#list formList as form>      
	      <#list form.fieldList as field>
	      <#if field.key!="">
		        <div  class = "label-view">
		                <text    v-text="root['${field.i18nKey!field.caption}']"></text>
		                <text>:</text>
		                <text   v-text="item.${field.key}"></text>
		        </div>
		  </#if>
	       </#list>
      </#list>
            </div>

            <div  class="sys-row"  slot = "value"> 
              <div class="sys-fields">
                    <text class="sys-field" v-text="item.status_caption||''"></text>
                    <text class="sys-field" v-text="item.createUser_caption||''" ></text>
                    <text class="sys-field" > {{item.createTime | formatDate}}</text> 
              </div>
              <div  v-if="isListEdit" > 
                    <!-- <input  @click="doItemClick(item,index)"  class="checkbox"  type="checkbox"  :value="index"   v-model="checkedItem"/>   -->
                     <qx-checkbox  :value="item"  @wxcCheckBoxItemChecked="wxcCheckBoxItemChecked"  :checked="false" >  </qx-checkbox>
            
              </div>
            </div>
            </wxc-cell>
            
        

    </cell>
     
    </list>
    </div>
    </scroller>
   
    //查看页面
     <wxc-popup
               v-if="popupType=='view'"
               popup-color="rgb(92, 184, 92)"
               :show="popupType=='view'"
               @wxcPopupOverlayClicked="popupOverlayBottomClick"
               pos="right"
               width="100%">
      <div class="demo-content"  >
        <wxc-minibar :title="'查看'+root['${i18nKey!titleName}']"
                       left-text ="关闭"
                       background-color="#009ff0"
                       text-color="#FFFFFF"
                       right-text="修改"
                       @wxcMinibarLeftButtonClicked="popupOverlayBottomClick"
                       @wxcMinibarRightButtonClicked="doEdit"
                       :use-default-return="false"></wxc-minibar>       
        
                <#list formList as form>      
	      <#list form.fieldList as field>
	      <#if field.key!="">
		        <wxc-cell 
		                @wxcCellClicked="wxcCellClicked">
		                <label  slot="label"></label>
		                <text   slot="title"  v-text="root['${field.i18nKey!field.caption}']"></text>
		                <output   slot="value" v-text="viewItem.${field.key}"/>
		        </wxc-cell>
		  </#if>
	       </#list>
      </#list>
      </div>
    </wxc-popup>
    //修改(新增)页面
    <wxc-popup 
               v-if="popupType=='edit'||popupType=='add'"
               popup-color="rgb(92, 184, 92)"
               :show="popupType=='edit'||popupType=='add'"
               @wxcPopupOverlayClicked="popupOverlayBottomClick"
               pos="right"
               width="100%">
      <div class="demo-content" >
         <header>
        <wxc-minibar :title="'修改'+root['${i18nKey!titleName}']"
                       left-text ="取消"
                       background-color="#009ff0"
                       text-color="#FFFFFF"
                       right-text="保存"
                       @wxcMinibarLeftButtonClicked="doCancel"
                       @wxcMinibarRightButtonClicked="doSaveEdit"
                       :use-default-return="false"></wxc-minibar>     
         </header>  
                <#list formList as form>      
	      <#list form.fieldList as field>
	      <#if field.key!="">
		        <wxc-cell 
		                @wxcCellClicked="wxcCellClicked">
		                <label  slot="label"></label>
		                <text   slot="title"  v-text="root['${field.i18nKey!field.caption}']"></text>
		                <input  slot="value"    type="text"  v-model="editItem.${field.key}"/>
		        </wxc-cell>
		  </#if>
	       </#list>
      </#list>
		      
      </div>
    </wxc-popup>
     <wxc-slide-nav    v-if="isListEdit"  class="nav nav-bottom" ref="bottomNav" position="bottom">
      <div class="nav-cell"  @click="doListDelete"><text class="nav-text">删除</text></div>
      <div class="nav-cell"><text class="nav-text">作废</text></div>
      <div class="nav-cell"><text class="nav-text">审核通过</text></div>
      <div class="nav-cell"><text class="nav-text">取消审核</text></div>
    </wxc-slide-nav>
      </div>
</template>

<script>
  import { WxcMinibar,WxcCell,WxcPopup ,WxcButton,WxcSlideNav } from 'weex-ui';
  import { formatDate } from  '../../../filters';
  var modal = weex.requireModule('modal');
  import  i18nMdJson from  '../../../i18n/module/${moduleKey}' ;
  import  QxCheckbox from  '../../../components/qx-checkbox' ;
  export default {
    components: { WxcCell,WxcMinibar,WxcPopup,WxcButton ,WxcSlideNav,QxCheckbox},
    data:()=>({ 
        lists:[{label:1,text:1111},
              {label:1,text:2222}],    
        root:i18nMdJson,
        t:"1111",
        rows:[1,2],
        viewItem:null,
        editItem:null,
        popupType:'nopopup',
        curIndex:null,
        checkedItem:[],
        isListEdit:false,
        Loop:null,
        checkedTag:null,
        entityName:'${entityName}',
        checked:false
    }),
    created(){
        this.doQuery();
    },
    beforeUpdate(){
    },
    computed:{
    },
    methods: {
      wxcCheckBoxItemChecked(e){
           console.log(e);
           if (e.checked) {
          this.checkedItem.push(e.value);
          } else {
            const index = this.checkedItem.indexOf(e.value);
            this.checkedItem.splice(index, 1);
          }
      },
      doSelfProcess(){
          let ids = [];
          let rows = this.checkedItem;
          for(let i = 0;i<rows.length;i++){
            if(rows[i].status!='10'&&rows[i].status!='31'){
              this.alert("只有草稿状态的记录才可以进行该操作！");
              return;
            }
            ids.push(rows[i].id);
          }
          if(ids.length){
            this.confirm('确定要审核通过吗？审核通过后，该记录将不许修改和删除！',r=>{
              if(r){

              }
            })
          }
      },
      //删除勾选的
      doListDelete(){         
           console.log(this.checkedItem);
          let ids = [];
          for(let i=0;i<this.checkedItem.length;i++){
            console.log(this.checkedItem[i]);
            if(this.checkedItem[i].status!='10'){
               this.alert("亲，非草稿状态的不能删除！");
               return;
            }
            ids.push(this.checkedItem[i].id);
          }
          if(ids!=null&&ids.length>0){
            this.confirm('确定要删除吗',r=>{
                 if(r){
                     this.sendRequest({
                       url:'/gs/gs-mng!delete.action?entityName='+this.entityName,
                       data:{
                         'ids':ids
                       },
                       success:data=>{
                          if(data.msg){
                            this.doQuery();
                          }
                       },
                     })
                 }
            })
          }else{
            this.alert("请选择要删除的记录！");
          }
          
      },
      //列表编辑
      doListEdit(){   
          clearInterval(this.Loop);//再次清空定时器，防止重复注册定时器
          this.Loop=setTimeout(()=>{
              this.isListEdit = true;
              this.alert( this.isListEdit);
          },1000);
  
      },
      clearLoop(){
          clearInterval(this.Loop);
      },
      //新增
      doAdd(){
        this.editItem={
          danweibianhao:null,
          danweimingchen:null,
          status:'10'
        }
        this.popupType = 'add';
        
      },
      
      //保存修改
      doSaveEdit(){
        let jsonSaveData = JSON.stringify(this.editItem);
        let params = {
          "jsonSaveData":jsonSaveData
        };
        this.sendRequest({
          "url":"/gs/gs-mng!save.action?entityName="+this.entityName,
          "data":params,
          "success":data=>{
              //this.rows[this.index]=this.editItem;
              //this.viewItem=this.editItem;
              //this.popupType = 'nopupType';
              if(data.flag==1){
                   let id = data.msg;
                   if(this.popupType=='edit'){ //只更新修改了的数据
                      this.upLoadList(id);
                   }else if(this.popupType=='add'){ //更新整个列表数据
                      this.doQuery();
                      this.popupType='nopopup';
                   }
              }else{

              }

          }
        })
      },
      //更新列表
      upLoadList(id){
           this.readbyId(id,rowData=>{
             this.rows[this.curIndex]=rowData;
             this.popupType='nopopup';
             modal.toast({
             message:'保存成功！'
           });
           });

      },
      //通过id查找
      readbyId(id,callFunc){
          let options = {
            'url':'/gs/gs-mng!queryEntity.action?entityName='+this.entityName,
            data:{
              "id":id
            },
            success:data=>{
              if(data.flag==1){
                callFunc(data.msg);
              }
            }
          }
          this.sendRequest(options);
      },
      //取消编辑
      doCancel(){
          if(this.popupType=='add'){
            this.popupType='nopopup'
          }else if(this.popupType='edit'){
            this.popupType="view";
          }
      },
      //修改
      doEdit(){
        //深拷贝
         this.editItem=JSON.parse(JSON.stringify(this.viewItem));
         this.popupType="edit";
      },
      //查看
      doView(item,index){
        if(!this.isListEdit){
              this.curIndex=index;
              this.viewItem = item;
              this.popupType = "view";
        }
      },

      //非状态组件，需要在这里关闭
      popupOverlayBottomClick () {
        this.popupType = "nopopup";
      },
      wxcCellClicked (e) {
        console.log(e)
      },
      minibarLeftButtonClick(e){ 
        modal.toast({message:"left"});
      },
      minibarRightButtonClick(e) {
        modal.toast({message:"right"});
      },
      doQuery(){
        const self = this;
        let params = {
				"rows" : 120,
				"page" : 1,
				"sort" : "id",
				"order" : "desc"
		    };
        self.sendRequest({
    			"url" : "/gs/gs-mng!queryDictList.action?entityName="+this.entityName, // servlet请求地址
    			"data" : params, // action对应的参数
    			"success" : data=> {
                 console.log(data.rows);
                 this.rows =   data.rows; 
                 this.isListEdit = false;              
          }
    	});
      }
    },
    filters: {
    capitalize (value) {
      if (!value) return ''
      value = value.toString()
      return value.charAt(0).toUpperCase() + value.slice(1)
    },
    formatDate(time) {
      let date = new Date(time);
      return formatDate(date, 'yyyy-MM-dd');
    }
    }
  };
</script>

<style scoped>
  .nav {
    position: fixed;
    width: 100%;
    left: 0;
    flex-direction: row;
    align-items: center;
  }
  .nav-bottom {
    bottom: 0;
  }
  .nav-cell {
    flex: 1;
    height: 80px;
    width:25%;
    background-color: #ffffff;
    align-items: center;
    justify-content: center;
  }
  .nav-cell:active{
     background-color:#00BDFF;
  }


  .scroll-container {
    flex: 1;
    margin-bottom:200px; 
  }

.footer{
    flex: 0;      
}
  .item {
    border-bottom-width: 2px;
    border-bottom-color: #c0c0c0;
    margin:15px;
  }

  .item-container:active {
     background-color: #00BDFF;
   }
  
  .label-view{
     flex-direction: row;
   }
   .sys-fields{
     margin: 20px;
   }
   .sys-field{
     font-size: 60%;
     color:#c0c0c0;
   }
   .sys-row{
      flex-direction: row;
   }
  .checkbox{
      margin:auto;
      padding-left:60;
     width:40px;
    height:40px;
    color: green;
  }

  .scroll-container {
    flex: 1;
  }
  .body-container {
    width: 750px;
    background-color: #f2f3f4;
  }
 
</style>

