<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<title><s:text name="system.index.title"/></title>
	<%@ include file="/common/meta-css.jsp" %>
<#import "common/field.ftl" as fieldftl>
<#--include "common/field.ftl"-->
<#assign entityName=root.entityName />
<#assign titleName=root.moduleProperties.caption/>
<#assign moduleKey=root.moduleProperties.key/>
<#assign i18nKey=root.moduleProperties.i18nKey/>
<#assign formList=root.dataSource.formList/>
<#assign tabsList=root.dataSource.tabsList/>
<#assign flowList= root.flowList />
<#assign isSelfApprove=true />
<#if flowList??&&flowList?size gt 0>
<#assign isSelfApprove=false />
</#if>
<#assign dynamicShowTable = false /><#-- 控制是否动态显示子表 -->
<#-- 关联表表 -->
<#assign realEntityName = entityName />
<#if root.moduleProperties.relevanceModule??>
<#assign relevanceModule=root.moduleProperties.relevanceModule/>
<#list formList as form>
<#if form.isMaster>
	<#assign realEntityName = form.entityName />
	<#break/>
</#if>
</#list>
<#else>
<#assign relevanceModule=""/>
</#if>
<#list formList as form><#-- 用于将隐藏子表数量去掉 -->
	<#if form.isMaster>
		<#assign dynamicShowTable = (form.dynamicShow)!false />
	</#if>
</#list>
<link rel="stylesheet" type="text/css" href="${r"${ctx}"}/js/easyui/gray/easyui-module.css" colorTitle="gray"/>
<link rel="stylesheet" type="text/css" href="${r"${ctx}"}/js/easyui/blue/easyui-module.css" colorTitle="blue"/>
<style type="text/css"><#-- 初始化样式 -->
.tabs-tool {border:0;}
body {margin:0px}
<@fieldftl.initFormCssOrJavascript formList=formList eventKey="addCssStyle"/>
</style>
</head>
<body class="easyui-layout" fit="true" id="layout_body">
<c:choose>
	<c:when test="${r"${param.isEdit=='1'}"}"> <#-- 驳回时使用 -->
	<div region="north" style="overflow:hidden;" border="false" id="toolbar_layout">
	    <div class="datagrid-toolbar">
								<a id="tcancel" href="javascript:void(0);" onclick="doCancelEdit()" class="easyui-linkbutton" plain="true" iconCls="icon-undo"><s:text name="system.button.cancel.title"/>(U)</a>
	                             <security:authorize
	                             			<#-- 关联表 -->
											url="/gs/gs-mng!save.action?entityName=${entityName}&relevanceModule=${relevanceModule}">
	                             	<a id="tsave" href="javascript:void(0);" onclick="doSaveObj()" class="easyui-linkbutton" plain="true" iconCls="icon-save"><s:text name="system.button.save.title"/>(S)</a>
	                             </security:authorize>    
	                             <security:authorize
											url="/gs/gs-mng!modify.action?entityName=${entityName}">
											<a id="tedit" href="javascript:void(0);" onclick="doSetFormToEdit()"
												class="easyui-linkbutton" plain="true" iconCls="icon-edit"
												><s:text name="system.button.modify.title"/>(M)</a>
								 </security:authorize>
								 <security:authorize
													url="/sys/attach/attach.action?entityName=${entityName}">
			                            <a id="tattach" href="javascript:void(0);" onclick="doAccessory('atmId')" class="easyui-linkbutton" plain="true" iconCls="icon-attach"><s:text name="system.button.accessory.title"/>(Y)</a>
			                     </security:authorize>
	    </div>
	</div>                        	
	</c:when>
	<c:when test="${r"${param.isEdit=='2'}"}"> <#-- 只允许查看时使用 -->
	<div region="north" style="overflow:hidden;" border="false" id="toolbar_layout">
	    <div class="datagrid-toolbar">
	    <security:authorize
				url="/gs/gs-mng!revise.action?entityName=${entityName}"> <#-- 修订 -->
										<a id="trevise" href="javascript:void(0);" onclick="doSetFormToRevise()"
											class="easyui-linkbutton" plain="true" iconCls="icon-edit"
											><s:text name="启用修订"/></a>
										<a id="tcancelrevise" href="javascript:void(0);" onclick="doCancelSetFormToRevise()"
											class="easyui-linkbutton" plain="true" iconCls="icon-edit"
											><s:text name="退出修订"/></a>	
		</security:authorize>
	    <security:authorize url="/sys/attach/attach.action?entityName=${entityName}">
		<a id="tattach" href="javascript:void(0);" onclick="doAccessory('atmId')" class="easyui-linkbutton" plain="true" iconCls="icon-attach"><s:text name="system.button.accessory.title"/>(Y)</a>
	    </security:authorize>
		<@fieldftl.initButtonOnViewPage entityName=entityName moduleKey=moduleKey formList=formList isDisplayReport=true isReportOnly=true />
	    </div>
	</div>                        	
	</c:when>
	<c:when test="${r"${param.isEdit=='3'}"}"> <#-- 快捷新增时使用，只有保存按钮 -->
	<div region="north" style="overflow:hidden;" border="false" id="toolbar_layout">
	    <div class="datagrid-toolbar">
	                             <security:authorize
	                             			<#-- 关联表 -->
											url="/gs/gs-mng!save.action?entityName=${entityName}&relevanceModule=${relevanceModule}">
	                             	<a id="tsave" href="javascript:void(0);" onclick="doSaveObj()" class="easyui-linkbutton" plain="true" iconCls="icon-save"><s:text name="system.button.save.title"/>(S)</a>
	                             </security:authorize>
	    </div>
	</div>                        	
	</c:when>	
	<c:otherwise>
<div region="north" style="overflow:hidden;" border="false" id="toolbar_layout">
                        <div class="datagrid-toolbar">
                             <a id="tclose" href="javascript:void(0);" onclick="doCloseCurrentTab()" class="easyui-linkbutton" plain="true" iconCls="icon-cancel" title="<s:text name="system.button.close.title"/>">(Q)</a>
                             <a id="tcancel" href="javascript:void(0);" onclick="doCancelEdit()" class="easyui-linkbutton" plain="true" iconCls="icon-undo"><s:text name="system.button.cancel.title"/>(U)</a>
                             <a id="trefresh" href="javascript:void(0);" onclick="doCancelEdit()" class="easyui-linkbutton" plain="true" iconCls="icon-reload" title="<s:text name="刷新"/>"></a>
                             <security:authorize
                             			<#-- 关联表 -->
										url="/gs/gs-mng!save.action?entityName=${entityName}&relevanceModule=${relevanceModule}">
                             	<a id="tsave" href="javascript:void(0);" onclick="doSaveObj()" class="easyui-linkbutton" plain="true" iconCls="icon-save"><s:text name="system.button.save.title"/>(S)</a>
                             </security:authorize>
		                           <security:authorize
												url="/gs/gs-mng!view.action?entityName=${entityName}">
		                            <a id="tfirst" href="javascript:void(0);" onclick="doView('first')" class="easyui-linkbutton" plain="true" iconCls="icon-first" title="<s:text name="system.button.first.title"/>">(F)</a>
		                            <a id="tprev" href="javascript:void(0);" onclick="doView('pre')" class="easyui-linkbutton" plain="true" iconCls="icon-prev" title="<s:text name="system.button.prev.title"/>">(←)</a>
		                            <a id="tnext" href="javascript:void(0);" onclick="doView('next')" class="easyui-linkbutton" plain="true" iconCls="icon-next" title="<s:text name="system.button.next.title"/>">(→)</a>
		                            <a id="tlast" href="javascript:void(0);" onclick="doView('last')" class="easyui-linkbutton" plain="true" iconCls="icon-last" title="<s:text name="system.button.last.title"/>">(L)</a>
		                           </security:authorize>
		                           <security:authorize
										url="/gs/gs-mng!add.action?entityName=${entityName}">
										<a id="tadd" href="javascript:void(0);" onclick="doAdd()" class="easyui-linkbutton"
											plain="true" iconCls="icon-add"><s:text name="system.button.add.title"/>(I)</a>
									</security:authorize>
									<security:authorize
										url="/gs/gs-mng!modify.action?entityName=${entityName}">
										<a id="tedit" href="javascript:void(0);" onclick="doSetFormToEdit()"
											class="easyui-linkbutton" plain="true" iconCls="icon-edit"
											><s:text name="system.button.modify.title"/>(M)</a>
									</security:authorize>
									<security:authorize
										url="/gs/gs-mng!revise.action?entityName=${entityName}"> <#-- 修订 -->
										<a id="trevise" href="javascript:void(0);" onclick="doSetFormToRevise()"
											class="easyui-linkbutton" plain="true" iconCls="icon-edit"
											><s:text name="启用修订"/></a>
										<a id="tcancelrevise" href="javascript:void(0);" onclick="doCancelSetFormToRevise()"
											class="easyui-linkbutton" plain="true" iconCls="icon-edit"
											><s:text name="退出修订"/></a>	
									</security:authorize>
									<security:authorize
										url="/sys/log/revise-log.action?mn=${entityName}"> <#-- 查看修订日志 -->
										<a id="treviseview" href="javascript:void(0);" onclick="doViewRevise()"
											class="easyui-linkbutton" plain="true" iconCls="icon-new-doc-ico"
											><s:text name="修订日志"/></a>
									</security:authorize>
									<#-- 关联表 -->
									<security:authorize url="/gs/gs-mng!delete.action?entityName=${entityName}&relevanceModule=${relevanceModule}">
										<a id="tdel" href="javascript:void(0);" onclick="doDel()"
											class="easyui-linkbutton" plain="true" iconCls="icon-remove"
											><s:text name="system.button.delete.title"/>(D)</a>
									</security:authorize>
									<security:authorize
										url="/gs/gs-mng!edit.action?entityName=${entityName}">
										<a id="tcopy" href="javascript:void(0);" onclick="doCopyMainTab()"
											class="easyui-linkbutton" plain="true" iconCls="icon-copy"><s:text name="system.button.copy.title"/>(G)</a>
									</security:authorize>
									<security:authorize
										url="/gs/gs-mng!edit.action?isChange=1&entityName=${entityName}">		
										<a id="tcopyChange" href="javascript:void(0);" onclick="doChange()"
											class="easyui-linkbutton" plain="true" iconCls="icon-copy"><s:text name="system.button.copyChange.title"/></a>	
									</security:authorize>
		                            <security:authorize
												url="/sys/attach/attach.action?entityName=${entityName}">
		                            <a id="tattach" href="javascript:void(0);" onclick="doAccessory('atmId')" class="easyui-linkbutton" plain="true" iconCls="icon-attach"><s:text name="system.button.accessory.title"/>(Y)</a>
		                           </security:authorize>
		                        <#if flowList??&&flowList?size gt 0>
		                        <#assign processBtName="system.button.startFlow.title" />
		                        <#list formList as form>
									      <#if form.isMaster>
									      	<#if form.extension?? && form.extension.editPage??>
									      		<#assign buttonList=form.extension.editPage.buttonList />
									      		<#if buttonList?? && buttonList?size gt 0>
									      			<#list buttonList as button>
									      				<#if button.key == "bt_process">
									      					<#assign processBtName=button.caption />
									      				</#if>
									      			</#list>
									      		</#if>
									      	</#if>
									      </#if>
								</#list>
		                        <security:authorize
												url="/gs/process!startApprove.action?entityName=${entityName}">
		                        <a id="tprocess" href="javascript:void(0);" onclick="doProcess()" class="easyui-linkbutton" plain="true" iconCls="icon-flowStart"><s:text name="${processBtName}"/>(P)</a>
		                        </security:authorize>
		                        <security:authorize
												url="/gs/process!cancelApprove.action?entityName=${entityName}">
		                        <a id="tcancelprocess" href="javascript:void(0);" onclick="doCancelProcess()" class="easyui-linkbutton" plain="true" iconCls="icon-user"><s:text name="system.button.cancelProcess.title"/>(J)</a>
		                        </security:authorize>
		                        <a id="tviewprocess" href="javascript:void(0);" onclick="doViewProcess()" class="easyui-linkbutton" plain="true" iconCls="icon-flowDetail"><s:text name="system.button.viewFlow.title"/>(E)</a>
                        		<s:if test='#session.USER.isSuperAdmin=="1"'><#-- 对于已经审批结束的单据进行操作，将其设为草稿状态，可以重新发起流程，只有超级管理员可以执行该操作 -->
									<a id="tcancelApproved" href="javascript:void(0);" onclick="doCancelApproved()" class="easyui-linkbutton" plain="true" iconCls="icon-user"><s:text name="撤销审批"/></a>
								</s:if>
                        		<#else>
                        		<security:authorize
												url="/gs/process!startApprove.action?entityName=${entityName}">
                        		<a id="tprocess" href="javascript:void(0);" onclick="doSelfProcess()" class="easyui-linkbutton" plain="true" iconCls="icon-flowStart"><s:text name="system.button.selfApprove.title"/>(P)</a>
                        		</security:authorize>
                        		<security:authorize
												url="/gs/process!cancelApprove.action?entityName=${entityName}">
                        		<a id="tcancelprocess" href="javascript:void(0);" onclick="doCancelSelfProcess()" class="easyui-linkbutton" plain="true" iconCls="icon-user"><s:text name="system.button.cancelProcess.title"/>(J)</a>
                        		</security:authorize>
                        		<a id="tviewprocess" href="javascript:void(0);" onclick="doViewProcess(true)" class="easyui-linkbutton" plain="true" iconCls="icon-flowDetail"><s:text name="审核历史"/>(E)</a>
                        		</#if>
                        		<security:authorize
												url="/gs/process!invalidProcess.action?entityName=${entityName}">
                        		<a id="tinvalidData" href="javascript:void(0);" onclick="openCancelWin()" class="easyui-linkbutton" plain="true" iconCls="icon-invalid"><s:text name="system.button.invalid.title"/></a>                     		
                        		</security:authorize>
                        		<@fieldftl.initButtonOnViewPage entityName=entityName moduleKey=moduleKey formList=formList isDisplayReport=true isReportOnly=false />
							
                        </div>
</div>     
	</c:otherwise>
</c:choose>           
<div region="center" style="+position:relative;overflow:auto;" class="module-bg" border="false">
                <form
                        action=""
                        name="viewForm" id="viewForm" style="margin:0px;">
                        <input type="hidden" id="id" name="id"/>
                        <input type="hidden" id="atmId" name="atmId" />
                        <input type="hidden" id="flowInsId" name="flowInsId" />
                        <input type="hidden" id="version" name="version" /><#-- 并发操作版本号 -->
                        <input type="hidden" id="status" name="status" value="10" />
                      <#list formList as form>
                      		<#if form.isMaster || form.listType=="Form">
                      			<div style="display:none;">
                      			<#if form.listType=="Form" >
                      				<input type="hidden" id="${form.key}_id" name="${form.key}_id"/>
                      			</#if>
                      			<#list form.fieldList as field>
                      				<#if !field.editProperties.visible>
                      					<@fieldftl.getFiledView field=field form=form/>
                      				</#if>
                      			</#list>
                      			</div>
                      		</#if>
                      </#list>  
                      <#if tabsList?? && tabsList?size gt 0>
                          <#--分tab页面 -->    
                          <#list tabsList as tabs>
                          	  <#if !dynamicShowTable || (dynamicShowTable && tabs_index lt (tabsList?size-1)) >
                               <#assign tabList= tabs.tabList>
                               <#if tabList?size gt 0>
	                               <#if tabs_index gt 0>
	                           	   <div style="height:10px;"></div>
	                               </#if>
	                           		<#-- 避免二次，二次初始化时不会更新panel面板 -->
                               		<div id="form_tabs_${tabs.rows}">
	                                <#list tabList as tab >
	                                     <#list formList as form>
		                                        <#if form.key==tab.form>
			                                        <#if form.isMaster || form.listType == "Form"><#--主表分tab页面-->
										                <@fieldftl.displayForm form=form tab=tab/>
			                                        <#else>
			                                        	<#if !dynamicShowTable && form.visible ><#-- 非动态显示子表 -->
				                                        <@fieldftl.displayListForm form=form tab=tab/>	
										                </#if>
			                                        </#if>
			                                   </#if>
			                              </#list>
			                        </#list>
		                 			</div>
		                 		</#if>
		                 	  </#if>
		           	</#list>
		           	<#if dynamicShowTable > 
						<#-- 动态显示子表 -->
						<div style="height:10px;"></div>
						<div id="form_tabs_dynamic"></div>    
					</#if>
		       </#if>
        </form>
</div>
         	<div id="mm" class="easyui-menu" style="width:140px;">
				<div id="stInsert" onclick="doInsert()"><s:text name="system.button.insert.title"/></div>
				<div id="stCopy" onclick="doCopy()"><s:text name="system.button.copy.title"/></div>
				<div id="stCopyAfter" onclick="doCopy(true)"><s:text name="复制到下一行"/></div>
				<div id="stMoveTo" onclick="doMoveTo()"><s:text name="移动至第几行之前"/></div>
				<div id="stMoveUp" onclick="doMoveUp()"><s:text name="system.button.moveUp.title"/></div>
				<div id="stMoveDown" onclick="doMoveDown()"><s:text name="system.button.moveDown.title"/></div>
				<div id="stDelete" onclick="doDelete()"><s:text name="system.button.delete.title"/></div>
			</div>
			<div id="mm_treeGrid" class="easyui-menu" style="width:120px;">
				<div id="stInsert_tree" onclick="doTreeInsert()"><s:text name="system.button.insert.title"/></div>
				<div id="stMoveUp_tree" onclick="doTreeMoveUp()"><s:text name="system.button.moveUp.title"/></div>
				<div id="stMoveDown_tree" onclick="doTreeMoveDown()"><s:text name="system.button.moveDown.title"/></div>
				<div id="stDelete_tree" onclick="doTreeDelete()"><s:text name="system.button.delete.title"/></div>
			</div>
			<@fieldftl.initButtonMenuOnViewPage formList=formList /> 	
			
     <div id="subTableDetailInfo" class="easyui-dialog" title="<s:text name="system.button.ctabledetail.title"/>" style="width:800px;height:450px;"
			closed="true">
		<iframe id="ifr_subtable" name="ifr_subtable" scrolling="auto" frameborder="0"  src="" style="width:100%;height:100%;overflow:hidden;"></iframe>
	</div>
	<div id="cancelReasonWin" closable="false" minimizable="false" maximizable="false" collapsible="false" closed="true" title="<s:text name="system.module.invalidReason.title"/>" >							
								<div class="easyui-layout" fit="true">	
									<div region="center" border="false" style="padding:1px;">
										<textarea style="text-align: left; width: 98%;height:73%;" name="cancelReason" id ="cancelReason" ></textarea>				
									</div>
									<div region="south" border="false" style="text-align:right;">
										<a class="easyui-linkbutton" iconCls="icon-ok" href="javascript:void(0)" onclick="doInvalidData();"><s:text name="system.button.ok.title"/></a> 
										<a class="easyui-linkbutton" iconCls="icon-cancel" href="javascript:void(0)" onclick="$('#cancelReasonWin').window('close');"><s:text name="system.button.cancel.title"/></a>
									</div>
								</div>	
	</div> 
	<#-- 新版easyui在初始化layout时，将会查询children中的form元素，如存在则初始化对form兄弟元素无效 -->
	<div style="display:none;">
	<form method="post" name="hidFrm" id="hidFrm" >
		<textarea style="display:none;" name="hid_queryParams" id="hid_queryParams">${r"${param.queryParams}"}</textarea><#-- 查询参数 -->
		<textarea style="display:none;" name="hid_addParams" id="hid_addParams">${r"${param.addParams}"}</textarea><#-- 新增参数 -->
		<input type="hidden" name="hid_relevanceModule" id="hid_relevanceModule" value="${entityName}"/><#-- 关联模块 -->
	</form>
	</div>
	<div id="_dv_approved_" style="position: absolute;top:26px;right:48px;display:none;z-index:99999;">
		<img src="${r"${imgPath}/approve.png"}" width="50px" alt="已审" />
	</div>
	<%@ include file="/common/meta-js.jsp" %>
	<script type="text/javascript" src="${r"${ctx}"}/gs/gs-mng!loadJs.action?entityName=${entityName}&jf=viewJs&id=${r"${param.id}"}&operMethod=${r"${operMethod}"}&initOperMethod=${r"${param.initOperMethod}"}&isEdit=${r"${param.isEdit}"}&isChangeTitle=${r"${param.isChangeTitle}"}&isChange=${r"${param.isChange}"}&afterSaveFunc=${r"${param.afterSaveFunc}"}&ntuId=${r"${param.ntuId}"}"></script>
	<@fieldftl.initFormCssOrJavascript formList=formList eventKey="referJsOrCss"/>
</body>
</html>
