<%@ include file="/common/meta-html5-v2.jsp"%>
<%-- 
<script src="${ctx}/jslib/bootstrap/bootstrap.min.js"></script>
<script src="${ctx}/jslib/bootstrap/bootbox.min.js"></script>
<script src="${ctx}/jslib/bootstrap/toastr.min.js"></script>
<script src="${ctx}/jslib/index/metisMenu.min.js"></script>
<script src="${ctx}/jslib/index/jquery.slimscroll.min.js?v1.0.0"></script>
<script src="${ctx}/jslib/index/pace.min.js?v1.0.0"></script>
<script src="${ctx}/jslib/index/contabs.min.js?v1.2.1"></script>
<script src="${ctx}/jslib/common/common.js?v1.3.11"></script>

<script src="${ctx}/jslib/index/index.min.js?v1.8.0"></script>
<script src="${ctx}/jslib/common/es-core.js?1.6.64"></script>
<script src="${ctx}/jslib/common/es-tool.js?1.2.24"></script>
<script src="${ctx}/jslib/layui/layui-es.js"></script>
<script src="${ctx}/jslib/requirejs/require.js"></script>--%>
<!-- jquery js -->
<script src="${ctx}/js/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
<script src="${ctx}/js/jquery/jquery-migrate-1.4.1.min.js" type="text/javascript"></script>
<script src="${ctx}/js/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${ctx}/js/layer/layer.js" type="text/javascript"></script>

<script type="text/javascript" src="${ctx}/js/datepicker/${locale}_WdatePicker.js"></script>
<script type="text/javascript" src="${ctx}/js/dist/${jsVersion}/app-v2.min.js"></script>
<script type="text/javascript" src="${ctx}/js/easyui-1.4/locale/easyui-lang-${locale}.js"></script>
<!--common i18n -->
<script type="text/javascript" src="${ctx}/js/common/scripts/locale/data-${locale}.js"></script>
<script type="text/javascript" src="${ctx}/js/dist/${jsVersion}/app.min.js"></script>
<script type="text/javascript" src="${ctx}/js/dist/${jsVersion}/app-index.min.js"></script>
<%--
<script src="${ctx}/js/metismenu/metisMenu.min.js"></script>
<script src="${ctx}/js/index/jquery.nicescroll.min.js"></script>
<script src="${ctx}/js/index/contabs.min.js?v1.2.1"></script>
<script src="${ctx}/js/index/index.min.js?v1.8.0"></script> --%>
