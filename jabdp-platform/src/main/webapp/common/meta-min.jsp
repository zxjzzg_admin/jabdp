<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
<meta http-equiv="Cache-Control" content="no-store"/>
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<link rel="icon" type="image/x-icon" href="${ctx}/favicon.ico"></link> 
<link rel="shortcut icon" type="image/x-icon" href="${ctx}/favicon.ico"></link>
<%@ include file="/common/meta-html5-v2.jsp"%>
<!-- jquery js -->
<script src="${ctx}/js/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
<script src="${ctx}/js/jquery/jquery-migrate-1.4.1.min.js" type="text/javascript"></script>
<link href="${cssPath}/main.css" rel="stylesheet" type="text/css"/>
<!-- toastr -->
<link rel="stylesheet" href="${ctx}/js/toastr/toastr.min.css">
<script src="${ctx}/js/toastr/toastr.min.js"></script>
