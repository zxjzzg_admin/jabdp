<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<%@ page import="org.springframework.security.web.WebAttributes" %>
<%@ page import="org.springframework.security.web.authentication.session.SessionAuthenticationException" %>
<%@ page import="org.springframework.security.core.AuthenticationException" %>
<%@ page import="com.qyxx.platform.sysmng.utils.Constants" %>
<%@ page import="com.qyxx.platform.common.utils.spring.SpringContextHolder" %>
<%@ page import="com.qyxx.platform.gsc.utils.SystemParam" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%
	SystemParam systemParam = SpringContextHolder.getBean("systemParam");
	String systemLoginUrl = systemParam.getSystemLoginUrl();
	if(StringUtils.isNotBlank(systemLoginUrl)) {
		response.sendRedirect(request.getContextPath() + "/" + systemLoginUrl);
	}
%>
<!DOCTYPE html>
<html>
<head>
	<title><%=systemParam.getSystemTitle()%></title>
	<%@ include file="/common/meta-min.jsp" %>
	<link rel="stylesheet" type="text/css" href="${ctx}/js/easyui/blue/easyui.css"/>
	<script type="text/javascript" src="${ctx}/js/easyui/scripts/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="${ctx}/js/easyui/scripts/locale/easyui-lang-zh_CN.js"></script>
	<link href="${ctx}/js/loadmask/jquery.loadmask.css" rel="stylesheet" type="text/css"/>
	<script type="text/javascript" src="${ctx}/js/loadmask/jquery.loadmask.min.js"></script>
	<script type="text/javascript" src="${ctx}/js/common/scripts/common.js"></script>
	<link href="${cssPath}/login.css" rel="stylesheet" type="text/css"/>
	<script type="text/javascript">
		//平台、设备和操作系统
		var system = {
			win : false,
			mac : false,
			xll : false
		};
		//检测平台
		var p = navigator.platform;
		system.win = p.indexOf('Win') == 0;
		system.mac = p.indexOf('Mac') == 0;
		system.x11 = (p == 'X11') || (p.indexOf('Linux') == 0);
		//跳转语句
		if(!(system.win||system.mac||system.xll)) {//转向手机登陆页面
			window.location.href = "${ctx}/login-mobile.action";
		}
		<s:if test="#parameters.timeout!=null">
			if(window.top!=window) {
				window.top.document.location.replace("${ctx}/login.action?timeout=true");
			}
		</s:if>
		function doCheckUserIsLogin(userName, userPass) {
			var flag = false;
			var options = {
					async:false,
					url : '${ctx}/sys/account/online-user!checkUserIsLogin.action',
					data : {
						"userName" : userName,
						"userPass" : userPass
					},
					success : function(data) {
						var fm = data.msg;
						if(fm) {
							switch(fm.flag) {
								case "0":
									var cMsg = "<div>" + fm.msg + "。是否确认将其强制下线？</div>";
									$.messager.confirm('该用户已经登录', cMsg, function(r){
										if (r){
											doRemoveOnlineUser(userName);
										} else {
											$("#j_username").val("");
											$("#j_password").val("");
											$("#j_username").focus();
										}
									});
									break;
								case "2":
									$(".user_login_msg").text(fm.msg);
									break;
								default:
									flag = true;
							}
						}
					}
			};
			fnFormAjaxWithJson(options);
			return flag;
		}

		function doRemoveOnlineUser(userName) {
			var options = {
					async:false,
					url : '${ctx}/sys/account/online-user!removeLoginUser.action',
					data : {
						"userName" : userName
					},
					success : function(data) {
						$("#loginForm").unbind("submit");
						$("#loginForm").submit();
					}
			};
			fnFormAjaxWithJson(options);
		}
		
		$(document).ready(function() {
			<%
				AuthenticationException authException = (AuthenticationException)session.getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
				if(authException!=null && authException instanceof SessionAuthenticationException) {
					String errorMsg = authException.getMessage();
			%>
					$(".user_login_msg").text("<%=errorMsg%>");
			<%
				}
			%>
			
			$("#loginForm").submit(function() {
				var userName = $("#j_username").val();
				if(!userName) {
					$(".user_login_msg").text("请输入用户名");
					$("#j_username").focus();
					return false;
				}
				var pass = $("#j_password").val();
				if(!pass) {
					$(".user_login_msg").text("请输入登录密码");
					$("#j_password").focus();
					return false;
				}
				return doCheckUserIsLogin(userName, pass);
			});

			$("#j_username").focus();
		});
	</script>
</head>
<%
	String username = "";
    Cookie[] cs = request.getCookies();
	if(null!=cs) {
		for(Cookie ck : cs) {
			if(Constants.USER_NAME_COOKIE.equals(ck.getName())) {
				username = java.net.URLDecoder.decode(ck.getValue(), Constants.DEFAULT_ENCODE);
				break;
			}
		}
	}
%>
<body id="userlogin_body">
<form id="loginForm" action="${ctx}/j_spring_security_check" method="post">
<div id="user_login">
<dl>
  <dd id="user_top">
  <ul>
    <li class="user_top_l"></li>
    <li class="user_top_c"></li>
    <li class="user_top_r"></li>
  </ul>
  <ul class="user_top_title">
	<li>
		<img src="${imgPath}/<%=systemParam.getSystemLogo()%>" style="height:40px;vertical-align:top;"/>
		<span><%=systemParam.getSystemTitle()%></span>
	</li>
  </ul>
  </dd>
  <dd id="user_main">
  <ul>
    <li class="user_main_l"></li>
    <li class="user_main_c">
		<div class="user_main_box">
		<ul>
		  <li class="user_main_text"><label for="j_username"><s:text name="system.login.username.title"/></label></li>
		  <li class="user_main_input"><input type='text' id='j_username' name='j_username' class="txtusernamecssclass"
						<s:if test="#parameters.error!=null">
							value='<%=session.getAttribute(WebAttributes.LAST_USERNAME)%>'</s:if>
						<s:else>value='<%=username%>'</s:else>	/>
		  </li>
		</ul>
		<ul>
		  <li class="user_main_text"><label for="j_password"><s:text name="system.login.password.title"/></label></li>
		  <li class="user_main_input">
		  	<input type='password' id='j_password' name='j_password' class="txtpasswordcssclass"/>
		  </li>
		</ul>
		<ul>
		  <li class="user_main_text"><label for="request_locale"><s:text name="system.login.language.title"/></label></li>
		  <li class="user_main_input">
		  	<s:select labelposition="top" cssStyle="width:165px;" theme="simple"
					name="request_locale" id="request_locale"
					listKey="value" listValue="displayName"
	      			list="%{@com.qyxx.platform.sysmng.dictmng.web.DefinitionCache@getSet('SYS_LOCALE_DICT','zh_CN')}">
	      		</s:select>
		  </li>
		</ul>
		<ul>
			<li>
				<div class="user_login_msg">
					<s:if test="#parameters.error!=null">
						<span><s:text name="system.login.error.userpass.title"/></span>&nbsp;&nbsp;
					</s:if>
					<s:if test="#parameters.timeout!=null">
						<span><s:text name="system.login.error.timeout.title"/></span>&nbsp;&nbsp;
					</s:if>
				</div>
			</li>
		</ul>
		</div>
	</li>
    <li class="user_main_r">
		<input class="ibtnentercssclass" id="bt_login" type="image" src="${imgPath}/user_botton.gif" name="bt_login" />
	</li>
  </ul>
  </dd>
  <dd id="user_bottom">
  <ul>
    <li class="user_bottom_l"></li>
    <li class="user_bottom_c"></li>
    <li class="user_bottom_r"></li>
  </ul>
  <ul class="user_bottom_title">
	<li><span><%=systemParam.getSystemPowerBy()%></span></li>
  </ul>
 </dd>
 </dl>
</div>
</form>
</body>
</html>

