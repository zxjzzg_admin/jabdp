<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<title><s:text name="system.index.title"/></title>
	<%@ include file="/common/meta-gs.jsp" %>
<script type="text/javascript">
     //定义一个全局变量保存操作状态
      	var operMethod = "${operMethod}";
	    var _userList = {};


	function doInitForm(data, callFunc) {
		if (data.msg) {
			var jsonData = data.msg;
			if(jsonData.level==2){
				$("#level1").hide();
				$("#level3").hide();
			}else if(jsonData.level==1){	
				$("#level2").hide();
				$("#level3").show();
				$("#layoutVal").val(jsonData.layoutVal);
			}
			if(!jsonData["parentId"]) {
				jsonData["parentId"] = "0";
			}
			if(operMethod=="edit"){
				$('#viewForm').form('load', jsonData);
			}else {
				$('#viewForm').form('load', jsonData);
				// initTreeChecked(jsonData.roleIds);
			}
		}

		if (callFunc) {
			callFunc();
		}
	}



	$(document).ready(function() {
		parentDesktopTree();
		// initRoleZtree();
		if (operMethod == "add") {
			//新增 
			$('#tadd').hide();
			$('#tedit').hide();
			$('#tcopy').hide();
			$("#level3").hide();
			$('#tsave').show();
			focusFirstElement();
			$("#viewform").form("validate");
		} else if (operMethod == "view") {
			//查看
			doModify("${param.id}", doDisabled);
			//$('#tsave').hide();
			$('#tedit').show();
		} else if (operMethod == "edit") {
			//复制
			doModify("${param.id}", doEnable);
			$("#tsave").show();
			$('#tcopy').hide();
			$('#tadd').hide();
			$('#tedit').hide();
		}
	});

 	//构造Ztree角色
	/**
	var setting = {
		check : {
			enable : true
		},
		data : {
			simpleData : {
				enable : true
			}
		}
	};
 	

	//用户初始化
	function initRoleZtree() {
		var org_id = $("#orgnizationId").val();
		var options = {
			//url : '${ctx}/sys/account/role!roleList.action', 
			url : '${ctx}/sys/account/user!treeList.action',
			async:false,
			data : {
				"newValue" : org_id
			},
			success : function(data) {
				if (data.msg) {
					$.fn.zTree.init($("#roleTree"), setting, data.msg);
				}
			}
		};
		fnFormAjaxWithJson(options, true);
	} 
	**/
	


  //设置已有的角色为选中
/* 	  function initTreeChecked(roleIds) {
		if (roleIds && roleIds.length > 0) {
			var u_zTree = $.fn.zTree.getZTreeObj("roleTree");
			if(u_zTree){
				$.each(roleIds, function(k, v) {
					var node = u_zTree.getNodeByParam("id", "r_" + v);
					// console.log(node);
					if (node) {
						u_zTree.checkNode(node, true, true);
					} 
				});
			}
		}
	}  

 //将选择的角色id字符串传到后台保存到数据库中
	function getIds() {
		var u_zTree = $.fn.zTree.getZTreeObj("roleTree");
		var u_nodes = u_zTree.getCheckedNodes(true);
		var u_nodeIds = [];
		$.each(u_nodes, function(k, v) {
			u_nodeIds.push(v.idNum);
		});
		$("#roleListIds").val(u_nodeIds.join(","))

	} */
 
 
	 //根据级别展示一颗树
    function parentDesktopTree() {
       	var options = {
             url : '${ctx}/sys/desktop/desktop!desktopTree.action',
             success : function(data) {
            	 comnbotreeInit(data.msg,"parentId",{"id":"id","text":"title","pid":"parentId"}, function(node) {
            		if(node.id != "0") {
            			getLevelBydesktopId(node.id, function(id,level){
   	            		 if(id>0){
   	            			 if(level==1){
   	                			 $("#level2").show();
   	                			 $("#level1").hide();
   	                		 }else if(level==2){
   	                			 $("#level1").hide();
   	                			 $("#level2").hide();
   	                		 }else if(level==3){ 
   	                			 $("#parentId").combotree('setValue',"");
   	                			 $.messager.alert('<s:text name="system.javascript.alertinfo.errorInf"/>','父节点不能是第三级别，请重新选择!','warning');
   	                			
   	                		 }
   	            		 }else if(id==0){
   	            			 $("#level1").show();
   	            			 $("#level2").hide();
   	            		 }	
   	            	  });
            		} else {
            			$("#level1").show();
	            		$("#level2").hide();
            		}
            	 },{"id":"0","text":"最顶级","pid":"-1"});
             },
             async:false
        };
        fnFormAjaxWithJson(options,true);  
   } 

	//设置焦点
	function focusFirstElement() {
		focusEditor("title");
	}

	//将控件设置成可编辑状态
	function setFormToEdit() {
		var status = $("#status").val();
		if (status == "1") {
			$('#release').hide();
			$('#repeal').show();
		} else if (status == "0") {
			$('#release').show();
			$('#repeal').hide();
		}
		$('#tsave').show();
		$('#tadd').hide();
		$('#tedit').hide();
		$('#tcopy').hide();
		operMethod = "modify";
		doEnable();
		focusFirstElement();
	}

	function doCopyMainTab() {
		var id = $("#id").val();
		if (id == null) {
			$.messager
					.alert(
							'<s:text name="system.javascript.alertinfo.title"/>',
							'<s:text name="system.javascript.alertinfo.copy"/>',
							'info');
			return;
		} else {
			parent.addTab(
					'<s:text name="system.sysmng.desktop.copy.title"/>-' + id,
					'${ctx}/sys/desktop/desktop!edit.action?id=' + id);
		}
	}

	//页面数据初始化
	function doModify(id, callFunc) {
		var options = {
			url : '${ctx}/sys/desktop/desktop!queryDesktop.action',
			data : {
				"id" : id
			},
			success : function(data) {
				doInitForm(data, callFunc);
			}
		};
		fnFormAjaxWithJson(options, true);
	}

	function saveObj() {
		//getIds();
		if (operMethod == "edit" || operMethod == "add") {
			$("#id").val("");
		}
			var options = {
				url : '${ctx}/sys/desktop/desktop!save.action',
				success : function(data) {
					doModify(data.msg, doAfterSave);
					$('#tsave').hide();
					$('#tcopy').show();
					$('#repeal').show();
					$('#tadd').show();
					$('#tedit').show();
					$('#tattach').show();
					doDisabled();

				}

			};
			fnAjaxSubmitWithJson("viewForm", options);
			
	

	}

	function doRepeal(st) {
		if (st == "1") {
			$("#status").val("");
			$("#status").val("1");
		} else if (st == "0") {
			$("#status").val("");
			$("#status").val("0");

		}
		if (operMethod == "edit" || operMethod == "add") {
			$("#id").val("");
		}

		var options = {
			url : '${ctx}/sys/notice/news!repeal.action',
			success : function(data) {
				doModify(data.msg, doAfterSave);

			}

		};
		fnAjaxSubmitWithJson("viewForm", options);
		$('#tcopy').show();
		$('#repeal').hide();
		$('#release').hide();
		doDisabled();
	}

	function doAfterSave() {
		if (parent.refreshTabData) {
			parent.refreshTabData("<s:text name='system.sysmng.desktop.manager.title'/>");
		}
		doDisabled();
	}

	function doEnable() {
		$("input[type!=hidden],textarea,select").removeAttr("disabled");
		$("input.easyui-combobox").combobox('enable');
		//只有在新增和复制的情况下才可以修改父桌面	
		if(operMethod=="modify" || operMethod=="view"){
			$("#parentId").combobox('disable');
		}		
	}

	//将页面上的控件置为不可编辑状态
	function doDisabled() {
		$("input[type!=hidden],textarea,select").attr("disabled", "disabled");
		$("input.easyui-combobox").combobox('disable');
	}

	function formClear() {
		$("#viewForm")[0].reset();
	}

	//查看里的新增
	function doAdd() {
		$('#tadd').hide();
		$('#tedit').hide();
		$('#tsave').show();
		$('#tcopy').hide();
		$('#release').show();
		operMethod = "add";
		doEnable();
		formClear();
		$("#status").val("0");
		$("#showVlaue").html("草稿");
	}
</script>
</head>
<body class="easyui-layout" fit="true" title="maintable-view">
	<div region="north" style="overflow: hidden;" border="false">
		<div class="datagrid-toolbar">
			<a id="tsave" href="javascript:saveObj();" class="easyui-linkbutton"
				plain="true" iconCls="icon-save"><s:text name="system.button.save.title"/></a>
			<c:if test="${operMethod=='view'}">
				<a id="tclose"
					href="javaScript:window.parent.closeTab('<s:text name="system.sysmng.desktop.view.title"/>-${param.id}')"
					class="easyui-linkbutton" plain="true" iconCls="icon-cancel"><s:text name="system.button.close.title"/> </a>
			</c:if>
			<c:if test="${operMethod=='add'}">
				<a id="tclose"
					href="javaScript:window.parent.closeTab('<s:text name="system.sysmng.desktop.add.title"/>')"
					class="easyui-linkbutton" plain="true" iconCls="icon-cancel"><s:text name="system.button.close.title"/> </a>
			</c:if>
			<c:if test="${operMethod=='edit'}">
				<a id="tclose"
					href="javaScript:window.parent.closeTab('<s:text name="system.sysmng.desktop.copy.title"/>-${param.id}')"
					class="easyui-linkbutton" plain="true" iconCls="icon-cancel"><s:text name="system.button.close.title"/> </a>
			</c:if>

			<a id="tadd" href="javaScript:doAdd();" class="easyui-linkbutton"
				plain="true" iconCls="icon-add"><s:text name="system.button.add.title"/></a> <a id="tedit" href="#"
				onclick="setFormToEdit();" class="easyui-linkbutton" plain="true"
				iconCls="icon-edit"><s:text name="system.button.modify.title"/></a> <a id="tcopy"
				href="javaScript:doCopyMainTab();" class="easyui-linkbutton"
				plain="true" iconCls="icon-copy"><s:text name="system.button.copy.title"/></a>
		</div>
	</div>


	<div region="center" style="position: relative; overflow: auto;" border="false">
		<form action="" method="post" name="viewForm" id="viewForm"
			style="margin: 0px;">
			<input type="hidden" id="id" name="id" /> 
			<input type="hidden" id="version" name="version"/>
			<table border="0" cellpadding="0" cellspacing="1" class="table_form">
				<tr>
					<th><label><s:text name="system.sysmng.desktop.modelTitle.title"/></label>
					</th>
					<td><input type="text" name="title" id="title"
						class="easyui-validatebox Itext"  style="width: 400px"/>
					</td>
					<th><label><s:text name="system.sysmng.desktop.key.title"/></label>
					</th>
					<td><input type="text" name="keyworlds" id="keyworlds"
						class="easyui-validatebox Itext"  style="width: 400px"/>
					</td>
				</tr>
				<tr>
					<th><label><s:text name="system.sysmng.desktop.fontNum.title"/></label></th>
					<td><input type="text" name="fontNum" id="fontNum"
						class="easyui-numberbox"  /></td>
						<th><label><s:text name="system.sysmng.desktop.displayNum.title"/></label>
					</th>
					<td><input type="text" name="displayNum" id="displayNum"
						class="easyui-numberbox Itext" />
					</td>
				</tr>
				<tr>
				<th><label><s:text name="system.sysmng.desktop.father.title"/></label></th>
					<td><input class="easyui-combotree" name="parentId"
						id="parentId" style="width: 200px;" ></input></td>
					<th><label><s:text name="system.sysmng.desktop.height.title"/></label></th>
					<td><input type="text" name="height" id="height"
						class="easyui-numberbox"  />
					</td>
				</tr>
				<tr>
					<th><label><s:text name="system.sysmng.desktop.moduleUrl.title"/></label>
					</th>
					<td><input type="text" name="moduleUrl" id="moduleUrl"
						class="easyui-validatebox Itext"  style="width: 400px"/>
					</td>
					<th><label><s:text name="system.sysmng.desktop.dataUrl.title"/></label>
					</th>
					<td><input type="text" name="dataUrl" id="dataUrl"
						class="easyui-validatebox Itext"  style="width: 400px"/>
					</td>
				</tr>
				<tr>
					<th><s:text name="system.sysmng.desktop.content.title"/></th>
					<td colspan="3"><textarea id="source" name="source" style="width: 100%;height: 200px"></textarea>
					</td>
				</tr>
				<tr>
					<th><label><s:text name="system.sysmng.desktop.sourceType.title"/></label>
					</th>
					<td colspan="3"><select id="sourceType" class="easyui-combobox" name="sourceType" 
											style="width: 200px;" >
							<option value="fixed" selected="selected">固定值</option>
							<option value="sql">sql语句</option>
							<option value="proc">存储过程</option>
							<option value="java">java代码</option>
					</select> 
					</td>
					
					<!--  <th><label for="roleName"><s:text name="system.sysmng.user.setRole.title"/>:</label>
					</th>
					<td><input type="hidden" name="roleListIds" id="roleListIds" />
						<input type="hidden" id="orgnizationId" value="${orgId}" />
						<ul id="roleTree" class="ztree"></ul></td>
					 -->	
						
				</tr>
				<tr id="level1">
					<th><label><s:text name="system.sysmng.desktop.layoutType.title"/></label>
					</th>
					<td colspan="3"><select id="layoutType" class="easyui-combobox"
						name="layoutType" style="width: 200px;"  >
							<option value="90%" selected="selected">(90%)一列</option>
							<option value="45%;45%">(45%;45%)两列</option>
							<option value="30%;60%">(30%;60%)两列</option>
							<option value="30%;30%;30%">(30%;30%;30%)3列</option>
							<option value="20%;40%;20%">(20%;40%;20%)3列</option>
					</select></td> 
					<!-- <th><label>实际布局</label></th>
					<td><input type="text" name="layoutVal"
						id="layoutVal" style="width: 200px;" ></input></td> -->
				</tr>
				<tr id="level2">
					 <th><label><s:text name="system.sysmng.desktop.desktopType.title"/></label>
					</th>
					 <td colspan="3"><select id="type" class="easyui-combobox" name="type" 
						style="width: 200px;" >
						    <option value="toDoList">待办事宜</option>
							<option value="textList" selected="selected">文字列表</option>
							<option value="picTextList">图文类型</option>
							<option value="picList">图片类型</option>
							<option value="datagrid">datagrid列表类型</option>
							<option value="statistic1">统计分析类型1</option>
							<option value="statistic2">统计分析类型2</option>
							<option value="weather">天气预报类型</option>
							<option value="datetime">日期时间类型</option>
							<option value="iframe">iframe类型</option>
							<option value="textSlides">文字滚动</option>
					</select></td> 
			   </tr>
			   <tr id="level3">
					 <th><label><s:text name="system.sysmng.desktop.layoutVal.title"/></label>
					</th>
					<td colspan="3"><input type="text" name="layoutVAL" id="layoutVal"
						class="easyui-validatebox Itext"  style="width: 400px"/>
					</td>
			   </tr>
			</table>
		</form>
	</div>
</body>
</html>
