<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
 <title><s:text name="system.index.title"/></title>
 <%@ include file="/common/meta-css.jsp"%>
</head>
<body>
<c:choose>
		<c:when test="${param.multiple=='true'}">
<div class="easyui-layout" style="width:100%;height:100%;" data-options="fit:true">
        <div data-options="region:'west',border:false" style="width:48%;padding:4px;">
           <div class="easyui-layout" data-options="fit:true">
			 <div data-options="region:'center',border:false">
			  <table id="_datalist_" ></table>
			 </div>
			</div>
        </div>
        <div data-options="region:'center',border:false,split:true" style="width:48%;padding:4px;">
        	<div class="easyui-layout" data-options="fit:true">
			 <div data-options="region:'center',border:false">
			  <table id="_datalist_sel_" ></table>
			 </div>
			</div>
        </div>
</div>
<div id="tb" style="padding:2px 5px;">
        <input class="easyui-searchbox" data-options="prompt:'请输入关键字查询',searcher:doSearch" style="width:100%;"></input>
        <a href="#" class="easyui-linkbutton" iconCls="icon-add" title="移至右边" plain="true" onclick="doAddAllSel();">移至选中列表</a>
        <span style="color:red;font-size:10px;">【双击数据可以移至选中列表】</span>
</div>
<div id="tb1" style="padding:2px 5px;">
        <input class="easyui-searchbox" data-options="prompt:'请输入关键字查询',searcher:doSearchSel" style="width:100%;"></input>
        <a href="#" class="easyui-linkbutton" iconCls="icon-remove" title="全部清空" plain="true" onclick="doClearAllSel();">全部清空</a>
        <span style="color:red;font-size:10px;">【双击移除选中数据】</span>
</div>
</c:when>
        <c:otherwise>
<div class="easyui-layout" style="width:100%;height:100%;" data-options="fit:true">
        <div data-options="region:'west',border:false" style="width:96%;padding:4px;">
           <div class="easyui-layout" data-options="fit:true">
			 <div data-options="region:'center',border:false">
			  <table id="_datalist_" ></table>
			 </div>
			</div>
        </div>
</div>
<div id="tb" style="padding:2px 5px;">
        <input class="easyui-searchbox" data-options="prompt:'请输入关键字查询，【双击可以选择数据】',searcher:doSearch" style="width:100%;"></input>
</div>
        </c:otherwise>
        </c:choose>
<div style="display:none;">
<textarea id="_query_params_">${param.queryParams}</textarea>
</div>
<%@ include file="/common/meta-js.jsp"%>
<script type="text/javascript">
var items = [];
var scond = "";//其他值查询条件
var idcond = "";//唯一值查询条件
var filter = {
	entityName : "${param.entityName}",
	style : "${param.style}"
};
//获取值
function getValue() {
	<c:choose>
	<c:when test="${param.multiple=='true'}">
	var rows = $("#_datalist_sel_").datagrid("getRows");
	</c:when>
	<c:otherwise>
	var rows = $("#_datalist_").datagrid("getSelections");
	</c:otherwise>
	</c:choose>
	if(rows && rows.length) {
		var keys = [], captions = [];
		for(var i in rows) {
			var row = rows[i];
			keys.push(row["id"] || row["key"]);
			captions.push(row["text"] || row["caption"]);
		}
		return {
			key : keys.join(","),
			id : keys.join(","),
			caption : captions.join(","),
			text: captions.join(",")
		};
	} else {
		return null;
	}
}
function initSearchCond(items) {
	var arr = [];
	for(var i in items) {
		if("hidden" != items[i].order && "disquery" != items[i].order) {
			if(items[i].columntype != "long") arr.push(items[i].column);
		}
	}
	scond = "filter_LIKES_" + arr.join("_OR_");
	var str = "filter_";
	for(var i in items) {
		if(items[i].key == 'key' || items[i].key == 'id') {
			if(items[i].columntype=="long") {
				str = str + 'INL_' + items[i].column;
			} else {
				str = str + 'INS_' + items[i].column;
			}
			break;
		}
	}
	idcond = str;
}
//初始化结果窗
function initDatagrid(items) {
	var filters = $.extend({}, filter);
	<c:if test="${param.multiple!='true'}">
	var idKey = "${param.key}";
	if(idKey) {
		filters[idcond] = idKey;
	}
	</c:if>
	var queryParams = $("#_query_params_").val();
	if(queryParams) {
		$.extend(filters, JSON.parse(queryParams));
	}
	var fields = [{
		field : "ck",
		checkbox : true
	}];
	for(var i in items) {
		var field = {
			field : items[i].key,
			title : items[i].caption,
			width : items[i].width ? items[i].width : 100
		};
		if(items[i].order == 'hidden') field.hidden = true;
		fields.push(field);
	}
	$("#_datalist_").datagrid({
		title:"请选择数据",
		fit:true,
		url : "${ctx}/gs/gs-mng!queryFilterResult.action",
		queryParams : filters,
		<c:if test="${param.multiple!='true'}">
		singleSelect : true,
		</c:if>
		pagination : true,
		pageList:[10,20,30,40,50,100,200,500],
		pageSize:10,
		rownumbers :true,
		striped : true,	
		nowrap : false, // 截取
		idField : 'key',
		columns : [fields],
		toolbar:'#tb',
		onDblClickRow:function(index, row) {
			<c:choose>
			<c:when test="${param.multiple=='true'}">
			doAddSelToSelList([row]);
			$('#_datalist_').datagrid('unselectRow',index);
			</c:when>
			<c:otherwise>
			$("#_datalist_").datagrid("selectRow",index);
			window.parent.$("#_form_win_ifr_").parent().parent().parent().find("div.modal-footer button:first").trigger("click");
			</c:otherwise>
			</c:choose>
		}
	});
}

function doSearch(value) {
	var qp = $.extend({}, filter);
	qp[scond] = value;
	var queryParams = $("#_query_params_").val();
	if(queryParams) {
		$.extend(qp, JSON.parse(queryParams));
	}
	$("#_datalist_").edatagrid("load", qp);
}

<c:if test="${param.multiple=='true'}">
//搜索选择数据
function doSearchSel(value) {
	if(value) {
		var selRows = $("#_datalist_sel_").datagrid("getRows");
		var fields = $("#_datalist_sel_").datagrid("getColumnFields");
		if(selRows && selRows.length) {
			$.each(selRows, function(i, row) {
				for(var j in fields) {
					var field = fields[j];
					if(field != "id" && field != "key") {
						var fieldVal = String(row[field]);
						if(fieldVal.indexOf(value) >= 0) {
							$("#_datalist_sel_").datagrid("selectRow", i);
							break;
						}
					}
				}
			});
		}
	}
}

function doAddAllSel() {
	var rows = $("#_datalist_").datagrid("getSelections");
	if(rows.length) {
		doAddSelToSelList(rows);
	} else {
		$.messager.confirm('提醒', '您当前没有选中任何数据，确认要默认选中当页所有数据么？', function(r){
            if (r){
            	var rows = $("#_datalist_").datagrid("getRows");
            	doAddSelToSelList(rows);
            }
        });
	}
}

function doAddSelToSelList(rows) {
	var selRows = $("#_datalist_sel_").datagrid("getRows");
	var selRowIds = $.map(selRows, function(row) {
		return row["id"]||row["key"];
	});
	for(var i in rows) {
		var row = rows[i];
		if($.inArray(row["id"]||row["key"], selRowIds) == -1) {
			$("#_datalist_sel_").datagrid("appendRow", row);
		}
	}
}

function doClearAllSel() {
	$("#_datalist_sel_").datagrid("loadData", []);
}

function initSelDatagrid(items) {
	var filters = $.extend({}, filter);
	filters[idcond] = "${param.key}" || "0";
	var fields = [{
		field : "ck",
		checkbox : true
	}];
	for(var i in items) {
		var field = {
			field : items[i].key,
			title : items[i].caption,
			width : items[i].width ? items[i].width : 100
		};
		if(items[i].order == 'hidden') field.hidden = true;
		fields.push(field);
	}
	var queryParams = $("#_query_params_").val();
	if(queryParams) {
		$.extend(filters, JSON.parse(queryParams));
	}
	$("#_datalist_sel_").datagrid({
		title:"选中数据",
		fit:true,
		url : "${ctx}/gs/gs-mng!queryFilterResult.action",
		queryParams : filters,
		pagination : false,
		rownumbers :true,
		striped : true,	
		nowrap : false, // 截取
		idField : 'key',
		columns : [fields],
		toolbar:'#tb1',
		onDblClickRow:function(index, row) {
			$("#_datalist_sel_").datagrid("deleteRow", index);
		}
	});
}
</c:if>
$(function(){
	var object = {
			url : "${ctx}/gs/gs-mng!getItems.action",
			data : filter,
			success : function(data) {
				items = data || [];
				initSearchCond(items);
				initDatagrid(items);
				<c:if test="${param.multiple=='true'}">
				initSelDatagrid(items);
				</c:if>
			}
	};
	$.ajax(object);
});
</script>
</body>
</html>