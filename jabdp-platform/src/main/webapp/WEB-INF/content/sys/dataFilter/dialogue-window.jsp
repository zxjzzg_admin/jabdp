<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="/common/meta-css.jsp" %>
	<title><s:text name="system.index.title"/></title>
</head>
<body>
<div class="easyui-layout" fit="true">
 <div region="north" style="height:40px;padding-top:9px" border="false">
  <span style="padding:0px 10px 0px 10px">查询值：</span></span><input id="queryvalue"></input><span style="padding-left:5px;color:red;">注：点击查询按钮可以重置</span>
 </div>
 <div region="center" border="false">
  <table id="_dialoguewindow" fit="true"></table>
 </div>
</div>
<div id="menu" style="width:120px;">
 <div iconCls="icon-ok">所有...</div>
</div>
<div style="display:none;">
<textarea id="_query_params_">${param.queryParams}</textarea>
</div>
<%@ include file="/common/meta-js.jsp" %>
<script type="text/javascript">
var items = [];
var filter = {
	entityName : "${param.entityName}",
	style : "${param.style}"
};
//获取值
function getValue() {
	var row = $("#_dialoguewindow").datagrid("getSelected");
	if(row) {
		return {
			key : row.key,
			caption : row.caption
		};
	} else {
		return null;
	}
}
//初始化结果窗
function initDatagrid() {
	var filters = $.extend({}, filter);
	if("${param.key}"!="") {
		var str = "filter_";
		for(var i in items) {
			if(items[i].key == 'key') {
				if(items[i].columntype=="long") {
					str = str + 'EQL_' + items[i].column;
				} else {
					str = str + 'EQS_' + items[i].column;
				}
				break;
			}
		}
		filters[str] = "${param.key}";
	}
	var queryParams = $("#_query_params_").val();
	if(queryParams) {
		$.extend(filters, JSON.parse(queryParams));
	}
	var fields = [{
		field : "ck",
		checkbox : true
	}];
	for(var i in items) {
		var field = {
			field : items[i].key,
			title : items[i].caption,
			width : items[i].width ? items[i].width : 100
		};
		if(items[i].order == 'hidden') field.hidden = true;
		fields.push(field);
	}
	$("#_dialoguewindow").datagrid({
		url : "${ctx}/gs/gs-mng!queryFilterResult.action",
		queryParams : filters,
		singleSelect : true,
		pagination : true,
		pageList:[10,20,30,40,50,100,200,500],
		pageSize:10,
		rownumbers :true,
		striped : true,	
		nowrap : false, // 截取
		idField : 'key',
		columns : [fields],
		onDblClickRow:function(index, row) {
			$("#_dialoguewindow").datagrid("selectRow",index);
			window.parent.$("#_form_win_").parent().find("div.dialog-button a:first").trigger("click");
		}
	});
}
//初始化查询条件框
function initSearchbox() {
	var menu_str = "";
	for(var i in items) {
		if(items[i].order !== 'hidden' && items[i].order !== 'disquery') {
			menu_str = menu_str + "<div>" + items[i].caption + "</div>";
		}
	}
	$("#menu").append(menu_str);
	$("#queryvalue").searchbox({
		menu : '#menu',
		width : 500,
		searcher : searcher,
		prompt:'请输入查询关键字'
	});
}
function searcher(value, name) {
	var filters = $.extend({}, filter);
	var queryParams = $("#_query_params_").val();
	if(queryParams) {
		$.extend(filters, JSON.parse(queryParams));
	}
	if(value != "") {
		var key = "filter_LIKES";
		if(name == "所有...") {
			for(var i in items) {
				if("hidden" != items[i].order && "disquery" != items[i].order) {
					if(key.indexOf("filter_LIKES_") === 0) {
						key = key + "_OR";
					}
					key = key + "_" + items[i].column;
				}
			}
		} else {
			for(var i in items) {
				if(name == items[i].caption) {
					key = key + "_" + items[i].column;
					break;
				}
			}
		}
		filters[key] = "%" + value + "%";
	}
	$("#_dialoguewindow").datagrid("load", filters);
	$("#_dialoguewindow").datagrid("resize");
}
$(function(){
	var object = {
		url : "${ctx}/gs/gs-mng!getItems.action",
		data : filter,
		success : function(data) {
			if(data && data.length) items = data;
			initDatagrid();
			initSearchbox();
		}
	};
	$.ajax(object);
});
</script>
</body>
</html>