<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
<title><s:text name="system.index.title"/></title>
<%@ include file="/common/meta-gs.jsp" %>
	<script type="text/javascript">
		function getOption() {
			return {
				title:'<s:text name="system.sysmng.onlineUser.list.title"/>',
				iconCls:"icon-search",
				width:600,
				height:350,
				nowrap: false,
				striped: true,
				fit: true,
				url:'${ctx}/sys/account/online-user!queryList.action',
				sortName: 'id',
				sortOrder: 'desc',
				idField:'id',
				frozenColumns:[[
	                {field:'ck',checkbox:true},
	                {title:'<s:text name="system.search.id.title"/>',field:'id',width:50,sortable:true,align:'right'}
				]],
				columns:[[
							{field:'loginUsername',title:'<s:text name="system.sysmng.onlineUser.loginUsername.title"/>',width:80},
							{field:'loginIp',title:'<s:text name="system.sysmng.onlineUser.loginIp.title"/>',width:80},
							{field:'loginMachine',title:'<s:text name="system.sysmng.onlineUser.loginMachine.title"/>',width:80},
							{field:'loginTime',title:'<s:text name="system.sysmng.onlineUser.loginTime.title"/>',width:140, sortable:true, align:'center'},
							{field:'sessionId',title:'<s:text name="system.sysmng.onlineUser.sessionId.title"/>',width:200}
						]],
				pagination:true,
				rownumbers:true,
				toolbar:[{
					id:'bt_del',
					text:'<s:text name="system.button.delete.title"/>',
					iconCls:'icon-remove',
					handler:function(){
						doDelete();
					}
				},'-']
			};
		}
		
		function doDelete() {
			var ids = [];
			var rows = $('#queryList').datagrid('getSelections');
			for(var i=0;i<rows.length;i++){
				ids.push(rows[i].id);
			}
			
			if (ids != null && ids.length > 0) {
				$.messager.confirm('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.info"/>', function(r){
					if (r) {
						var options = {
								url : '${ctx}/sys/account/online-user!delete.action',
								data : {
									"ids" : ids
								},
								success : function(data) {
									$('#queryList').datagrid('clearSelections');
									doQuery();
								},
								traditional:true
						};
						fnFormAjaxWithJson(options);
						}
					});
			} else {
				$.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.question"/>', 'info');
			}
			
		}
		
		function doQuery() {
			var param = $("#queryForm").serializeArrayToParam();
			$("#queryList").datagrid("load", param);
		}
		
		$(document).ready(function() {
			focusEditor("loginUsername");
			doQuseryAction("queryForm");
			$("#queryList").datagrid(getOption());
			$("#loginTimeStart").focus(function() {
				WdatePicker({maxDate:'#F{$dp.$D(\'loginTimeEnd\')}'});
			});
			$("#loginTimeEnd").focus(function() {
				WdatePicker({minDate:'#F{$dp.$D(\'loginTimeStart\')}'})
			});
			$("#bt_query").click(doQuery);
			$("#bt_reset").click(function() {
				//$("#queryForm")[0].reset();
				$("#queryForm").form("clear");
				doQuery();
			});
		});
	</script>
</head>
<body class="easyui-layout" fit="true">
		<div region="west" border="false" title="<s:text name="system.search.title"/>" split="true" style="width:260px;padding:0px;" iconCls="icon-search">
			<form action="" name="queryForm" id="queryForm">
				<table border="0" cellpadding="0" cellspacing="1" class="table_form">
					<tbody>
						<tr><th><label for="loginUsername"><s:text name="system.sysmng.onlineUser.loginUsername.title"/>:</label></th>
							<td><input type="text" class="Itext" name="filter_LIKES_loginUsername" id="loginUsername"></input></td>
						</tr>
						<tr>
							<th><label for="loginTimeStart"><s:text name="system.sysmng.onlineUser.loginTime.title"/>:</label></th>
							<td><input type="text" name="filter_GED_loginTime" id="loginTimeStart" readonly="readonly" class="Idate Wdate" style="width:160px;"></input>
								<br/>--<br/><input type="text" name="filter_LED_loginTime" id="loginTimeEnd" readonly="readonly" class="Idate Wdate" style="width:160px;"></input></td>
						</tr>
						<tr>		
							<td colspan="2" align="center">
								<button type="button" id="bt_query"><s:text name="system.search.button.title"/></button>&nbsp;&nbsp;
								<button type="button" id="bt_reset"><s:text name="system.search.reset.title"/></button>
							</td>
						</tr>
					</tbody>
				</table>
			</form>
		</div>
		<div region="center" title="" border="false">
			<table id="queryList" border="false"></table>
		</div>
</body>
</html>

