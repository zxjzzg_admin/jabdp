<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>CKEditor Demo</title>
	<%@ include file="/common/meta.jsp" %>
	<script type="text/javascript" src="${ctx}/js/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="${ctx}/js/ckeditor/adapters/jquery.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			var config = {
					toolbar:
					[
						['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink'],
						['UIColor', 'Preview']
					],
					customConfig : 'config_en.js'
			};

			// Initialize the editor.
			// Callback function can be passed and executed after full instance creation.
			$('#editor1').ckeditor(config);
			
			$("#bt_demo").click(function() {
				$.messager.alert('<s:text name="system.javascript.alertinfo.titleInfo"/>', $("#editor1").val() ,'info');
			});
		});
	</script>
</head>
<body class="easyui-layout">
		<div region="center" title="CKEditor Demo" style="overflow:hidden;">
			<textarea class="jquery_ckeditor" cols="80" id="editor1" name="editor1" rows="10"></textarea>
			<button type="button" id="bt_demo">测试</button>
		</div>
</body>
</html>

