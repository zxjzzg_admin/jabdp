<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<%@ include file="/common/meta.jsp"%>
	<link type="text/css" href="${ctx}/js/jquery-ui/blue/jquery-ui-1.8.23.custom.css" rel="stylesheet"/>
	<style type="text/css">
.button {
	display: inline-block;
	outline: none;
	cursor: pointer;
	text-align: center;
	text-decoration: none;
	font: 16px/100% 'Microsoft yahei',Arial, Helvetica, sans-serif;
	padding: .5em 2em .55em;
	text-shadow: 0 1px 1px rgba(0,0,0,.3);
	-webkit-border-radius: .5em; 
	-moz-border-radius: .5em;
	border-radius: .5em;
	-webkit-box-shadow: 0 1px 2px rgba(0,0,0,.2);
	-moz-box-shadow: 0 1px 2px rgba(0,0,0,.2);
	box-shadow: 0 1px 2px rgba(0,0,0,.2);
}
.button:hover {
	text-decoration: none;
}
.button:active {
	position: relative;
	top: 1px;
}
/* white */
.white {
	color: #606060;
	border: solid 1px #b7b7b7;
	background: #fff;
	background: -webkit-gradient(linear, left top, left bottom, from(#fff), to(#ededed));
	background: -moz-linear-gradient(top,  #fff,  #ededed);
	filter:  progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffff', endColorstr='#ededed');
}
.white:hover {
	background: #ededed;
	background: -webkit-gradient(linear, left top, left bottom, from(#fff), to(#dcdcdc));
	background: -moz-linear-gradient(top,  #fff,  #dcdcdc);
	filter:  progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffff', endColorstr='#dcdcdc');
}
.white:active {
	color: #999;
	background: -webkit-gradient(linear, left top, left bottom, from(#ededed), to(#fff));
	background: -moz-linear-gradient(top,  #ededed,  #fff);
	filter:  progid:DXImageTransform.Microsoft.gradient(startColorstr='#ededed', endColorstr='#ffffff');
}
</style>
	</head>
<body>
		<div style="overflow:hidden;padding:5px 5px;width:90%;text-align:center;">
			<input id="userSel" style="width:82%;height:36px;" placeholder="请输入关键字查询人员"/>
			<input type="hidden" name="userId" id="userId" /> 
			<a class="button white" href="javascript:void(0);" onclick="doDelegate();" ><s:text name="system.button.submit.title"/></a>
		</div>	
<script type="text/javascript" src="${ctx}/js/jquery-ui/scripts/jquery-1.8.0.min.js"></script>
	<script type="text/javascript" src="${ctx}/js/jquery-ui/scripts/jquery-ui-1.8.23.custom.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			/*var _userList=findAllUser();
			var _userSelList = [];
			$.each(_userList, function(k, v) {
				var obj = {};
				obj["label"]= v.realName + "(" + v.loginName + ")[" + v.orgName + "]-"+v.nickName;
				if("${param.taskId}"){
					obj["value"]= v.loginName;
				}else{
					obj["value"]= v.id;
				}				
				_userSelList.push(obj);
			});
			//初始化用户集合
			$("#userSel").autocomplete({
				source: _userSelList,
				minLength: 1,
				select: function( event, ui ) {
					$("#userSel").val( ui.item.label);
					$("#userId").val( ui.item.value );
					return false;
				}
			});*/
			
			$( "#userSel" ).autocomplete({
			      source: function( request, response ) {
			        $.ajax({
			          url: "${ctx}/sys/account/user!queryUserListMobile.action",
			          dataType: "json",
			          type : "post",
			          data: {
			              filter_LIKES_loginName_OR_realName_OR_nickname_OR_employeeId : request.term,
			              filter_EQS_status : "1",
			        	  page:1,
			        	  rows:10
			          },
			          success: function( data ) {
			            response( $.map( data.msg, function( item ) {
			              	item["label"] = item.nickName + "【" + item.orgName + "】";
			              	if("${param.taskId}"){
			              		item["value"] = item.loginName;
			              	} else {
			              		item["value"] = item.id;
			              	}
			              	return item;
			            }));
			          }
			        });
			      },
			      minLength: 1,
			      select: function( event, ui ) {
			    	  $("#userSel").val( ui.item.label);
					  $("#userId").val( ui.item.value );
					  return false;
			      }
			 });
		});

		var tid ="${param.taskId}";
		//设置任务转派
		function doDelegate(){
			var uId = $('#userId').val();
			if(uId) {
				var options = {
						url : '${ctx}/gs/process!delegateUser.action',
						data : {
							"userId":uId,
							"taskId":tid
						},
						success : function(data) {
							if (data.msg) {
								alert('任务转派成功！');
								doClose();
							}				
						}
					};
					fnFormAjaxWithJson(options,true);
			} else {
				alert('请输入转派人员');
			}
		} 
		function getUserId(){
			var uId = $('#userId').val();
			return uId;
		}
		function doClose() {
			var partenW = window.parent;
            var parentDiv = partenW.document.getElementById('contextForOper');     
            var parentModel = partenW.justep.Bind.contextFor(parentDiv).$model;  
			parentModel.closePage();
		}
	</script>		
</body>
</html>