<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="/common/meta-css.jsp"%>
</head>
<body>
<div class="easyui-layout" fit="true">
 	<div region="north" border="false">
		<div class="datagrid-toolbar">
			<a id="batch_agree" class="easyui-linkbutton" iconCls="icon-ok" plain="true"
					href="javascript:void(0)" onclick="doBatchAgree()" title="批量同意">批量同意</a>
		</div>		
	</div>
	<div region="center" border="false">
		<div id="main_tabs_div" fit="true" border="false">
		</div>
	</div>
</div>	
<div style="display:none;">
	<input type="hidden" id="taskIds" name="taskIds" value="${param.taskIds}" />
	<input type="hidden" id="moduleName" name="moduleName" value="${param.moduleName}" />
</div>
<%@include file="/common/meta-js.jsp"%>	
<script type="text/javascript">
function closeCurrentTab() {
	var tab = $("#main_tabs_div").tabs('getSelected');
	if(tab) {
		var tabIndex = $("#main_tabs_div").tabs("getTabIndex", tab);
		$("#main_tabs_div").tabs('close', tabIndex);
	}
}
function addTab(moduleName, url, taskId, selected) {
	var title = moduleName + "-" + taskId;
	if ($('#main_tabs_div').tabs('exists',title)){
		$('#main_tabs_div').tabs('select', title);
	} else {
		var content = [];
		content.push("<iframe scrolling='auto' frameborder='0' ");
		content.push(" style='width:100%;height:100%;'");
		content.push(" ></iframe>");
		$("#main_tabs_div").tabs("add",{
			title: title,
			content:content.join(""),
			closable:true,
			cache:false,
			//selected:(selected || false),
			strUrl:url,
			bkUrl:url,
			taskId:taskId
		});
	}	
}

function initTab() {
	$("#main_tabs_div").tabs({
		/* onSelect:function(title) {
			var tb = $('#main_tabs_div').tabs('getTab',title);
			if(tb) {
				var opt = tb.panel("options");
				var strUrl = opt.strUrl;
				var tb_ifr = tb.panel("body").find("iframe");
				if(tb_ifr) {
					var win = tb_ifr[0].contentWindow.window;
					win.location.replace(strUrl);
				}
			}
		}, */
		onBeforeClose: function(title){
			var tb = $('#main_tabs_div').tabs('getTab',title);
			if(tb) {
				var opt = tb.panel("options");
				var taskId = opt.taskId;
				var obj = $("#main_tabs_div").data("param");
				obj[taskId] = false;
			}
			return true;
		}
	});
	
	var obj = {};
	var moduleName = $("#moduleName").val();
	var tids = $("#taskIds").val();
	var taskIdArr = tids.split(",");
	var tUrl = "${ctx}/gs/process!getProcess.action?taskId=";
	for(var i in taskIdArr) {
		var tid = taskIdArr[i];
		obj[tid] = true;
		var url = tUrl + tid;
		if(i == 0) {
			addTab(moduleName, url, tid, true);
		} else {
			addTab(moduleName, url, tid);	
		}
	}
	$("#main_tabs_div").data("param", obj);
	
	var tabsOpt = $("#main_tabs_div").tabs("options");
	tabsOpt.onSelect = function(title) {
		var tb = $('#main_tabs_div').tabs('getTab',title);
		if(tb) {
			var opt = tb.panel("options");
			var strUrl = opt.strUrl;
			if(strUrl) {
				var tb_ifr = tb.panel("body").find("iframe");
				if(tb_ifr) {
					var win = tb_ifr[0].contentWindow.window;
					win.location.replace(strUrl);
					opt.strUrl = null;
				}	
			}
		}
	};
	$("#main_tabs_div").tabs("select",0);
}

//根据标题获取tab页窗口对象
function getTabDataWin(title) {
	var obj = null;
	var tb = $('#main_tabs_div').tabs('getTab',title);
	if(tb) {
		var tbby = tb.panel("body");
		if(tbby) {
			var tb_ifr = tbby.find("iframe");
			if(tb_ifr) {
				var win = tb_ifr[0].contentWindow.window;
				obj = win;
			}
		}
	}
	return obj;
}

function doBatchAgree() {
	var ids = [];
	var obj = $("#main_tabs_div").data("param");
	for(var i in obj) {
		if(obj[i]) {
			ids.push(i);
		}
	}
	if(ids.length) {
		jwpf.doBatchAgreeProcess(ids, null, function() {
			if(window.top.closeCurrentTab) {
				window.top.closeCurrentTab();
			}
		});
	} else {
		alert("无待办事宜需要操作！");
	}
}

	$(document).ready(function() {
		initTab();	
	});

 </script>	
</body>
</html>