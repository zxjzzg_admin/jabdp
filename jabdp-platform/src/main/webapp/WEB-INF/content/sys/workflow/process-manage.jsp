<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title><s:text name="system.sysmng.process.processDefinitionMng.title"/></title>
<%@ include file="/common/meta-gs.jsp"%>

<script type="text/javascript">

	$(document).ready(function() {
		$("#queryList").datagrid(getOption());
		$("#bt_query").click(doQuery);
		$("#bt_reset").click(function() {
			$("#queryForm").form("clear");
			doQuery();
		});
	});
	function doQuery() {
		var param = $("#queryForm").serializeArrayToParam();
		$("#queryList").datagrid("load", param);
	}
	//流程管理
	function getOption() {
		return {
			iconCls:"icon-search",
			width:600,
			height:350,
			border:false,
			nowrap: false,
			striped: true,
			fit: true,
			sortName: 'version',
			sortOrder: 'desc',
			url:'${ctx}/gs/process!processInstanceHistory.action',
			frozenColumns : [ [ {
				field : 'ck',
				checkbox : true
			}] ],
			columns:[[
						{field:'moduleName',title:'<s:text name="system.sysmng.process.moduleName.title"/>',width:80},
						{field:'name',title:'<s:text name="system.sysmng.process.processName.title"/>',width:100},
						{field:'key',title:'<s:text name="system.sysmng.process.processKey.title"/>',width:80},
						{field:'version',title:'<s:text name="system.sysmng.process.processVersion.title"/>',width:50, sortable:true},
						{field:'description',title:'<s:text name="system.sysmng.process.processDescription.title"/>',width:80},
						{field:'resourceName',title:'<s:text name="system.sysmng.process.processResourceName.title"/>',width:150},
						{field:'dgrmResourceName',title:'<s:text name="system.sysmng.process.processDgrmResourceName.title"/>',width:150},
						{
							field : 'oper',
							title : '<s:text name="system.button.oper.title"/>',
							width : 250,
							align : 'left',
							formatter : operFormatter
						}
						]],
						toolbar : [ {
							id : 'bt_del',
							text : '<s:text name="system.button.delete.title"/>',
							iconCls : 'icon-remove',
							handler : function() {
								var rows = $('#queryList').datagrid('getSelections');
								if(rows.length<=0 ){								
								return	$.messager.alert('<s:text name="system.javascript.alertinfo.errorInfo"/>','<s:text name="system.javascript.alertinfo.recordInfo"/>','info');
								}
								$.messager.confirm(
									'<s:text name="system.javascript.alertinfo.title"/>',
									'<s:text name="system.javascript.alertinfo.info"/>',
									function(r) {
										if (r) {											
											for ( var i = 0; i < rows.length; i++) {
												doDel(rows[i].deploymentId);
											}
										}
									});
				}
			} ],
			pagination : true,
			rownumbers : true,

			onDblClickRow : function(rowIndex, rowData) {
				doView(rowData.deploymentId);
			}
		};
	}

	function operFormatter(value, rowData, rowIndex) {
		var strArr = [];
		strArr.push('<a href="javascript:void(0);" onclick="doView(');
		strArr.push(rowData.deploymentId);
		strArr
				.push(')"><s:text name="system.sysmng.process.processDgrmResourceName.title"/></a>&nbsp;<a href="javascript:void(0);" onclick="doDel(');
		strArr.push(rowData.deploymentId);
		strArr.push(')"><s:text name="system.button.delete.title"/></a>');
		return strArr.join("");
	}
	function doView(deploymentId) {
		top.addTab(
				'<s:text name="system.sysmng.process.processDgrmResourceName.title"/>-'
						+ deploymentId,
				'${ctx}/sys/process/deploy!view.action?deploymentId='
						+ deploymentId);
<%--var content = [];
		content.push("<iframe src='${ctx}/sys/process/deploy!view.action?deploymentId=");
		content.push(deploymentId);
		content.push("' frameborder='0' style='width:100%;height:100%'></iframe>");
		$("#tt").tabs("add", {
			title : '<s:text name="流程图"/>-' + deploymentId + '',
			content : content.join(""),
			closable : true,
			cache : false
		});--%>
	}
	function doDel(deploymentId){
		var options = {
				url : '${ctx}/sys/process/deploy!delete.action',
				data : {
					"deploymentId" :deploymentId
				},
				success : function(data) {
					if (data.msg) {
						$("#queryList").datagrid(getOption());
					}
				}
			};
		fnFormAjaxWithJson(options);
	}
</script>
</head>
<body class="easyui-layout">
	<div region="west" border="false" title="<s:text name="system.search.title"/>" split="true" style="width:260px;padding:0px;" iconCls="icon-search">
		<form action="" name="queryForm" id="queryForm">
				<table border="0" cellpadding="0" cellspacing="1" class="table_form">
					<tbody>
						<tr><th><label for="key"><s:text name="system.sysmng.process.processKey.title"/>:</label></th>
							<td><input type="text" name="filter_LIKES_key" id="key" class="Itext"></input></td>
						</tr>
						<tr>		
							<td colspan="2" align="center">
								<button type="button" id="bt_query"><s:text name="system.search.button.title"/></button>&nbsp;&nbsp;
								<button type="button" id="bt_reset"><s:text name="system.search.reset.title"/></button>
							</td>
						</tr>
					</tbody>
				</table>
			</form>
	</div>
	<div region="center" title="<s:text name="system.sysmng.process.searchProcessInfo.title"/>" border="false">
	<%-- 	
	<div id="tt" class="easyui-tabs" fit="true" border="false">
		<div  title="<s:text name="流程信息"/>" > --%>
		<table id="queryList"></table>
	<%-- </div>
		</div>--%>
	</div>
</body>
</html>