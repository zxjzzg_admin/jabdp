<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<title><s:text name="system.index.title"/></title>
	<%@ include file="/common/meta-gs.jsp" %>
	<%-- <link href="${ctx}/js/easyui/${themeColor}/panel.css" rel="stylesheet" type="text/css"/>--%>
    <script type="text/javascript" src="${ctx}/js/easyui-1.4/easyui.xpPanel.js"></script>
</head>
<body class="easyui-layout" fit="true">
 <div region="west" title="<s:text name='system.search.title'/>"  border="false" 
  split="true" style="width:215px;padding:0px;" 
  iconCls="icon-search" tools="#pl_tt" >
  <form action="" name="queryForm" id="queryForm">
   <div class="easyui-accordion" data-options="multiple:true" style="width:98%;" id="queryFormId">
	<div class="xpstyle" title="<s:text name="主题"/>" collapsible="true" collapsed="true">
	 <input type="text" name="filter_LIKES_b.title" id="title" class="Itext"/>
	</div>
	<div class="xpstyle" title="<s:text name="提醒日期"/>" collapsible="true" collapsed="true">
	 <input name="filter_GED_b.createTime" id="remindDateStart" class="Idate Wdate" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})"></input>
	 <br/>--<br/>
	 <input name="filter_LED_b.createTime" id="remindDateEnd" class="Idate Wdate" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})"></input>
	</div>
	<div class="xpstyle" title="<s:text name="提醒状态"/>" collapsible="true" collapsed="true">
	 <select class="easyui-combobox" name="filter_LIKES_a.status" id="status" >
	  <option value>全部</option>
	  <option value="0">等待提醒</option>
	  <option value="1">正在提醒</option>
	  <option value="2">过期提醒</option>
	 </select>
	</div>
	</div>
	 <div style="text-align:center;padding:8px 8px;">
	<button type="button" id="bt_query" class="button_small">
	 <s:text name="system.search.button.title"/>
	</button>
	&nbsp;&nbsp;
	<button type="button" id="bt_reset" class="button_small">
	 <s:text name="system.search.reset.title"/>
	</button>
	</div>
   </div>
  </form>
 </div>
 <div region="center" title="" border="false">
  <table id="queryList" border="false"></table>
 </div>
 <div id="mm" class="easyui-menu" style="width:120px;">
  <div onclick="doCmView()" iconCls="icon-search"><s:text name="system.button.view.title"/></div>
 </div>
<script type="text/javascript">
var _userList = {};
function getOption() {
    return {
        width : 'auto',
        height : 'auto',
        nowrap : false,
        striped : true,
        fit : true,
        url :'${ctx}/sys/memo/memo!queryMemoToUserList.action',
        sortName : 'b.createTime',
        sortOrder : 'desc',
        frozenColumns : [
            [
                {
                    field : 'ck',
                    checkbox : true
                }
            ]
        ],
        columns : [
            [
				{
				    field : 'mtuId',
				    sortable : true,
				    hidden:true
				    
				} ,
                {
                     field : 'title',
                     title : '主题',
                     width : 200,
                     sortable : true
                 } ,
                 {
                     field : 'remindType',
                     title : '定时提醒类型',
                     width : 100,
                     sortable : true
                 } ,
                 {
                     field : 'remindTime',
                     title : '提醒时间',
                     width : 100,
                     sortable : true
                 } ,
                 {
                     field : 'remindDate',
                     title : '提醒日期',
                     width : 100,
                     sortable : true,
                     formatter:function(value,rowData,rowIndex) {
                    	 if(value) {
							value = value.replace("00:00:00","");
                    	 }
                     	 return value;
                     }
                 },
                 {
                     field : 'remindWeek',
                     title : '周提醒日',
                     width : 200,
                     sortable : true,
                     formatter:function(value,rowData,rowIndex) {
                    	 if(value) {
                    		 value = value.replace("1","周一");
                        	 value = value.replace("2","周二");
                        	 value = value.replace("3","周三");
                        	 value = value.replace("4","周四");
                        	 value = value.replace("5","周五");
                        	 value = value.replace("6","周六");
                        	 value = value.replace("7","周日");
                    	 }
                     	 return value;
                     }
                 } ,
                 {
                     field : 'mtuStatus',
                     title : '到期状态',
                     width : 80,
                     sortable : true,
                     formatter:function(value, rowData, rowIndex) {
                    	 if(value == 1) {
                    		 return "正在提醒";
                    	 } else if (value == 2) {
                    		 return "过期提醒";
                    	 } else {
                    		 return "等待提醒";
                    	 }
                     }
                 } ,
                 {
                     field : 'createUser',
                     title : '<s:text name="system.sysmng.desktop.createUser.title"/>',
                     width : 100,
                     sortable : true,
                     formatter:function(value, rowData, rowIndex){
                      	/*var uObj=_userList[value];
                          if(uObj){
                              var nickName = (uObj.nickName)?("-" + uObj.nickName):"";
 								return uObj.realName + "[" + uObj.loginName + "]" + nickName;
                          }else{
                              return value;
                          }*/
                    	 return rowData["createUserCaption"];  
                  	}
                 } ,
                 {
                     field : 'createTime',
                     title : '<s:text name="system.sysmng.desktop.createTime.title"/>',
                     width : 200,
                     sortable : true
                 } 
            ]
        ],
        onDblClickRow:function(rowIndex, rowData){
        	doDblView(rowData.id, rowData.mtuId);
        	$('#queryList').datagrid('unselectRow',rowIndex);
        },
		onRowContextMenu  : function(e, rowIndex, rowData){
			e.preventDefault();
			$("#mm").data("rowIndex", rowIndex);
			$('#mm').data("id", rowData.id);
			$('#mm').data("mtuId", rowData.mtuId);
			$('#mm').menu('show', {
				left: e.pageX,
				top: e.pageY
			});
		},
        pagination : true,
        rownumbers : true,
    };
}
//查询
function doQuery() {
    var param = $("#queryForm").serializeArrayToParam();
    $("#queryList").datagrid("load", param);
}
//右键查看
function doCmView() {
	doDblView($("#mm").data("id"), $("#mm").data("mtuId"));
}
//查看tab
function doDblView(id, mtuId) {
	parent.addTab('<s:text name="查看备忘录"/>-' + id, '${ctx}/sys/memo/memo-query-view.action?operMethod=view&id=' + id + '&mtuId=' + mtuId);
}
//初始化
$(function() {
	//_userList=findAllUser();
	//加载列表数据
	$("#queryList").datagrid(getOption());
	doQuseryAction("queryForm");
	$("#bt_query").click(doQuery);
    $("#bt_reset").click(function() {
        $("#queryForm")[0].reset();
        doQuery();
    });
});
</script>
</body>
</html>
