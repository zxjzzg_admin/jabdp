<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/common/meta-css.jsp"%>
<!-- uploadify -->
<link href="${ctx}/js/uploadify/uploadify-3.1.css" rel="stylesheet"
	type="text/css" />
</head>
<body class="easyui-layout">
	<div region="center" style="padding: 2px 2px;" border="false">
		<div class="file_upload_button">
			<input type="file" name="uploadify_gs" id="file_upload" />
				<button class="upload-button-1 button_big" id="bt_upload_gs"
					type="button"><s:text name="开始导入"/></button>
				<button class="upload-button-2 button_big" onclick="doExportModule();"
					type="button"><s:text name="导出模板"/></button>
		</div>
		<div id="uploadGsQueue" class="file_upload_queue"></div>
		<div style="padding:10px;"><span id="resultDesc" style="color:red;"></span></div>
		<div style="padding:10px;"><span style="color:red;">操作步骤：<br/>1、请单击“导出模板”按钮，导出Excel数据模板；<br/>2、根据Excel数据模板填好数据；<br/>3、单击“选择文件”按钮选择填好的Excel数据模板；<br/>4、单击“开始导入”按钮完成导入操作。</span></div>
		<div style="display:none;"><input type="hidden" name="hid_expName" id="hid_expName" value=""/></div>
	</div>
<%@ include file="/common/meta-js.jsp"%>	
<script type="text/javascript"
	src="${ctx}/js/uploadify/jquery.uploadify-3.1.min.js"></script>
<script type="text/javascript">
var entityName = "${param.entityName}";

function doUploadFile() {	
	$('#file_upload').uploadify('upload','*');
}
function  doExportModule(){
	var expUrl = "${ctx}/gs/gs-mng!outputExcelModule.action?entityName=" + entityName;
	window.open(expUrl);
}

function doExportExpExcel(){
	var expName = $("#hid_expName").val();
	var expUrl = "${ctx}/gs/gs-mng!outputExpExcel.action?entityName=" + entityName + "&value=" + expName;
	window.open(expUrl);
}

$(document).ready(function() {    
			$("#file_upload").uploadify({
								'uploader' : '${ctx}/gs/gs-mng!importModuleExcel.action;jsessionid=<%=session.getId()%>',
								'formData' : {'entityName':entityName},  
								'swf' : '${ctx}/js/uploadify/uploadify-3.1.swf',
								'cancelImg' : '${ctx}/js/uploadify/uploadify-cancel-3.1.png',
								'queueID' : 'uploadGsQueue',
								'auto' : false,
								'multi' : true,
								'width' : 95,
								'height' : 20,
								'buttonClass' : 'uploadify-button-small',
								'fileSizeLimit' : '50MB', //限制上传的文件大小
								'fileTypeExts' : '*.xls', //允许的格式 
								'onUploadSuccess' : function(file,data, response) {
									if(data) {
										var msg = $.parseJSON(data);
										if(msg && msg.flag == "1") {
											if(parent.doRefreshDataGrid) {
												parent.doRefreshDataGrid();
											}
											if(msg.msg) {
												//存在导入异常数据
												$("#hid_expName").val(msg.msg);
												$("#resultDesc").html("部分数据导入失败（由于部分数据重复或者填写不正确），详见<a href='javascript:void(0);' onclick='doExportExpExcel()'>异常数据Excel</a>");
												doExportExpExcel();
											} else {
												$("#resultDesc").html("Excel数据全部导入成功");
											}
										} else {
											$.messager.alert("<s:text name="system.javascript.alertinfo.titleInfo"/>", '文件'
													+ file.name
													+ '导入失败',
													"warning");
										}
									} else {
										$.messager.alert("<s:text name="system.javascript.alertinfo.titleInfo"/>", '文件'
												+ file.name
												+ '导入失败',
												"warning");
									}
								},
								'onUploadError' : function(file,errorCode, errorMsg,errorString) {
									$.messager.alert("<s:text name="system.javascript.alertinfo.titleInfo"/>", '文件'
											+ file.name
											+ '上传失败，错误信息如下：'
											+ errorString,
											"warning");
								},
								'onQueueComplete' : function(queueData) {				
			
								}
							});
	
			$("#bt_upload_gs").click(function(){
				$("#resultDesc").html("");
				$("#hid_expName").val("");
				doUploadFile();
			});
		});
</script>	
</body>
</html>