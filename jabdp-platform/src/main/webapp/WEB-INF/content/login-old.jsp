<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<%@ page import="org.springframework.security.web.WebAttributes" %>
<%@ page import="org.springframework.security.web.authentication.session.SessionAuthenticationException" %>
<%@ page import="org.springframework.security.core.AuthenticationException" %>
<%@ page import="com.qyxx.platform.sysmng.utils.Constants" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><s:text name="system.login.title"/></title>
	<%@ include file="/common/meta-min.jsp" %>
	<link rel="stylesheet" type="text/css" href="${ctx}/js/validate/jquery.tipTip.css"/>
	<script type="text/javascript" src="${ctx}/js/validate/jquery.tipTip.js"></script>
	<link rel="stylesheet" type="text/css" href="${ctx}/js/validate/jquery.validate.css"/>
	<script type="text/javascript" src="${ctx}/js/validate/jquery.validate.js"></script>
	<script type="text/javascript" src="${ctx}/js/validate/locale/messages_${locale}.js"></script>	
	<style type="text/css">
		html{margin:0; padding:0; height:100%;}
	</style>
	<script>
		$(document).ready(function() {
			
			<%
				AuthenticationException authException = (AuthenticationException)session.getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
				if(authException!=null && authException instanceof SessionAuthenticationException) {
					String errorMsg = authException.getMessage();
			%>
					$(".login_msg").text("<%=errorMsg%>");
			<%
				}
			%>
			
			$("#loginForm").validate({
					rules: {
						j_username: "required",
						j_password: "required",
						j_locale: "required"
					}
			});
			
		});
	</script>
</head>
<%
	String username = "";
    Cookie[] cs = request.getCookies();
	if(null!=cs) {
		for(Cookie ck : cs) {
			if(Constants.USER_NAME_COOKIE.equals(ck.getName())) {
				username = java.net.URLDecoder.decode(ck.getValue(), Constants.DEFAULT_ENCODE);
				break;
			}
		}
	}
%>
<body class="login_body">
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="login_td">
    <form id="loginForm" action="${ctx}/j_spring_security_check" method="post">
    <div class="login_div">
    	<ul class="login_list">
        	<li>
            	<label for="j_username"><s:text name="system.login.username.title"/></label>
            	<input type='text' id='j_username' name='j_username' style="width:148px;"
						<s:if test="#parameters.error!=null">
							value='<%=session.getAttribute(WebAttributes.LAST_USERNAME)%>'</s:if>
						<s:else>value='<%=username%>'</s:else>	/>
            </li>
        	<li>
            	<label for="j_password"><s:text name="system.login.password.title"/></label>
            	<input type='password' id='j_password' name='j_password' style="width:148px;"/>
            </li>
        	<li>
            	<label for="j_locale"><s:text name="system.login.language.title"/></label>
            	<s:select labelposition="top" cssStyle="width:154px;" theme="simple"
					name="request_locale" id="request_locale"
					listKey="value" listValue="displayName"
	      			list="%{@com.qyxx.platform.sysmng.dictmng.web.DefinitionCache@getSet('SYS_LOCALE_DICT',#request.locale)}">
	      		</s:select>
            </li>
        </ul>
        <div class="login_msg">
			<s:if test="#parameters.error!=null">
				<span><s:text name="system.login.error.userpass.title"/></span>&nbsp;&nbsp;
			</s:if>
			<s:if test="#parameters.timeout!=null">
				<span><s:text name="system.login.error.timeout.title"/></span>&nbsp;&nbsp;
			</s:if>
		</div>
        <input class="login_bt" type="submit" value="<s:text name="system.login.login.title"/>" id="bt_login"/>
    </div>
    </form>
    </td>
  </tr>
</table>
</body>
</html>

