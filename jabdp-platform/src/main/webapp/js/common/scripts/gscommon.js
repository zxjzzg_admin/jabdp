/***传入相应参数(sql)，返回json结果 */
  function getJsonObjByParam(param)
  {
  	var jsonData=null;
  	var url = param.url || (__ctx+"/gs/gs-mng!queryResult.action");
	var opt = {
			data:param,
			async:false,
            url:url,
			success:function(data){
				jsonData=data.msg;
			}
	};
  	fnFormAjaxWithJson(opt, true);
	return jsonData;
  }
  /*** 查询列表字段翻译 */
  function getColumnValue(id,name,proJson,value)
  {
  	  var res = "";
	  if(proJson){
  		if(value!==undefined && value!==null){
  			var tv=String(value).split(",");
  			if(tv){
  				var tvName = [];
  				for(var i=0;i<proJson.length;i++){
  		      		if($.inArray(String(proJson[i][id]),tv) >= 0){
  		      			tvName.push(proJson[i][name]);
  		      		}
  		      	}
  				res = tvName.join(",");
  			}
  		}
  	  }
	  return res;
  }
  
  /* 查询列表字段翻译 */
  function getColumnValueByKeyCaption(paramObj,proJson,value)
  {
  	if(proJson){
  		if(value!==undefined && value!==null){
  			var tv=String(value).split(",");
  			if(tv){
  				var tvName = [];
  				for(var i=0;i<proJson.length;i++){
  		      		if($.inArray(String(proJson[i][paramObj["key"]]),tv) >= 0){
  		      			tvName.push(proJson[i][paramObj["caption"]]);
  		      		}
  		      	}
  				return tvName.join(",");
  			}
  		}
  	}
  }
  
  /* 查询列表字段翻译 */
  function getColumnValueByIdText(paramObj,proJson,value)
  {
  	if(proJson){
  		if(value!==undefined && value!==null){
  			var tv=String(value).split(",");
  			if(tv){
  				var tvName = [];
  				for(var i=0;i<proJson.length;i++){
  		      		if($.inArray(String(proJson[i][paramObj["id"]]),tv) >= 0){
  		      			tvName.push(proJson[i][paramObj["text"]]);
  		      		}
  		      	}
  				return tvName.join(",");
  			}
  		}
  	}
  }
  
  /**
   * 查询用户列表
   * @returns
   */
  function findAllUser(){
		var jsonData=null;
		$.ajax({
			type:"post",
			async:false,
            url:__ctx+"/sys/account/user!findAllUser.action",
			success:function(data){
				jsonData=data.msg;
				},
			dataType:"json"
		});
		return jsonData;
  } 
  /**
   * 查询公用分类列表
   * 用于列表中的类型字段翻译
   * @returns
   */
  function findAllCommonType(typeUrl){
		var jsonData=null;
		$.ajax({
			type:"post",
			async:false,
            url:__ctx+typeUrl,
			success:function(data){
				jsonData=data.msg;
				},
			dataType:"json"
		});
		return jsonData;
	  
  }
  
  
  /**
   * 树形查询和普通查询切换
   * @param queryFormId
   * @param a_exp_clp
   * @param a_switch_query
   * @param tree_type
   */
/*  function treeShowOrNot(queryFormId,a_exp_clp,a_switch_query,tree_type){
	  $("#"+queryFormId).hide();
		 $("#"+a_exp_clp).hide();
			$("#"+a_switch_query).toggle(function() {
				$("#"+queryFormId).show();
				$("#"+a_exp_clp).show();
				$("#"+tree_type).hide();
			}, function() {
				$("#"+queryFormId).hide();
				$("#"+a_exp_clp).hide();
				$("#"+tree_type).show();
			});
		  $("#"+a_switch_query).show();
		  
		   $("#"+a_exp_clp).toggle(expandAll, collapseAll);
  }
 
	   
  function collapseAll(xpstyle_panel,xpstyle) {
		$("."+xpstyle_panel+" ."+xpstyle).panel("collapse");
		$(this).removeClass("icon-collapse");
		$(this).addClass("icon-expand");
		$(this).attr("title", "全部展开");
	}

	function expandAll(xpstyle_panel,xpstyle) {
		$("."+xpstyle_panel+" ."+xpstyle).panel("expand");
		$(this).removeClass("icon-expand");
		$(this).addClass("icon-collapse");
		$(this).attr("title", "全部收缩");
	}*/
	
	
/**
 * 发送查询公用类型请求
 * @param queryUrl
 * @param treeId
 * @param rootName
 *//*
  function typeTree(queryUrl,treeId,rootName) {
     	var options = {
           url : __ctx+queryUrl,
           success : function(data) {
          	 treeInit(data.msg,treeId,{"id":"id","text":"name","pid":"pid"},doQueryByType,{"id":"0","text":rootName,"pid":""});
          	 
           },
           async:false
      };
      fnFormAjaxWithJson(options,true);  
 } 
  
  *//**
   * 公用类型树状查询
   * @param filterType
   * @param id
   * @param tableId
   *//*
  function doQueryByType(filterType,id,tableId){
				var param = {filterType:id};																                
	              $("#"+tableId).datagrid("load", param);
 } 
*/

  /**
   * 焦点事件
   * @param id
   */
  function focusEditor(id) {
		setTimeout(function() {$("#" + id).focus();}, 0);
	} 
  /**
   * 回车事件监听
   */
  function doQuseryAction(id){
	  //焦点移出控件
	  $(document).bind('keydown', 'return',function (evt){doQuery(); return false; });
		//焦点在控件里面
	 $("#"+id+" input").bind('keydown', 'return',function (evt){doQuery(); return false; });
  }
  
  /**
   * 上传图片
   * @param buttonUploadId
   * @param id
   * @param imageId
   * @returns
   */
  function uploadImage(buttonUploadId,id,imageId,path){
	  $("#"+buttonUploadId).uploadify(
				{
					'uploader' : __ctx+'/sys/attach/upload-file!saveImage.action;',
					'swf' : __ctx+'/js/uploadify/uploadify-3.1.swf',
					'auto' : true,
					'multi' : false,
					'width' : 20,
					'height' : 20,
					'buttonClass' : 'uploadify-button-add',
					'fileSizeLimit' : '10MB', //限制上传的文件大小
					'fileTypeExts' : '*.jpg;*.JPG;*.gif;*.GIF;*.png;*.PNG;',
					'queueID'     : true,    //控制进度条是否显示
					'onUploadSuccess' : function(file,
							data, response) {
						 var obj = $.parseJSON(data);
				            if(obj) {
				            	if(obj.flag=='1') {
				            		//$.messager.alert('提示', '上传成功！','info');
				            		$("input[name="+ id +"]").attr("value",obj.msg);
				            		$("#"+imageId).attr("src",path+obj.msg);
				            		//alert(path+obj.msg);
				            		
				                } else {
				                	$.messager.alert($.jwpf.system.alertinfo.titleInfo, $.jwpf.system.alertinfo.uploadFailed + obj.msg + '。'+$.jwpf.system.alertinfo.tryAgain,'warning');
				                }
				            } else {
				            	$.messager.alert($.jwpf.system.alertinfo.titleInfo,$.jwpf.system.alertinfo.serverException,'warning');
				            }
					},
					'onUploadError' : function(file,
							errorCode, errorMsg,
							errorString) {
						$.messager.alert($.jwpf.system.alertinfo.titleInfo,$.jwpf.system.alertinfo.file
								+ file.name
								+ $.jwpf.system.alertinfo.failedInfo
								+ errorString,
								"warning");
					}
									
				}); 
  }
	
 	
  /**
   * 转换成json格式，用于子表生成chexkbox
   * @param data
   * @param obj
   * @returns {___anonymous1809_1831}
   */
  function transCheckboxData(data,obj){
	  if(!obj) {
	  		obj = {};
	  		obj["key"] = "key";
	  		obj["caption"] = "caption";
	  	}
		return {"data":data,"obj":obj};
	   
  }
  
  /**
   * 转换json格式，用于子表生成radiobox
   * @param data
   * @param obj
   * @returns {___anonymous2118_2140}
   */
  function transRadioboxData(data,obj){
	  if(!obj) {
	  		obj = {};
	  		obj["key"] = "key";
	  		obj["caption"] = "caption";
	  	}
	  return {"data":data,"obj":obj};
  }

  /**
   * 转换json格式数据，用于子表生成combotree
   * @param data
   * @param obj
   * @param isAutoCollapse 自动折叠
   * @returns {___anonymous2609_2652}
   */
  function transCombotreeData(data,obj,isAutoCollapse){
	  if(data && data.length) {
		   var treeData = [];
		   $.each(data, function(k, v) {
			   var node = {};
			   $.each(obj, function(m, n) {
				   node[m] = v[n];
			   });
			   if(isAutoCollapse) {
				   node["state"] = "closed";
			   }
			   treeData.push(node);
		   });
		   var data2 = transData(treeData, "id", "pid", "children");
		   return {"data":data2,"obj":obj,"treeData":treeData};
	   }
  }
  
  /**
   * 转换combogrid需要的json格式，用于子表生成combogrid
   * @param data
   * @param obj
   * @returns {___anonymous3530_3579}
   */
  function transCombogridData(data,obj){
	   var data2=[];
	   if(data && data.length){
		  $.each(data,function(k,v){
			  var obj2={};
			  $.each(obj,function(m,n){
				  obj2[m]=v[n["column"]];
			  });
			  data2.push(obj2);
		  });
	   }
	   
	   var fields = [];
	   $.each(obj,function(m,n){
		   var field = {width:80};
		   field["field"] = m;
		   field["title"] = n["caption"];
		   if(m == "key") {
			   field["hidden"] = true;
		   }
		   fields.push(field);
	   });
	   
	   var fColumns=[];
	   var field={};
	   field["field"]="ck";
	   field["checkbox"]=true;
	   fColumns.push(field);
	   
	   var jsonData={
   		   "total":data2.length,                                                      
			   "rows":data2
				        
      };
	   return {"data":jsonData,"obj":fields,"fColumns":fColumns};
  }
  
  /**
   * 转换成json格式,用于子表生成combobox
   * @param data
   * @param obj
   * @returns {___anonymous4063_4088}
   */
  function transComboboxData(data,obj){
	  if(!obj) {
	  		obj = {};
	  		obj["key"] = "key";
	  		obj["caption"] = "caption";
	  	}
	  	var newData = [];
	  	//var selData = {};
	  	//selData[obj.key]="";
	  	//selData[obj.caption]=$.jwpf.system.alertinfo.set;
	  	//newData.push(selData);
		if(data) {
			newData = newData.concat(data);
		}
		return {"data":newData,"obj":obj};
  }
  
  /**
   * 通知类型初始化
   * @param data
   * @param selectid
   * @param obj
   */
  function selectType(data,selectid,obj,callFun){
	  var newDataObj= transTypeData(data,obj);
	 	var $select1=$("#"+selectid);
		$select1.combobox({
			//required: true,
			data:newDataObj.data,
			valueField:newDataObj.obj.key,
			textField:newDataObj.obj.caption,
			//onChange:function(newValue,oldValue){
				onSelect:function(rec){
				if(rec.id=="-1"){
					if(callFun){
						callFun();
					}
				}
			}
		});
		//$select1.combobox("loadData", newDataObj.data);
		//var selVal = $select1.data("selVal");
		/*
		if(selVal) {
			$select1.combobox("setValue", selVal);
			$select1.removeData("selVal");
		}*/
	}
  
  
  /**
   * 通知类型数据转换
   * @param data
   * @param obj
   * @returns {___anonymous5858_5883}
   */
  function transTypeData(data,obj){
	  if(!obj) {
	  		obj = {};
	  		obj["key"] = "key";
	  		obj["caption"] = "caption";
	  	}
	  	var newData = [];
	  	var selData = {};
	  	//selData[obj.key]="";
	  	//selData[obj.caption]="";
	  	//newData.push(selData);
		if(data) {
			newData = newData.concat(data);
		}
	    selData = {};
		selData[obj.key]="-1";
	  	selData[obj.caption]=$.jwpf.system.alertinfo.addType;
		newData.push(selData);
		return {"data":newData,"obj":obj};
  }
  
  
  /**
   * 生成查询checkbox控件
   * @param data
   * @param selectid
   * @param obj
   */
 function queryCheckbox(data,selectid,obj){
	 var newData=transCheckboxData(data,obj);
	   if(data && data.length){
		  $.each(data,function(k,v){
			  var str="<input name=filter_INS_"+selectid+"  type='checkbox' value='"+v[newData.obj["key"]]+"'/>"+v[newData.obj["caption"]];
			  $("#div_"+selectid).append(str);
		  });
	   }
 }
 
 /**
  * 生成查询radiobox控件
  * @param data
  * @param selectid
  * @param obj
  */
 function queryRadiobox(data,selectid,obj){
	  var newData=transRadioboxData(data,obj);
	  if(data && data.length){
		  $.each(data,function(k,v){
			  var str="<input name=filter_INS_"+selectid+" type='radio' value='"+v[newData.obj["key"]]+"'/>"+v[newData.obj["caption"]];
			  $("#div_"+selectid).append(str);
		  });
	   }
 }
  
 /**
  * 主表checkbox初始化
  * @param data
  * @param selectid
  * @param obj
  */
  function addCheckbox(data,selectid,obj){
		var newData=transCheckboxData(data,obj);
	   if(data && data.length){
		  $.each(data,function(k,v){
			  var str="<input name="+selectid+"  type='checkbox' value='"+v[newData.obj["key"]]+"'/>"+v[newData.obj["caption"]];
			  $("#div_"+selectid).append(str);
		  });
	   }
  }
  
  /**
   * 主表radiobox初始化
   * @param data
   * @param selectid
   * @param obj
   */
  function addRadio(data,selectid,obj){
	  var newData=transRadioboxData(data,obj);
	  if(data && data.length){
		  $.each(data,function(k,v){
			  var str="<input name="+selectid+" type='radio' value='"+v[newData.obj["key"]]+"'/>"+v[newData.obj["caption"]];
			  $("#div_"+selectid).append(str);
		  });
	   }
  }
  

    /**主表comboboxsearch初始化*/
    function comboboxsearchinit(data,selectid,obj,callFun){
  	    var newDataObj= transComboboxData(data,obj);
  	 	var $select1=$("#"+selectid);
  		$select1.comboboxsearch({
  			valueField:newDataObj.obj.key,
  			textField:newDataObj.obj.caption
  		});
  		$select1.comboboxsearch("loadData",newDataObj.data);
  		var selVal = $select1.data("selVal");
  		if(selVal) {
  			$select1.comboboxsearch("setValue", selVal);
  			$select1.removeData("selVal");
  		}
  		if(callFun) {
  			callFun();
  		}
  	}

/**
 * 主表combobox初始化
 * @param data
 * @param selectid
 * @param obj
 */
  function selectinit(data,selectid,obj,callFun){
	  var newDataObj= transComboboxData(data,obj);
	 	var $select1=$("#"+selectid);
		$select1.combobox({
			//required: true,
			data:newDataObj.data,
			valueField:newDataObj.obj.key,
			textField:newDataObj.obj.caption
		});
		//$select1.combobox("loadData", newDataObj.data);
		var selVal = $select1.data("selVal");
		if(selVal) {
			$select1.combobox("setValue", selVal);
			$select1.removeData("selVal");
		}
		if(callFun) {
			callFun();
		}
	}
  
  /**初始化自动完成带搜索下拉框，后台过滤数据*/
  function autoCompleteComboBoxSearchInit(sid, param) {
	  var $sel = $("#" + sid);
	  var opt = {
				mode:"remote",
				hasDownArrow:false,
				autoComplete:true,
				delay:400,
				url:__ctx+"/gs/gs-mng!queryAjaxResult.action",
				valueField:"key",
				textField:"caption",
				onBeforeLoad:function(p) {
					//$.extend(p, param);
					/*if(!$(this).data("_afterFirstInit_")) {
						$(this).data("_afterFirstInit_", true);
						return false;
					}*/
					_initSelectParam_(p, this, param);
				}
	  };
	  if(param["formatter"]) {
		  opt["formatter"] = param["formatter"];
		  delete param["formatter"];
	  }
	  $sel.comboboxsearch(opt);
  }
  
  /**
   * 初始化自动完成下拉框，后台过滤数据
   * @param sid
   * @param param
   */
  function autoCompleteSelectInit(sid, param) {
	  var $sel = $("#" + sid);
	  var opt = {
				mode:"remote",
				hasDownArrow:false,
				autoComplete:true,
				delay:400,
				url:__ctx+"/gs/gs-mng!queryAjaxResult.action",
				valueField:"key",
				textField:"caption",
				onBeforeLoad:function(p) {
					//$.extend(p, param);
					/*if(!$(this).data("_afterFirstInit_")) {
						$(this).data("_afterFirstInit_", true);
						return false;
					}*/
					_initSelectParam_(p, this, param);
				}
		  };
	  if(param["formatter"]) {
		  opt["formatter"] = param["formatter"];
		  delete param["formatter"];
	  }
	  $sel.combobox(opt);
  }
  
  function ajaxSelectInit(sid, param) {
	  var $sel = $("#" + sid);
	  var opt = {
				mode:"remote",
				prompt:"请输入关键字查询",
				hasDownArrow:true,
				delay:400,
				url:__ctx+"/gs/gs-mng!queryAjaxResult.action",
				valueField:"key",
				textField:"caption",
				onBeforeLoad:function(p) {
					/*if(!$(this).data("_isFirstInit_")) {
						$(this).data("_isFirstInit_", true);
						return false;
					} else {
						return ajaxSelectParamInit(p, this, param);
					}*/
					return ajaxSelectParamInit(p, this, param);
				}
		  };
	  if(param["formatter"]) {
		  opt["formatter"] = param["formatter"];
		  delete param["formatter"];
	  }
	  if(param.multiple || $sel.prop("multiple")) {
		  $sel.comboboxsearch(opt);
	  } else {
		  $sel.combobox(opt);
	  }
  } 
  
  //初始化请求参数
  function _initSelectParam_(p, obj, param) {
	  $.extend(p, param);
	  var opts = $(obj).combobox('options');
	  if(opts && opts["onSetQueryParam"]) {
		$.extend(true, p, opts["onSetQueryParam"].call(obj, p, opts["rowData"], opts["rowIndex"]));
	  }
  }
  
  function ajaxSelectParamInit(p, obj, param) {
	  _initSelectParam_(p, obj, param);
	  if(p == null || p.q == null){
			var value = $(obj).combobox('getValue');
			if(value){// 修改的时候才会出现q为空而value不为空
				p.kq = "1";
				p.q = value;
				return true;
			}
	  }
	  return true;
  }
  //combo类控件显示值
  function ajaxSelectFormatter(row, fieldKey, param) {
	  var caption = row[fieldKey+"_caption"];
	  if(row["_isFooter_"]) {
		  caption = row[fieldKey] || "";
	  }
	  return (caption !==undefined && caption !== null) ? caption : getAjaxResultByKey(row, fieldKey, param);
  }
  
  function getAjaxResultByKey(row, fieldKey, param) {
	  	var value = row[fieldKey];
	  	if(!value && value !== 0) {
	  		return "";
	  	}
	  	var result=[];
		$.ajax({
			type:"post",
			async:false,
			data:$.extend(true, {}, param, {kq:"1", q:value}),
            url:__ctx+"/gs/gs-mng!queryAjaxResult.action",
			success:function(data){
			   result = $.map(data, function(row) {
				  return row["caption"]; 
			   });
			},
			dataType:"json"
		});
		var res = result.join(",");
	    row[fieldKey+"_caption"] = res;
		return res;
  }
  
  function comboRadioInit(data,selectid,obj,callFun){
	  var newDataObj= transComboboxData(data,obj);
	 	var $select1=$("#"+selectid);
		$select1.comboradio({
			data:newDataObj.data,
			valueField:newDataObj.obj.key,
			textField:newDataObj.obj.caption
		});
		if(callFun) {
			callFun();
		}
  }
  function  comboCheckInit(data,selectid,obj,callFun){
	  var newDataObj= transComboboxData(data,obj);
	 	var $select1=$("#"+selectid);
		$select1.combocheck({
			data:newDataObj.data,
			valueField:newDataObj.obj.key,
			textField:newDataObj.obj.caption
		});	
		if(callFun) {
			callFun();
		}
  }
  function  newRadioBoxInit(data,selectid,obj,callFun){
	  var newDataObj= transComboboxData(data,obj);
	 	var $select1=$("#"+selectid);
		$select1.newradiobox({
			data:newDataObj.data,
			valueField:newDataObj.obj.key,
			textField:newDataObj.obj.caption
		});
		if(callFun) {
			callFun();
		}
  }
  function  newCheckBoxInit(data,selectid,obj,callFun){
	  var newDataObj= transComboboxData(data,obj);
	 	var $select1=$("#"+selectid);
		$select1.newcheckbox({
			data:newDataObj.data,
			valueField:newDataObj.obj.key,
			textField:newDataObj.obj.caption
		});
		if(callFun) {
			callFun();
		}
  }
 /**
   * 转换成json格式,用于生成ztree
   * @param data
   * @param obj
   */
  function  transzTreeData(data,obj){
	  if(data && data.length){
		var treeData = [];
		$.each(data, function(k, v) {
			var node = {};
			$.each(obj, function(m, n) {
					node[m] = v[n];
				});
			treeData.push(node);
		});						
			return treeData;
	  }
	}
  function zTreeInit(data,selectid,obj,callFun){
	   	var _ztree_setting_ = {
				data : {
					key:{
						name :"text"
					},
					simpleData: {
						enable: true,
						idKey: "id",
						pIdKey: "pid"
					}
				}
		};
	  var newDataObj= transzTreeData(data,obj);
	  var zTreeObj = $.fn.zTree.init($("#"+selectid),_ztree_setting_,newDataObj);
	  zTreeObj.expandAll(true);
  }
  //不展开树
  function zTreeInitWithCollapse(data,selectid,obj,callFun){
	   	var _ztree_setting_ = {
				data : {
					key:{
						name :"text"
					},
					simpleData: {
						enable: true,
						idKey: "id",
						pIdKey: "pid"
					}
				}
		};
	  var newDataObj= transzTreeData(data,obj);
	  var zTreeObj = $.fn.zTree.init($("#"+selectid),_ztree_setting_,newDataObj);
   }
  
    function initCommonData(sid,param,obj, callFunc, subCallFunc,isAsync) {
    	if(isAsync!==false){
    		isAsync=true;//异步
    	}
    	var url = param.url || (__ctx+"/gs/gs-mng!queryResult.action");
    	$.ajax({
			type:"post",
			data:param,
			async:isAsync,
			url:url,
			success:function(data){
				if(callFunc) {
					callFunc(data.msg,sid,obj,subCallFunc);
				}
			},
			dataType:"json"
		});
    }
    
    /**
     * 数字控件初始化
     * @param data
     * @param selectid
     * @param obj
     */
    function numberBoxValueInit(data,selectid,obj, callFun) {
    	var newDataObj= transComboboxData(data,obj);
    	var val = "";
    	var data = newDataObj.data;
    	if(data && data.length > 0) {
    		val = data[0][newDataObj.obj.key];
    	}
    	var sid = $("#"+selectid);
    	sid.numberbox('setValue', val);
    	if(callFun) {
			callFun();
		}
    }
    
    /**
     * 文本控件初始化
     * @param data
     * @param selectid
     * @param obj
     */
    function textBoxValueInit(data,selectid,obj, callFun) {
    	var newDataObj= transComboboxData(data,obj);
    	var val = "";
    	var data = newDataObj.data;
    	if(data && data.length > 0) {
    		val = data[0][newDataObj.obj.key];
    	}
    	var sid = $("#"+selectid);
    	sid.val(val);
    	if(callFun) {
			callFun();
		}
    }
	   
	   /** 
	    * json格式转树状结构 
	    * @param   {json}      json数据 
	    * @param   {String}    id的字符串 
	    * @param   {String}    父id的字符串 
	    * @param   {String}    children的字符串 
	    * @return  {Array}     数组 
	    */  
	   function transData(a, idStr, pidStr, chindrenStr){  
	       var r = [], hash = {}, id = idStr, pid = pidStr, children = chindrenStr, i = 0, j = 0, len = a.length;  
	       for(; i < len; i++){  
	           hash[a[i][id]] = a[i];  
	       }  
	       for(; j < len; j++){  
	           var aVal = a[j], hashVP = hash[aVal[pid]];  
	           if(hashVP){  
	               !hashVP[children] && (hashVP[children] = []);  
	               hashVP[children].push(aVal);  
	           }else{  
	               r.push(aVal);  
	           }  
	       }  
	       return r;  
	   }  
	   
	   /**
	    * 根据id查找系统个人桌面
	    * @param id
	    * @param callFun
	    */
	 function getLevelBydesktopId(id,callFun) {
	       	var options = {
	             url : __ctx+'/sys/desktop/desktop!queryLevel.action',
	             data:{
	            	 "id":id
	             },
	             success : function(data) {
	            	 var level = data.msg.level;
	            	 //级别为1隐藏桌面布局和实际布局
	            	//if(level==1){
	            	 if(callFun) {
	            		 callFun(id,level);
	            	 }
	            	//}
	            	 
	             }
	        };
	        fnFormAjaxWithJson(options,true);
	   } 
	  
/**
 * 主表combotree初始化
 * @param data
 * @param selectid
 * @param obj
 */
	   function comnbotreeInit(data,selectid,obj,callFun,pNode){
		 var $select=$("#"+selectid);
		 if(data && data.length) {
			   var treeData = [];
			   $.each(data, function(k, v) {
				   var node = {};
				   $.each(obj, function(m, n) {
					   node[m] = v[n];
				   });
				   treeData.push(node);
			   });
			   var data2 = transData(treeData, "id", "pid", "children");
			   if(pNode){
				   pNode["children"]=data2;
				   data2=[pNode];
			   }
		       //var newData=transCombotreeData(data,obj);
			   $select.combotree({
					  lines:true,
					  editable:true,
					  onClick:function(rec){
						 if(callFun) {
							 callFun(rec);
						 }
						 //getLevelBydesktopId(rec.id,callFun);
					  }
			   });
			   $select.combotree("loadData",data2);
		   }
	   } 
	   
	   /**
	    * 主表combotree初始化，收缩所有节点
	    * @param data
	    * @param selectid
	    * @param obj
	    */
	   	   function comnbotreeInitWithCollapse(data,selectid,obj,callFun,pNode){
	   		   var $select=$("#"+selectid);
	   		 if(data && data.length) {
	   			   var treeData = [];
	   			   $.each(data, function(k, v) {
	   				   var node = {};
	   				   $.each(obj, function(m, n) {
	   					   node[m] = v[n];
	   				   });
	   				   treeData.push(node);
	   			   });
	   			   var data2 = transData(treeData, "id", "pid", "children");
	   			   if(pNode){
	   				   pNode["children"]=data2;
	   				   data2=[pNode];
	   			   }
	   		       //var newData=transCombotreeData(data,obj);
	   			   $select.combotree({
	   					  lines:true,
	   					  editable:true,
	   					  onClick:function(rec){
	   						  if(callFun) {
	   							  callFun(rec);
	   						  }
	   						  //getLevelBydesktopId(rec.id,callFun);
	   					  }
	   				  });
	   			   $select.combotree("loadData",data2);
	   			   $select.combotree("tree").tree("collapseAll"); //折叠所有节点
	   		   }
	   	   } 
	   
	
/**
 * 树状查询初始化
 * @param data
 * @param selectid
 * @param obj
 * @param subCallFunc
 */
	   function treeInit(data,selectid,obj, subCallFunc,pNode,menuId,isCheck){
		   var $select=$("#"+selectid);
		   if(pNode || (data && data.length)){
				   var treeData = [];
				   $.each(data, function(k, v) {
					   var node = {};
					   $.each(obj, function(m, n) {
						   node[m] = v[n];
					   });
					   treeData.push(node);
				   });
				   var data2 = transData(treeData, "id", "pid", "children");
				   if(pNode){
					   pNode["children"]=data2;
					   data2=[pNode];
				   }
				   if(data2 && data2.length>0){
					   $select.tree({
						     lines:true,
							 checkbox:isCheck,
							 onClick:function(node){
									if(subCallFunc) {
										if (node){
											if(node.id ==0) {
												subCallFunc("");
											} else {
												var children = $select.tree('getChildren', node.target);
												var idArray = [node.id];
												$.each(children, function(k,v) {
													idArray.push(v.id);
												});
												subCallFunc(idArray.join(","));
											}
										} 
										
									}
								},	/// /右键单击节点，然后显示上下文菜单
							 onContextMenu: function(e, node){
								 if(menuId){
										e.preventDefault();
										$select.tree('select', node.target);
										$('#'+menuId).menu('show', {
											left: e.pageX,
											top: e.pageY
										});
										var childrens = $select.tree('getChildren', node.target);
										var idArray = [node.id];
										$.each(childrens, function(k,v) {
											idArray.push(v.id);
										});
										$("#"+menuId).data("nodeId",node.id);
										$("#"+menuId).data("childIds",idArray);
								 }
							
							}
						 });
						 $select.tree("loadData",data2);
				   }
		
			   }
		   
	   }
   
	   /**
	    * 树状查询初始化，自动折叠节点
	    * @param data
	    * @param selectid
	    * @param obj
	    * @param subCallFunc
	    */
	   	   function treeInitWithCollapse(data,selectid,obj, subCallFunc,pNode,menuId,isCheck){
	   		   var $select=$("#"+selectid);
	   		   if(pNode || (data && data.length)){
	   				   var treeData = [];
	   				   $.each(data, function(k, v) {
	   					   var node = {};
	   					   $.each(obj, function(m, n) {
	   						   node[m] = v[n];
	   					   });
	   					   treeData.push(node);
	   				   });
	   				   var data2 = transData(treeData, "id", "pid", "children");
	   				   if(pNode){
	   					   pNode["children"]=data2;
	   					   data2=[pNode];
	   				   }
	   				   if(data2 && data2.length>0){
	   					   $select.tree({
	   						     lines:true,
	   							 checkbox:isCheck,
	   							 onClick:function(node){
	   									if(subCallFunc) {
	   										if (node){
	   											if(node.id ==0) {
	   												subCallFunc("");
	   											} else {
	   												var children = $select.tree('getChildren', node.target);
	   												var idArray = [node.id];
	   												$.each(children, function(k,v) {
	   													idArray.push(v.id);
	   												});
	   												subCallFunc(idArray.join(","));
	   											}
	   										} 
	   										
	   									}
	   								},	/// /右键单击节点，然后显示上下文菜单
	   							 onContextMenu: function(e, node){
	   								 if(menuId){
	   										e.preventDefault();
	   										$select.tree('select', node.target);
	   										$('#'+menuId).menu('show', {
	   											left: e.pageX,
	   											top: e.pageY
	   										});
	   										var childrens = $select.tree('getChildren', node.target);
	   										var idArray = [node.id];
	   										$.each(childrens, function(k,v) {
	   											idArray.push(v.id);
	   										});
	   										$("#"+menuId).data("nodeId",node.id);
	   										$("#"+menuId).data("childIds",idArray);
	   								 }
	   							
	   							}
	   						 });
	   						 $select.tree("loadData",data2);
	   						 $select.tree("collapseAll"); //折叠所有节点
	   				   }
	   		
	   			   }
	   		   
	   	   }	   
	  
	   
/**
 * 主表combogrid初始化
 * @param data
 * @param selectid
 * @param obj
 */
	   function combogridInit(data,selectid,obj, callFun){
		   var $sel=$("#"+selectid);
		   var data2=[];
		   if(data && data.length){
			  $.each(data,function(k,v){
				  var obj2={};
				  $.each(obj,function(m,n){
					  obj2[m]=v[n["column"]];
				  });
				  data2.push(obj2);
			  });
		   }
		   
		   var fields = [];
		   $.each(obj,function(m,n){
			   var field = {width:80};
			   field["field"] = m;
			   field["title"] = n["caption"];
			   if(m == "key") {
				   field["hidden"] = true;
			   }
			   fields.push(field);
		   });
		   
		   var fColumns=[];
		   var field={};
		   field["field"]="ck";
		   field["checkbox"]=true;
		   fColumns.push(field);
		   
		   var jsonData={
	    		   "total":data2.length,                                                      
				   "rows":data2
	       };
		   //var newData=transCombogridData(data,obj);
			 $sel.combogrid({
				rownumbers : true,
			    panelWidth:500,
			    fitColumns: true,
			    idField:'key',  
			    textField:'caption',
			    mode:"local",
			    data:jsonData,
			    //frozenColumns:[fColumns],
			    columns:[fields]
			    /**由于datagrid里的选中行做了修改（设置为不选中），在此可以通过点击行来触发事件设置选中值*/
			    /*onClickRow:function(rowIndex, rowData){
			    	var selRows=$sel.combogrid("grid").datagrid("getSelections"); 
			    	if($.inArray(rowData, selRows)>=0){
				        $sel.combogrid("grid").datagrid("unselectRow", rowIndex); 
			        }else{
			    		$sel.combogrid("grid").datagrid("selectRow", rowIndex); 
			    	}
			    }*/
			});  
			//$sel.combogrid("grid").datagrid("loadData", jsonData); 
			var selVal = $sel.data("selVal");
			if(selVal) {
				if($.isArray(selVal)) {
					$sel.combogrid("setValues",selVal);
				} else {
					if(selVal){
						$sel.combogrid("setValues",String(selVal).split(","));
					} else {
						$sel.combogrid("setValues",[]);
					}
				}
				
				$sel.removeData("selVal");
			}
			if(callFun) {
				callFun();
			}
	   }
	   function dataGridInit(data,selectid,obj, callFun){
			var data2=[];
			if(data && data.length){
				$.each(data,function(k,v){
					  var obj2={};
					  $.each(obj,function(m,n){
						  obj2[m]=v[n["column"]];
					  });
					  data2.push(obj2);
				  });
			 }
			
		  	var fields = [];
		   $.each(obj,function(i,n){
			   var field = {width:100};
			   field["field"] = i;
			   field["title"] = n.caption;
			  /* if(i == "key") {
				   field["hidden"] = true;
			   }*/
			   fields.push(field);
		   });	   
		   
		   var fColumns=[];
		   var field={};
		   field["field"]="ck";
		   field["checkbox"]=true;
		   fColumns.push(field);		   
		   var dataList={
	    		   "total":data2.length,                                                      
				   "rows":data2
	       };
		   
		   $('#'+selectid).datagrid({  
			    nowrap: true,
				striped: true,	
				rownumbers:true, 
				frozenColumns:[fColumns],
				columns:[fields]					 							
		   });  
			 $('#'+selectid).datagrid("loadData",dataList);
		}
	   function initDataGridFilterData(dataGridId,jsonData,title){
			var data = jsonData["list"];	
			var mapping = jsonData["map"];
		  	var fields = [];
		  	//加公用属性
		   $.each(mapping,function(i,n){
			   var field = {width:80};
			   field["field"] = n.key;
			   field["title"] = n.caption;
			 /*  if(nameMapping[n.key]){
				   	field["formatter"]= function(value, rowData, rowIndex){
					  var formatArr =  nameMapping[n.key];	   
					   for(var j=0,len=formatArr.length;j<len;j++){
				    		if(formatArr[j].key==value){
				    			return	formatArr[j].caption;
				    		}
					   	} 				    	                                                            
			        };
			   }*/

			   if(n.order == "hidden") {
				  field["hidden"] = true;
			   } else {
				  fields.push(field); 
			   } 
		   });	
		  	//加扩展属性
	/*	  if(nameArry.length){
		   $.each(nameArry, function(i,n) {
			   var field = {width:80};
			   field["field"] = n.key;
			   field["title"] = n.caption;
			   field["formatter"]= function(value, rowData, rowIndex){
				  var formatArr =  n.data;	   
				   for(var j=0,len=formatArr.length;j<len;j++){
			    		if(formatArr[j].key==value){
			    			return formatArr[j].caption;
			    		}
				   	} 				    	                                                            
		       };
			   fields.push(field);
		   		});
	   		}*/
		   var fColumns=[];
		   var field={};
		   field["field"]="ck";
		   field["checkbox"]=true;
		   fColumns.push(field);		   
		   var dataList={
				   "total":data.length,                                                      
				   "rows":data
		   };
		   
		   $('#'+dataGridId).datagrid({  
			   title:	title,
			    nowrap: true,
				striped: true,	
				rownumbers:true,
				frozenColumns:[fColumns],
				columns:[fields]
		   });  
			 $('#'+dataGridId).datagrid("loadData",dataList);
		}
	   
	   var $ES = {
				getCtxPath:function() {
					return __ctx;
				},
				getJqObj:function(obj) {
					if(obj instanceof jQuery) {
						return obj;
					} else {
						return $(obj);
					}
				},
				getJsonObjByParam:function(param) {
					var jsonData=null;
					var url = param.url || (__ctx+"/gs/gs-mng!queryAjaxResult.action");
					var opt = {
							data:param,
							async:false,
							url:url,
							success:function(data){
								if($.isPlainObject(data)) {
									jsonData=data.msg;
								} else {
									jsonData=data;
								}
							}
					};
					fnFormAjaxWithJson(opt, true);
					return jsonData;
				},
				getFilterData : function(sqlKey,filterParam,callback){				 
					 	var param = {
					 		"entityName" :	sqlKey
					 	};
					 	$.extend(param,filterParam);
					  	var jsonData=null;
						var opt = {
								data:param,
								//async:true,
								url:__ctx+"/gs/gs-mng!dataFilter.action",
								success:function(data){
									if(callback) {
										callback(data.msg);
									}
								}
						};
					  	fnFormAjaxWithJson(opt, true);
						return jsonData;
				},
				isNotBlank:function(val) {
					return ($.isNumeric(val) || val);
				},
				getStringIfBlank:function(val ,defaultVal) {
					if($ES.isNotBlank(val)) {
						return String(val);
					} else {
						return defaultVal;
					}
				},
				replaceNLToBr:function(val) {
					if(val) {
						val = val.replace(/\r\n/g,"<br/>").replace(/\n/g,"<br/>");
					}
					return val;
				},
				fnFormatText:function(val) {
					if(val) {
						if(val.length > 50) {
							val = val.slice(0, 50) + "...";
						}
					}
					return $ES.replaceNLToBr(val);
				},
				bindDefaultEvent:function(obj, eventObj) {
					var jqObj = $ES.getJqObj(obj);
					  $.each(eventObj, function(k, v) {
						  if(k) {
							  var eventName = k;
							  if(eventName.indexOf("on") >= 0) {
								  eventName = eventName.substr(2);
							  }
							  eventName = eventName.toLowerCase();
							  jqObj.off(eventName).on(eventName, v);
						  }
					  });
				},
				loadUrl:function(obj, url) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.each(function() {
						$(this).load(url || $(this).attr("url"));
					});
				},
				getTextBoxVal:function(obj) {
					var jqObj = $ES.getJqObj(obj);
					return jqObj.val();
				},
				setTextBoxVal:function(obj, val) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.val(val);
				},
				setText:function(obj, text) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.text(text);
				},
				getText:function(obj) {
					var jqObj = $ES.getJqObj(obj);
					return jqObj.text();
				},
				initTextBox:function(obj, opt) {
					var jqObj = $ES.getJqObj(obj);
					if(opt["jwDataType"] == "L") {//整数
						jqObj.attr("type", "number");
					} else if(opt["jwDataType"] == "C") {//小数
						jqObj.attr("type", "number");
						jqObj.attr("precision", opt["precision"]);
						var step = 1/parseInt("1".rightPad("0", parseInt(opt["precision"])+1));
						jqObj.attr("step", step);
						jqObj.attr("data-sanitize", "numberFormat");
						var fmt = String(step).replaceAll("1","0");
						jqObj.attr("data-sanitize-number-format", fmt);
					} else {
						if(opt["format"]) {
							jqObj.attr("type", opt["format"]);
						}
					}
					//增加校验信息
					if(opt["validate"]) {
						var validateOpt = opt["validate"];
						if(validateOpt["validation"] && validateOpt["validation"].indexOf("ajax") >= 0) {
							jqObj.attr("data-validation", validateOpt["validation"]);
							jqObj.attr("data-validation-url", validateOpt["url"]);
							jqObj.attr("data-validation-param-name", validateOpt["paramName"]);
							jqObj.attr("data-validation-req-params", validateOpt["reqParams"]);
						}
						if(validateOpt["pattern"]) {
							jqObj.attr("pattern", validateOpt["pattern"]);
						}
						if(validateOpt["length"]) {
							jqObj.attr("data-validation", "length");
							jqObj.attr("data-validation-length", validateOpt["length"]);
						}
					}
					if($ES.isNotBlank(opt.value)) {//设置初始值
						$ES.setTextBoxVal(jqObj, opt.value);
					}
					if(opt["onChange"]) {
						jqObj.on("input", function (event) {
							opt["onChange"].call(this, this.value);
						});
					}
					["onclick","ondblclick","onkeydown","onkeyup","onfocus", "onblur"].forEach(function(item, index, arr) {
						if(opt[item]) {
							var eventStr = item.slice(2);
							jqObj.on(eventStr, function (event) {
								opt[item].call(this, event);
							});
						}
					});
					if(opt["esAutoComplete"]) {//自动完成输入
						$ES.initAutoComplete(jqObj, opt);
					}
				},
				initAutoComplete:function(obj, opt) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.typeahead({
						source:function(query, process) {
							var qp = {};
							var onSetQueryParam = jqObj.data("onSetQueryParam");
							if(onSetQueryParam) {
								qp = onSetQueryParam(query);
							}
							var qd = $.extend(true, {}, opt.queryParams, qp, {q:query});
							fnFormAjaxWithJson({
								url:__ctx+"/gs/gs-mng!queryAjaxResult.action"
								,data:qd
								,success : function(data) {
					            	process(data);
					            }
							}, true);
						}
						,afterSelect:function(item) {
							var onChange = jqObj.data("onChange");
							if(onChange) {
								onChange.call(this, item.text);
							}
						}
						,delay: 300
						,showHintOnFocus:true
					});
				},
				bindAutoCompleteEvent:function(obj, eventObj) {
					var jqObj = $ES.getJqObj(obj);
					if(eventObj["onSetQueryParam"]) {
						jqObj.data("onSetQueryParam", eventObj["onSetQueryParam"]);
					}
					if(eventObj["onChange"]) {
						jqObj.data("onChange", eventObj["onChange"]);
					}
				},
				getResultByVal:function(val, opt, callBack) {
					if(opt["data"]) {
						var valArr = val.split(",");
						var data = opt["data"];
						var result = [];
						for(var i in data) {
							var obj = data[i];
							if(valArr.indexOf(obj.id) >= 0) {
								result.push(obj);
							}
						}
						if(callBack) {
							return callBack(result);
						}
					} else {
						if(val !== "") {
							var qp = $.extend(true,{q:val}, opt["queryParams"]);
							return $.post(__ctx+"/gs/gs-mng!queryAjaxResultByKey.action", qp, function (data) {
								var optArr = [];
								if(data.flag) {
									$MsgUtil.alert($.jwpf.system.alertinfo.loadDataError + data.msg);
								} else {
									if(callBack) {
										callBack(data);
									}
								}
								
							},"json");
						} else {
							if(callBack) {
								return callBack([]);
							}
						}
						
					}
				},
				getDefaultComboBoxOpt:function(opt, jqObj) {
					var initData = [{id: "", text: opt.placeholder || $.jwpf.system.alertinfo.set}];
					if(opt.data) {
						return {
						  width:opt.width || "resolve",
						  allowClear: true,
						  language: opt.locale || "zh-CN",
						  theme: opt.theme || "bootstrap",
						  containerCssClass:opt.containerCssClass || "input-sm",
						  placeholder:opt.placeholder || $.jwpf.system.alertinfo.set,
						  placeholderOption: "first",
						  data:initData.concat(opt.data),
						  multiple:opt["multiple"],
						  onChange:opt["onChange"],
						  onSelect:opt["onSelect"],
						  onUnSelect:opt["onUnSelect"]
						};
					} else {
						return {
						  width:opt.width || "resolve",
						  allowClear: true,
						  language: opt.locale || "zh-CN",
						  theme: opt.theme || "bootstrap",
						  containerCssClass:opt.containerCssClass || "input-sm",
						  placeholder:opt.placeholder || $.jwpf.system.alertinfo.set,
						  placeholderOption: "first",
						  multiple:opt["multiple"],
						  ajax: {
							  url: __ctx+"/gs/gs-mng!queryAjaxResult.action",
							  dataType: 'json',
							  delay: 250,
							  data: function (params) {
								  var qp = {};
								  var onSetQueryParam = jqObj.data("onSetQueryParam");
								  if(onSetQueryParam) {
									  qp = onSetQueryParam(params);
								  }
								  return $.extend(true,{
									q: params.term, 
									page: params.page
								  }, opt.queryParams, qp);
								},
								processResults: function (data, params) {
								  params.page = params.page || 1;
								  /*$.each(data, function(i,n) {
									 n["id"] = n["key"];
									 n["text"] = n["caption"];
								  });*/
								  return {
									results: initData.concat(data),
									pagination: {
									  more: (params.page * 30) < data.total_count
									}
								  };
								},
								cache: true
						  },
						  /*initSelection: function(element, callback) {
								var val = $(element).val();
								var multiple = $(element).prop("multiple");
								if(val) {
									var qp = $.extend(true,{q:val},opt.queryParams);
									return $.post(__ctx+"/gs/gs-mng!queryAjaxResultByKey.action", qp, function (data) {
										callback(multiple?data:data[0]);
									},"json");
								}
						  },*/
						  escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
						  minimumInputLength: opt.minInputLen || 0,
						  onChange:opt["onChange"],
						  onSelect:opt["onSelect"],
						  onUnSelect:opt["onUnSelect"],
						  onSetQueryParam:opt["onSetQueryParam"],
						  templateResult: function (data) {
							  return (data.caption2) ? data.caption2 : data.text;
						  },
						  templateSelection: function (data) {
							  return (data.caption2) ? data.caption2 : data.text;
						  }
						};	
					}
				},
				initComboBox:function(obj,opt) {
					var jqObj = $ES.getJqObj(obj);
					if(opt.data) {
						jqObj.data("isFixData", true);
					}
					if(jqObj.attr("multiple")) {
						opt.multiple = true;
					}
					jqObj.data("esFieldCaption", opt["esFieldCaption"]);
					jqObj.data("queryParams", opt.queryParams);
					jqObj.data("onSetQueryParam", opt.onSetQueryParam);
					var jqOpt = $ES.getDefaultComboBoxOpt(opt, jqObj);
					jqObj.select2(jqOpt);
					if($ES.isNotBlank(opt.value)) {
						$ES.setComboBoxVal(jqObj, opt.value, true);
					}
					$ES.bindComboBoxEvent(jqObj, jqOpt);
				},
				bindComboBoxEvent:function(obj, eventObj) {
					var jqObj = obj;
					if(eventObj["onChange"]) {
						jqObj.on("change", function(e) {
							if(!$(this).data("notTriggerChange")) {//不触发change事件
								eventObj["onChange"].call(this, $ES.getComboBoxValText(jqObj)["id"], $ES.getComboBoxResult(jqObj)["id"]);
								return true;
							} else {
								$(this).removeData("notTriggerChange");
								return false;
							}
						});
					}
					if(eventObj["onSelect"]) {
						jqObj.on("select2:select", function(e) {
							eventObj["onSelect"].call(this, e.params.data);
							return true;
						});
					}
					if(eventObj["onUnSelect"]) {
						jqObj.on("select2:unselect", function(e) {
							eventObj["onUnSelect"].call(this, e.params.data);
							return true;
						});
					}
					if(eventObj["onSetQueryParam"]) {
						jqObj.data("onSetQueryParam", eventObj["onSetQueryParam"]);
					}
				},
				setComboBoxVal:function(obj, val, isInit) {
					var jqObj = $ES.getJqObj(obj);
					if($.isPlainObject(val)) {
						$ES.setComboBoxValText(jqObj, val.id, val.text, isInit);
					} else {
						val = $ES.getStringIfBlank(val, "");
						if(jqObj.data("isFixData")) {//固定值
							if(jqObj.prop("multiple")) {
								jqObj.val(val.split(","));
							} else {
								jqObj.val(val);
							}
							jqObj.trigger("change");
							//if(!isInit) jqObj.trigger("change");
						} else {
							$ES.initComboBoxSelection(jqObj, val, isInit);
						}
					}
				},
				setComboBoxSelected:function(jqObj, val, txt) {
					var opt = jqObj.find("option[value='"+ val +"']");
					if(opt.length) {
						opt.prop("selected",true);
					} else {
						jqObj.append('<option value="'+ val +'" selected="true">'+ txt +'</option>');
					}
				},
				initComboBoxSelection:function(jqObj, val, isInit) {
					if($ES.isNotBlank(val)) {
						var onSetQueryParam = jqObj.data("onSetQueryParam");
						var qps = {};
						if(onSetQueryParam) {
							qps = onSetQueryParam({});
						}
						var qp = $.extend(true,{q:val},jqObj.data("queryParams"), qps);
						return $.post(__ctx+"/gs/gs-mng!queryAjaxResultByKey.action", qp, function (data) {
							var optArr = [];
							if(data.flag) {
								$MsgUtil.alert(jqObj.data("esFieldCaption") + $.jwpf.system.alertinfo.loadDataError + data.msg);
							} else {
								jqObj.empty();//先清空数据
								jqObj.append('<option value=""></option>');//默认空选项
								for(var i in data) {
									var row = data[i];
									var $opt = $('<option value="'+ row.id +'" selected="true">'+ (row.caption2 || row.text) +'</option>');
									$opt.data("data", row);
									jqObj.append($opt);
									//optArr.push();
								}
								//jqObj.html(optArr.join(""));
								if(!isInit) jqObj.trigger("change");
							}
							
						},"json");
					} else {
						jqObj.val(null);
						if(!isInit) jqObj.trigger("change");
					}
				},
				setComboBoxValText:function(obj, val, text, isInit) {
					var jqObj = $ES.getJqObj(obj);
					val = $ES.getStringIfBlank(val, "");
					var txt = text;
					if(jqObj.data("isFixData")) {//固定值
							txt = txt || val;
							if(jqObj.prop("multiple")) {
								if(val) {
									var valArr = val.split(",");
									var txtArr = txt.split(",");
									for(var i=0,len=valArr.length;i<len;i++) {
										$ES.setComboBoxSelected(jqObj, valArr[i], txtArr[i]);
									}
								} else {//清空数据
									jqObj.val(null);
								}
							} else {
								$ES.setComboBoxSelected(jqObj, val, txt);
							}
							if(!isInit) jqObj.trigger("change");
					} else {
						if(txt) {
							var optArr = [];
							optArr.push('<option value=""></option>');//默认空选项
							if(jqObj.prop("multiple")) {
								var valArr = val.split(",");
								var txtArr = txt.split(",");
								for(var i=0,len=valArr.length;i<len;i++) {
									optArr.push('<option value="'+ valArr[i] +'" selected="true">'+ txtArr[i] +'</option>');
								}
							} else {
								optArr.push('<option value="'+ val +'" selected="true">'+ txt +'</option>');
							}	
							jqObj.html(optArr.join(""));
							if(!isInit) jqObj.trigger("change");
						} else {//动态值，取查询数据
							$ES.initComboBoxSelection(jqObj, val, isInit);
						}
					}
				},
				getComboBoxVal:function(obj, textKey) {
					var jqObj = $ES.getJqObj(obj);
					var data = jqObj.select2("data");
					var valArr = [];
					var key = textKey || "id";
					for(var i in data) {
						valArr.push(data[i][key]);
					}
					return valArr.join(",");
				},
				getComboBoxValText:function(obj) {
					var jqObj = $ES.getJqObj(obj);
					var data = jqObj.select2("data");
					var valArr = [], textArr = [];
					for(var i in data) {
						valArr.push(data[i]["id"]);
						textArr.push(data[i]["caption2"]||data[i]["text"]);
					}
					return {"id":valArr.join(","),"text":textArr.join(",")};
				},
				getComboBoxResult:function(obj) {
					var jqObj = $ES.getJqObj(obj);
					var data = jqObj.select2("data");
					return data;
				},
				getDefaultDataGridOpt:function() {
					return {
						editable:false,
						classes:"table table-hover table-no-bordered es-table",
				        striped : true,
				        sidePagination:'server',
						method:'post',
						idField:'id',
						contentType:"application/x-www-form-urlencoded",
						search:true,
						searchOnEnterKey:true,
						advancedSearch:true,
						showToggle:true,
						showRefresh:true,
						showColumns:true,
						showExport:false,
						reorderableColumns:true,
						resizable:true,
						mobileResponsive:true,
				        pagination : true,
				        clickToSelect:true,
				        paginationVAlign:"both",
				        paginationHAlign:"left",
				        paginationDetailHAlign:"right",
				        stickyHeader: true,
			            stickyHeaderOffsetY: $ES.getStickyHeaderOffsetY,
			            fixedColumns: false,
			            fixedNumber: 0,
			            iconSize:"sm",
			            storage:true
					}
				},
				initDataGrid:function(obj, opt) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.bootstrapTable(opt);
					if($ES.isNotBlank(opt.value)) {
						$ES.setDataGridVal(jqObj, opt.value);
					}
				},
				methodDataGrid:function(obj, method, param) {
					var jqObj = $ES.getJqObj(obj);
					return jqObj.bootstrapTable(method, param);
				},
				refreshDataGrid:function(obj) {
					$ES.methodDataGrid(obj, "refresh");
				},
				enableDataGrid:function(obj) {
					var options = $ES.methodDataGrid(obj,"getOptions");
					if(options.editable == false) {
						options.editable = true;
						$ES.methodDataGrid(obj, "refresh");
					}
				},
				disableDataGrid:function(obj) {
					var options = $ES.methodDataGrid(obj,"getOptions");
					if(options.editable == true) {
						options.editable = false;
						$ES.methodDataGrid(obj, "refresh");
					}
				},
				setDataGridVal:function(obj, val) {
					var ele = obj;
					var option = $ES.methodDataGrid(ele, "getOptions");
					var newUrl = option["esUrl"];
					if(newUrl /*&& option["sidePagination"]!="client"*/) {
						newUrl = newUrl + "&id="+val;
						option["esBindId"] = val;
						option["url"] = newUrl;
						option["sidePagination"] = "server";
						$ES.methodDataGrid(ele, "refresh");
					}
				},
				getDataGridData:function(obj, isSel) {
					var method = (!isSel)?"getData":"getSelections";
					var resultData = $ES.methodDataGrid(obj, method);
					return resultData;
				},
				addDataGridRow:function(obj, row) {
					var opt = $ES.methodDataGrid(obj,'getOptions');
					var newRow = $.extend(true, {}, opt.defaultRow, row);
					$ES.methodDataGrid(obj,'append', newRow);
				},
				getDataGridRowIndex:function(obj, row) {
					var data = $ES.methodDataGrid(obj,'getData');
					return data.indexOf(row);
				},
				insertDataGridRow:function(obj, row) {
					var selRows = $ES.methodDataGrid(obj,'getSelections');
					if(selRows && selRows.length) {
						var curRow = selRows[0];
						var index = $ES.getDataGridRowIndex(obj, curRow);
						var opt = $ES.methodDataGrid(obj,'getOptions');
						var newRow = $.extend(true, {}, opt.defaultRow, row);
						$ES.methodDataGrid(obj, "insertRow", {index:index, row:newRow});
						$ES.methodDataGrid(obj, "uncheckAll");
						$ES.methodDataGrid(obj, "check", index);
					} else {
						$MsgUtil.alert($.jwpf.system.confirm.selectInsertRow);
					}
				},
				deleteDataGridRow:function(obj) {
					var selRows = $ES.methodDataGrid(obj,'getSelections');
					if(selRows && selRows.length) {
						var data = $ES.methodDataGrid(obj,'getData');
						for(var len=selRows.length,i=len-1;i>=0;i--) {
							var row = selRows[i];
							data.splice(data.indexOf(row), 1);
						}
						$ES.methodDataGrid(obj,'load', data);
					} else {
						$MsgUtil.alert($.jwpf.system.confirm.selectRow);
					}
				},
				preSaveDataGrid:function(obj, opt) {
					
				},
				saveDataGrid:function(obj, opt) {
					$ES.preSaveDataGrid(obj, opt);
					var resultData = $ES.getDataGridData(obj);
					var rows = {"updated":[]};
					var oper = opt.oper;
					for(var k = 0,len = resultData.length; k < len; k++) {
						if(oper == "edit" || oper == "add") {
							resultData[k].id = null;
						}
						rows.updated.push(resultData[k]);
					}
					return rows;
				},
				initTree:function(obj, opt) {
					var jqObj = $ES.getJqObj(obj);
					opt = $ES.getDefaultSelectTreeOpt(opt, jqObj);
					jqObj.data("queryParams", opt.queryParams);
					jqObj.esTree(opt);
				},
				getTreeVal:function(obj, textKey) {
					var jqObj = $ES.getJqObj(obj);
					var key = textKey || "id";
					var result = jqObj.esTree("getValue");
					return result[key];
				},
				setTreeVal:function(obj, val) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.esTree("setValue", val);
				},
				getDefaultSelectTreeOpt:function(defaultOpt, jqObj) {
					var opt = $.extend(true, {}, defaultOpt);
					var url = __ctx + "/gs/gs-mng!queryAjaxResult.action";
					var filterUrl = __ctx + "/gs/gs-mng!queryAjaxResultByKey.action";
					if(opt["esType"] == "DictTree") {
						url = __ctx + "/sys/common/common-type!queryList.action";
						filterUrl = __ctx + "/sys/common/common-type!queryListByKey.action";
					}
					opt["url"] = opt["url"] || url;
					opt["filterUrl"] = opt["filterUrl"] || filterUrl; 
					opt["queryParams"]["rows"] = opt["queryParams"]["rows"] || 65535;
					if(jqObj.attr("multiple")) {
						opt.multiple = true;
					}
					return opt;
				},
				initSelectTree:function(obj, opt) {
					var jqObj = $ES.getJqObj(obj);
					opt = $ES.getDefaultSelectTreeOpt(opt, jqObj);
					jqObj.data("queryParams", opt.queryParams);
					jqObj.selectTree(opt);
					if($ES.isNotBlank(opt.value)) {//设置初始值
						$ES.setSelectTreeVal(jqObj, opt.value);
					}
				},
				getSelectTreeVal:function(obj, textKey) {
					var jqObj = $ES.getJqObj(obj);
					var key = textKey || "id";
					var result = jqObj.selectTree("getValue");
					return result[key];
				},
				setSelectTreeVal:function(obj, val) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.selectTree("setValue", val);
				},
				setSelectTreeValText:function(obj, val, text) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.selectTree("setValue", {"id":val,"text":text});
				},
				initRichTextBox:function(obj, opt) {
					var jqObj = $ES.getJqObj(obj);
					if(opt["format"] == "html") {
						jqObj.data("format", "html");
						jqObj.summernote(opt);
						if($ES.isNotBlank(opt.value)) {//设置初始值
							$ES.setRichTextBoxVal(jqObj, opt.value);
						}
						if(opt["onChange"]) {
							jqObj.off("summernote.change").on("summernote.change", function(we, contents, $editable) {
								opt["onChange"].call(this, contents);
							});
						}
					} else {
						jqObj.data("format", "text");
						if($ES.isNotBlank(opt.value)) {//设置初始值
							$ES.setRichTextBoxVal(jqObj, opt.value);
						}
						if(opt["onChange"]) {
							jqObj.off("input").on("input", function (event) {
								opt["onChange"].call(this, this.value);
							});
						}
					}
				},
				setRichTextBoxVal:function(obj, val) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.val(val);
					if(jqObj.data("format")=="html") {
						jqObj.summernote("code",val);
					}
				},
				getRichTextBoxVal:function(obj) {
					var jqObj = $ES.getJqObj(obj);
					if(jqObj.data("format")=="html") {
						return jqObj.summernote("code");
					} else {
						return jqObj.val();
					}
				},
				enableRichTextBox:function(obj) {
					var jqObj = $ES.getJqObj(obj);
					if(jqObj.data("format")=="html") {
						jqObj.removeProp("readonly");
					} else {
						jqObj.removeProp("readonly");
					}
				},
				disableRichTextBox:function(obj) {
					var jqObj = $ES.getJqObj(obj);
					if(jqObj.data("format")=="html") {
						jqObj.prop("readonly", true);
					} else {
						jqObj.prop("readonly", true);
					}
				},
				initCheckBox:function(obj, opt) {
					var jqObj = $ES.getJqObj(obj);
					var jsonData = opt.data || $ES.getJsonObjByParam(opt.queryParams);
					var htmlStr = [];
					var checkType = "radio";
					opt.multiple = opt.multiple || (jqObj.attr("multiple")=="true" || jqObj.attr("multiple")=="multiple");
					if(opt.multiple) {
						checkType = "checkbox";
					}
					opt["esFieldKey"] = opt["esFieldKey"] || jqObj.attr("id");
					var className = checkType + "-inline";
					var checkId = opt["esFieldKey"] + (opt["esIndex"]||"");
					var checkName = opt["esFieldKey"] || checkId;
					for(var i in jsonData) {
						var json = jsonData[i];
						htmlStr.push("<label class='");
						htmlStr.push(className);
						htmlStr.push("'>");
						htmlStr.push("<input type='");
						htmlStr.push(checkType);
						htmlStr.push("' name='");
						htmlStr.push(checkName);
						htmlStr.push("' id='");
						htmlStr.push(checkId);
						htmlStr.push(i);
						htmlStr.push("' value='");
						htmlStr.push(json["id"]);
						htmlStr.push("' title='");
						htmlStr.push(json["text"]);
						htmlStr.push("' />");
						htmlStr.push(" ");
						htmlStr.push(json["text"]);
						htmlStr.push("</label>");
					}
					jqObj.html(htmlStr.join(""));
					if($ES.isNotBlank(opt.value)) {//设置初始值
						$ES.setCheckBoxVal(jqObj, opt.value);
					}
					$ES.bindCheckBoxEvent(jqObj, opt);
				},
				bindCheckBoxEvent:function(obj, eventObj) {
					var jqObj = $ES.getJqObj(obj);
					if(eventObj["onChange"]) {
						jqObj.find("input").off("change").on("change", function(event) {
							var val = $ES.getCheckBoxVal(obj);
							//var text = $ES.getCheckBoxVal(obj, "text");
							eventObj["onChange"].call(this, val);
						});
					}
				},
				setCheckBoxVal:function(obj, val) {
					var jqObj = $ES.getJqObj(obj);
					val = $ES.getStringIfBlank(val, "");
					var valArr = val.split(",");
					for(var i in valArr) {
						jqObj.find("input[value='" + valArr[i] + "']").prop("checked", true);
					}
				},
				getCheckBoxVal:function(obj, textKey) {
					var jqObj = $ES.getJqObj(obj);
					var data = [];
					var valArr = [];
					var key = textKey || "id";
					jqObj.find("input:checked").each(function() {
						if(key == "id") {
							valArr.push($(this).val());
						} else {
							valArr.push($(this).attr("title"));
						}
					});
					return valArr.join(",");
				},
				enableCheckBox:function(obj) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.find("input").removeProp("disabled");
				},
				disableCheckBox:function(obj) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.find("input").prop("disabled", true);
				},
				initCheckBoxGroup:function(obj, opt) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.checkboxgroup(opt);
				},
				reloadCheckBoxGroup:function(obj, param) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.checkboxgroup("reload", param);
				},
				getCheckBoxGroupVal:function(obj) {
					var jqObj = $ES.getJqObj(obj);
					return jqObj.checkboxgroup("getValue");
				},
				destroyCheckBoxGroup:function(obj, opt) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.checkboxgroup("destroy");
				},
				initComboBoxGroup:function(obj, opt) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.comboboxgroup(opt);
				},
				reloadComboBoxGroup:function(obj, param) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.comboboxgroup("reload", param);
				},
				getComboBoxGroupVal:function(obj) {
					var jqObj = $ES.getJqObj(obj);
					return jqObj.comboboxgroup("getValue");
				},
				destroyComboBoxGroup:function(obj, opt) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.comboboxgroup("destroy");
				},
				initDateBox:function(obj, opt) {
					var jqObj = $ES.getJqObj(obj);
					var newOpt = {};
					if(opt && opt["format"]) {
						//?replace("m","i"))?replace("M","m"))?replace("H","h")
						var fmt = opt["format"];
						fmt = fmt.replaceAll("m","i").replaceAll("M","m").replaceAll("H","h");
						if(fmt.indexOf("h") == -1 && fmt.indexOf("i") == -1
							&& fmt.indexOf("s") == -1) {
							newOpt["minView"] = 2;
						}
						newOpt["format"] = fmt;
					}
					opt = $.extend(true, {}, opt, {
						//"pickerPosition":"top-right", 
						"todayBtn":true,
						"todayHighlight":true,
						"minuteStep":1,
						"autoclose":true
					}, newOpt);
					jqObj.datetimepicker(opt);
					if($ES.isNotBlank(opt.value)) {//设置初始值
						$ES.setDateBoxVal(jqObj, opt.value);
					}
					if(opt["onChange"]) {
						jqObj.off("changeDate").on("changeDate", function (ev) {
							opt["onChange"].call(this, ev.target.value);
						});
					}
				},
				getDateBoxVal:function(obj) {
					var jqObj = $ES.getJqObj(obj);
					return jqObj.val();
				},
				setDateBoxVal:function(obj, val) {
					var jqObj = $ES.getJqObj(obj);
					return jqObj.val(val);
				},
				initFileUploadBox:function(obj, opt) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.fileUploader(opt);
				},
				initFileBox:function(obj, opt) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.attr("name","esFile");
					opt.baseUrl = __ctx;
					jqObj.filebox(opt);
					if($ES.isNotBlank(opt.value)) {//设置初始值
						$ES.setFileBoxVal(jqObj, opt.value);
					}
				},
				setFileBoxVal:function(obj, val) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.filebox("setValue", val);
				},
				getFileBoxVal:function(obj) {
					var jqObj = $ES.getJqObj(obj);
					return jqObj.filebox("getValue");
				},
				initSplitter:function(obj, opt) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.touchSplit(opt);
				},
				initDialogueWindow:function(obj, opt) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.dialoguewindow(opt);
					if($ES.isNotBlank(opt.value)) {//设置初始值
						$ES.setDialogueWindowVal(jqObj, opt.value);
					}
				},
				setDialogueWindowVal:function(obj, val) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.dialoguewindow("setValue", val);
				},
				getDialogueWindowVal:function(obj) {
					var jqObj = $ES.getJqObj(obj);
					return jqObj.dialoguewindow("getValue");
				},
				initButton:function(obj, opt) {
					var jqObj = $ES.getJqObj(obj);
					if(opt["onclick"]) {
						jqObj.off("click").on("click", function (event) {
							opt["onclick"].call(this, $(this).attr("esValue"));
						});
					}
				},
				setButtonVal:function(obj, val, text) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.attr("esValue", val);
					if(text) {
						jqObj.html(text);
					}
				},
				getButtonVal:function() {
					var jqObj = $ES.getJqObj(obj);
					return jqObj.attr("esValue");
				},
				initFullCalendar:function(obj, opt) {
					var jqObj = $ES.getJqObj(obj);
					var defaultOpt = {
							header: {
								left: 'prev,next today',
								center: 'title',
								right: 'month,agendaWeek,agendaDay,listMonth'
							},
							defaultDate: $today$,
							//height:"auto",
							weekNumbers: true,
							navLinks: true, 
							editable: true,
							eventLimit: true,
							eventClick: function(event) {
								var el = $(this);
						        el.popover({
						        	title:event.title,
						            content: event.content,
						            html: true,
						            placement: "auto",
						            container: 'body',
						            trigger: 'hover'
						        });
						        el.popover('show');
								return false;
							}
					};
					var fcOpt = $.extend(true, {}, defaultOpt, opt);
					jqObj.fullCalendar(fcOpt);
				},
				refreshFullCalendar:function(obj) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.fullCalendar("refetchEvents");
				},
				destroyFullCalendar:function(obj) {
					var jqObj = $ES.getJqObj(obj);
					jqObj.fullCalendar("destroy");
				},
				loadFormData:function(obj, opt) {
					var jqObj = $ES.getJqObj(obj);
					var data = opt["data"]||{};
					$.each(data, function(key, value) {
						jqObj.find("[name='" + key + "']").each(function() {
							var tagName = $(this)[0].tagName;
							var type = $(this).prop('type');
							if(tagName=='INPUT' || tagName=='TEXTAREA'){
								if(type=='radio'){
									$(this).prop('checked',$(this).val()==value);
								} else if(type=='checkbox'){
									value = $ES.getStringIfBlank(value, "");
									var arr = value.split(',');
									for(var i =0;i<arr.length;i++){
										if($(this).val()==arr[i]){
											$(this).prop('checked',true);
											break;
										}
									}
								} else{
									if($(this).hasClass("richtextbox")) {
										$ES.setRichTextBoxVal($(this),value);
									} else if($(this).hasClass("es-combotree")) {
										var textKey = key + "text";
										$ES.setSelectTreeValText($(this),value,data[textKey]);
									} else {
										$(this).val(value);
									}
								}
							} else if(tagName=='SELECT'){
								var textKey = key + "text";
								$ES.setComboBoxValText($(this),value,data[textKey]);
							}
						});
					});
				},
				getFormData:function(formObj) {
					var jqObj = $ES.getJqObj(formObj);
					var fields = jqObj.serializeArray();
			      	var jsonObj = {};
			      	$.each(fields, function(k, v) {
			      		 if (jsonObj[v.name]) {
			      			 if(v.value) {
			      				jsonObj[v.name] = jsonObj[v.name] + "," + v.value;
			      			 }
			      		 } else { 
			      			 jsonObj[v.name] = v.value || ''; 
			      		 } 
			      	}); 
			      	return jsonObj;
				},
				resetForm:function(formObj) {
					var jqObj = $ES.getJqObj(formObj);
					jqObj[0].reset();
				},
				validateForm:function(formObj) {
					var jqObj = $ES.getJqObj(formObj);
					return jqObj.isValid({},{},true);
					//return true;
				},
				initFormValidate:function(opt) {
					var defaultOpt = {
						modules : "html5",
						form: "#viewForm"
					};
					var option = $.extend(true, {}, defaultOpt, opt);
					$.validate(option);
				},
				doDisableForm:function(formObj) {
					var jqObj = $ES.getJqObj(formObj);
					jqObj.find("button,a.bt-extra,select,input.es-date,input:radio,input:checkbox").prop("disabled", true);
					jqObj.find("input,textarea").prop("readonly",true);
					jqObj.find("button.validInView").removeProp("disabled");
					jqObj.find("a.hyperlink").not(".validInView").removeProp("disabled");
					jqObj.find(".richtextbox").summernote("disable");
					var dgObj = jqObj.find("div.bootstrap-table");
					dgObj.find("button,input").removeProp("disabled");
					dgObj.find("input").removeProp("readonly");
			    },
				doEnableForm:function(formObj) {
					var jqObj = $ES.getJqObj(formObj);
					jqObj.find("button,a.bt-extra,select,input.es-date,input:radio,input:checkbox").removeProp("disabled");
					jqObj.find("input,textarea").removeProp("readonly");
					jqObj.find(".richtextbox").summernote("enable");
			    },
				doPrepareSaveForm:function(formObj) {
					var jqObj = $ES.getJqObj(formObj);
					jqObj.find("input,textarea,select").removeProp("disabled");
					jqObj.find("select").each(function() {
						var name = $(this).prop("name");
						name = "#" + name + "text";
						var text = $ES.getComboBoxVal($(this), "text");
						$ES.setTextBoxVal(name, text);
					});
					jqObj.find("div.es-radiobox,div.es-checkbox").each(function() {
						var name = $(this).attr("id");
						name = "#" + name + "text";
						var text = $ES.getCheckBoxVal($(this), "text");
						$ES.setTextBoxVal(name, text);
					});
					jqObj.find("textarea.richtextbox").each(function() {
						var val = $ES.getRichTextBoxVal($(this));
						$ES.setRichTextBoxVal($(this), val);
					});
					jqObj.find("input.es-combotree").each(function() {
						var name = $(this).prop("name");
						name = "#" + name + "text";
						var text = $ES.getSelectTreeVal($(this), "text");
						$ES.setTextBoxVal(name, text);
					});
				},//打开tab页或窗口
				doOpenUrl:function(title, url) {
					if(top.addTab) {
						top.addTab(title, url);
					} else {
						window.open(url, title);
					}
				},//获取状态列表
				getStatusList:function() {
					return $.jwpf.system.module.statusList;
				},
				getStatusDisplayStyleMap:function() {
					var dsMap = {
							"00":"label-info","10":"label-info","20":"label-primary","31":"label-success",
							"35":"label-success","32":"label-warning","40":"label-default"
					};
					return dsMap;
				},
				//获取状态显示
				getStatusDisplayStyle:function(status, rowData) {
					var dsMap = $ES.getStatusDisplayStyleMap();
					return sprintf('<span class="label %s">%s</span>', dsMap[status||""]||"label-info", $ES.getStatusDisplayValue(status, rowData["statusCaption"]));
				},//获取明细状态显示
				getSubStatusDisplayStyle:function(status, rowData) {
					var dsMap = $ES.getStatusDisplayStyleMap();
					return sprintf('<span class="label %s">%s</span>', dsMap[status||""]||"label-info", $.jwpf.system.module.statusSub[status] || rowData["statusCaption"]);
				},
				getStatusDisplayValue:function(status, defaultVal) {
					return $.jwpf.system.module.status[status] || defaultVal;
				},
				getPinYinList:function() {
					return [
						{"id":'a',"text":'A'},
						{"id":'b',"text":'B'},
						{"id":'c',"text":'C'},
						{"id":'d',"text":'D'},
						{"id":'e',"text":'E'},
						{"id":'f',"text":'F'},
						{"id":'g',"text":'G'},
						{"id":'h',"text":'H'},
						{"id":'i',"text":'I'},
						{"id":'j',"text":'J'},
						{"id":'k',"text":'K'},
						{"id":'l',"text":'L'},
						{"id":'m',"text":'M'},
						{"id":'n',"text":'N'},
						{"id":'o',"text":'O'},
						{"id":'p',"text":'P'},
						{"id":'q',"text":'Q'},
						{"id":'r',"text":'R'},
						{"id":'s',"text":'S'},
						{"id":'t',"text":'T'},
						{"id":'u',"text":'U'},
						{"id":'v',"text":'V'},
						{"id":'w',"text":'W'},
						{"id":'x',"text":'X'},
						{"id":'y',"text":'Y'},
						{"id":'z',"text":'Z'}
					];
				},//设置字段只读
				setFieldReadonly:function(obj, isReadonly){
					var object = $ES.getJqObj(obj);
					if(object.hasClass("es-combobox")) {
						if(isReadonly) {
							object.prop("disabled",true);
						} else {
							object.removeProp("disabled");
						}
					} else if(object.hasClass("es-combotree")) {
						if(isReadonly) {
							object.selectTree("disable");
						} else {
							object.selectTree("enable");
						}
					} else if(object.hasClass("es-checkbox")) {
						if(isReadonly) {
							$ES.disableCheckBox(object);
						} else {
							$ES.enableCheckBox(object);
						}
					} else if(object.hasClass("es-datebox")) {
						if(isReadonly) {
							object.prop("readonly", true);
							var onfocusStr = object.attr("onfocus");
							if(onfocusStr) {
								object.data("focusEvent",onfocusStr);
								object.removeAttr("onfocus");
							}
						} else {
							object.removeProp("readonly");
							var onfocusStr = object.data("focusEvent");
							if(onfocusStr) {
								object.attr("onfocus", onfocusStr);
								object.removeData("focusEvent");
							}
						}
					} else if(object.hasClass("es-dialoguewindow")) {
						if(isReadonly) {
							object.dialoguewindow("disable");
						} else {
							object.dialoguewindow("enable");
						}
					} else if(object.hasClass("es-richtextbox")) {
						if(isReadonly) {
							$ES.disableRichTextBox(object);
						} else {
							$ES.enableRichTextBox(object);
						}
					} else {
						if(isReadonly) {
							object.prop("readonly", true);
						} else {
							object.removeProp("readonly");
						}
					}		
				},
				initTabsCollapse:function() {
					$(".collapse-link").click(function() {
						var o = $(this).closest("div.ibox"),
							e = $(this).find("i"),
							i = o.find("div.ibox-content");
						i.slideToggle(200), e.toggleClass("fa-chevron-up").toggleClass("fa-chevron-down"), o.toggleClass("").toggleClass("border-bottom"), setTimeout(function() {
							o.resize(), o.find("[id^=map-]").resize();
						}, 50);
					});
					$(".tabs-collapse-link").click(function() {
						var o = $(this).closest("div.tabs-container"),
							e = $(this).find("i"),
							i = o.find("div.tab-content");
						i.slideToggle(200), e.toggleClass("fa-chevron-up").toggleClass("fa-chevron-down"), o.toggleClass("").toggleClass("border-bottom"), 
						setTimeout(function() {
							o.resize(), o.find("[id^=map-]").resize();
						}, 50);
					});
				},//根据attachId获取文件url
				getOpenerWin:function() {
					if(window.opener) {
						return window.opener.top;
					} else {
						return window.top;
					}
				},
				setPageValue:function(key, value) {
					this.getOpenerWin().PageStorage.write(key, value);
				},
				getPageValue:function(key) {
					return this.getOpenerWin().PageStorage.read(key);
				},
				removePageValue:function(key) {
					this.getOpenerWin().PageStorage.remove(key);
				},
				getQueryOperList:function() {
					return [
							{"id":'EQ',"text":'等于(=)'},
							{"id":'NE',"text":'不等于(!=)'},
							{"id":'LIKE',"text":'包含(CONTAINS)'},
							{"id":'LIKEL',"text":'开始于(START WITH)'},
							{"id":'LIKER',"text":'结束于(END WITH)'}
						];
				},
				initAdvanceQuery:function(opt) {
					if(!opt.isInit) {
						$ES.initComboBox("#queryFieldSel", {data:opt.data, multiple:true, width:'100%'
							,onSelect:function(data) {
								var strHtml = "";
								$(sprintf('#_qf_%(id)s_', data)).remove();
								switch(data.editType) {
									case "TextBox":	
									case "HyperLink":
										if(["dtInteger","dtLong","dtDouble"].indexOf(data.dataType) >= 0) {
											if(data.precision) {
												data.step = Math.pow(10, -1*parseInt(data.precision));
											} else {
												data.step = "1";
											}
											strHtml = sprintf(['<div id="_qf_%(id)s_" class="row form-group form-group-sm">',
											 '<div class="col-sm-2 form-horizontal text-right">',
											 '<label for="query_%(id)s" class="control-label">%(text)s</label>',
											 '</div>',
											 '<div class="col-sm-10 form-inline">',
											 '<div class="form-group">',
											 '<input type="number" class="form-control" placeholder="最小值" name="filter_GE%(dt)s_%(fieldKey)s" id="query_%(id)sStart" step="%(step)s">',
											 '</div>',
											 '<div class="form-group">',
											 ' -- <input type="number" class="form-control" placeholder="最大值" name="filter_LE%(dt)s_%(fieldKey)s" id="query_%(id)sEnd" step="%(step)s">',
											 '</div>',
											 '</div>',
											 '</div>'].join(""), data);
											$("#queryForm").append(strHtml);
											if($ES.isNotBlank(data.start)) {
												var queryKey = sprintf('#query_%(id)sStart',data);
												$(queryKey).val(data.start);
											}
											if($ES.isNotBlank(data.end)) {
												var queryKey = sprintf('#query_%(id)sEnd',data);
												$(queryKey).val(data.end);
											}
										} else {
											var oper = data.oper;
											if(oper!="USER") data.oper = "LIKE";
											strHtml = sprintf(['<div id="_qf_%(id)s_" class="row form-group form-group-sm">',
																 '<div class="col-sm-2 form-horizontal text-right">',
																 '<label for="query_%(id)s" class="control-label">%(text)s</label>',
																 '</div>',
																 '<div class="col-sm-10 form-inline">',
																 '<div class="form-group">',
																 '<select class="form-control" id="query_%(id)sCondSel" forId="query_%(id)s"></select>',
																 '</div>',
																 '<div class="form-group">',
																 '<input type="text" class="form-control" placeholder="%(text)s" name="filter_%(oper)sS_%(fieldKey)s" forName="%(fieldKey)s" id="query_%(id)s" >',
																 '</div>',
																 '</div>',
																 '</div>'].join(""), data);
											$("#queryForm").append(strHtml);
											var fieldKey = sprintf("#query_%(id)sCondSel",data);
											$ES.initComboBox(fieldKey, {"data":$ES.getQueryOperList(),"width":"160px", "onSelect":function(result) {
												var oper = result.id;
												var forId = $(this).attr("forId");
												var fieldName = $("#"+forId).attr("forName");
												//console.log(oper + "," + forId + "," + fieldName);
												$("#"+forId).attr("name", "filter_" + oper + "S_" + fieldName);
											}, "value":data.oper||"EQ"});
											if(oper=="USER") {
												//$ES.setComboBoxVal(fieldKey, "LIKE");
												$(fieldKey).prop("disabled", true);
											}
											if($ES.isNotBlank(data.val)) {
												var queryKey = sprintf('#query_%(id)s',data);
												$(queryKey).val(data.val);
											}
										}
										break;
									case "RichTextBox":
										data.oper = "LIKE";
										strHtml = sprintf(['<div id="_qf_%(id)s_" class="row form-group form-group-sm">',
															 '<div class="col-sm-2 form-horizontal text-right">',
															 '<label for="query_%(id)s" class="control-label">%(text)s</label>',
															 '</div>',
															 '<div class="col-sm-10 form-inline">',
															 '<div class="form-group">',
															 '<select class="form-control" id="query_%(id)sCondSel" forId="query_%(id)s"></select>',
															 '</div>',
															 '<div class="form-group">',
															 '<textarea class="form-control" placeholder="%(text)s" name="filter_%(oper)sS_%(fieldKey)s" forName="%(fieldKey)s" id="query_%(id)s" ></textarea>',
															 '</div>',
															 '</div>',
															 '</div>'].join(""), data);
										$("#queryForm").append(strHtml);
										var fieldKey = sprintf("#query_%(id)sCondSel",data);
										$ES.initComboBox(fieldKey, {"data":$ES.getQueryOperList(),"width":"160px", "onSelect":function(result) {
											var oper = result.id;
											var forId = $(this).attr("forId");
											var fieldName = $("#"+forId).attr("forName");
											$("#"+forId).attr("name", "filter_" + oper + "S_" + fieldName);
										}, "value":data.oper||"EQ"});
										if($ES.isNotBlank(data.val)) {
											var queryKey = sprintf('#query_%(id)s',data);
											$(queryKey).val(data.val);
										}
										break;
									case "DialogueWindow":
										strHtml = sprintf(['<div id="_qf_%(id)s_" class="row form-group form-group-sm">',
															 '<div class="col-sm-2 form-horizontal text-right">',
															 '<label for="query_%(id)stext" class="control-label">%(text)s</label>',
															 '</div>',
															 '<div class="col-sm-10 form-inline">',
															 '<div class="form-group">',
															 '<select class="form-control" id="query_%(id)sCondSel" forId="query_%(id)stext"></select>',
															 '</div>',
															 '<div class="form-group">',
															 '<input type="text" class="form-control" placeholder="%(text)s" name="filter_EQS_%(fieldKey)stext" forName="%(fieldKey)stext" id="query_%(id)stext" >',
															 '</div>',
															 '</div>',
															 '</div>'].join(""), data);
										$("#queryForm").append(strHtml);
										var fieldKey = sprintf("#query_%(id)sCondSel",data);
										$ES.initComboBox(fieldKey, {"data":$ES.getQueryOperList(),"width":"160px", "onSelect":function(result) {
											var oper = result.id;
											var forId = $(this).attr("forId");
											var fieldName = $("#"+forId).attr("forName");
											$("#"+forId).attr("name", "filter_" + oper + "S_" + fieldName);
										}, "value":data.oper||"EQ"});
										if($ES.isNotBlank(data.val)) {
											var queryKey = sprintf('#query_%(id)stext',data)
											$(queryKey).val(data.val);
										}
										break;
									case "DateBox":
										data.fmt = data.fmt || "yyyy-MM-dd";//默认日期格式
										strHtml = sprintf(['<div id="_qf_%(id)s_" class="row form-group form-group-sm">',
										 '<div class="col-sm-2 form-horizontal text-right">',
										 '<label for="query_%(id)s" class="control-label">%(text)s</label>',
										 '</div>',
										 '<div class="col-sm-10 form-inline">',
										 '<div class="form-group">',
										 '<input type="text" class="Wdate form-control input-sm input-sm-date" placeholder="起始值" name="filter_GE%(dt)s_%(fieldKey)s" id="query_%(id)sStart" ',
										 ' onclick="WdatePicker({dateFmt:\'%(fmt)s\',onpicked:function(dp) {if(!$dp.$(\'query_%(id)sEnd\').value)$dp.$(\'query_%(id)sEnd\').value=this.value;}})">',
										 '</div>',
										 '<div class="form-group">',
										 ' -- <input type="text" class="Wdate form-control input-sm input-sm-date" placeholder="结束值" name="filter_LE%(dt)s_%(fieldKey)s" id="query_%(id)sEnd"',
										 ' onclick="WdatePicker({dateFmt:\'%(fmt)s\'})" >',
										 '</div>',
										 '</div>',
										 '</div>'].join(""),data);
										$("#queryForm").append(strHtml);
										if($ES.isNotBlank(data.start)) {
											var queryKey = sprintf('#query_%(id)sStart',data);
											$(queryKey).val(data.start);
										}
										if($ES.isNotBlank(data.end)) {
											var queryKey = sprintf('#query_%(id)sEnd',data);
											$(queryKey).val(data.end);
										}
										break;
									case "ComboBox":
									case "ComboBoxSearch":
									case "CheckBox":
									case "RadioBox":
									case "ComboCheckBox":
									case "ComboRadioBox":
										strHtml = sprintf(['<div id="_qf_%(id)s_" class="row form-group form-group-sm">',
															 '<div class="col-sm-2 form-horizontal text-right">',
															 '<label for="query_%(id)s" class="control-label">%(text)s</label>',
															 '</div>',
															 '<div class="col-sm-10 form-inline">',
															 '<div class="form-group">',
															 '<select class="form-control combobox" name="filter_IN%(dt)s_%(fieldKey)s" id="query_%(id)s" multiple="true"><select>',
															 '</div>',
															 '</div>',
															 '</div>'].join(""), data);
										$("#queryForm").append(strHtml);
										var fieldKey = sprintf("#query_%(id)s", data);
										var selDataKey = sprintf("query_%(id)sJson", data);
										if(window[selDataKey]) {
											$ES.initComboBox(fieldKey, {"data":window[selDataKey],"width":"500px", "value":data.val});
										} else {
											var selDataParam = sprintf("query_%(id)sJsonParam", data);
											if(window[selDataParam]) {
												$ES.initComboBox(fieldKey, {"queryParams":window[selDataParam],"width":"500px", "value":data.val});
											}
										}
										break;
									case "ComboTree":
									case "DictTree":
										//alert("亲，暂不支持该控件查询！");
										strHtml = sprintf(['<div id="_qf_%(id)s_" class="row form-group form-group-sm">',
															 '<div class="col-sm-2 form-horizontal text-right">',
															 '<label for="query_%(id)s" class="control-label">%(text)s</label>',
															 '</div>',
															 '<div class="form-group">',
															 '<input type="text" class="form-control combotree" placeholder="%(text)s" name="filter_IN%(dt)s_%(fieldKey)s" multiple="true" id="query_%(id)s" notAutoWidth="true">',
															 '</div>',
															 '</div>',
															 '</div>'].join(""), data);
										$("#queryForm").append(strHtml);
										var fieldKey = sprintf("#query_%(id)s", data);
										var selDataKey = sprintf("query_%(id)sJson", data);
										if(window[selDataKey]) {
											$ES.initSelectTree(fieldKey, {"data":window[selDataKey],"value":data.val, "esType":(data.editType=="DictTree")?"DictTree":"", "theme":"bootstrap"});
										} else {
											var selDataParam = sprintf("query_%(id)sJsonParam", data);
											if(window[selDataParam]) {
												$ES.initSelectTree(fieldKey, {"queryParams":window[selDataParam],"value":data.val, "esType":(data.editType=="DictTree")?"DictTree":"", "theme":"bootstrap"});
											}
										}
										break;
									default:
										break;
										
								}
							}
							,onUnSelect:function(data) {
								$(sprintf('#_qf_%(id)s_', data)).remove();
							}});
						opt["isInit"] = true;
						$("#queryFieldSel").data("_data_",opt);
						var dk = "_advf_" + opt["entityName"];
						var valMap = store.get(dk);
						$ES.doInitQueryFieldSel(opt.data, valMap);
						$("#queryForm").on('keydown', 'input,textarea', function(e) {if(e.keyCode==13) {doQuery();return false;}});
					}
				},
				doResetAdvanceQuery:function(formKey) {
					var fk = "#" + (formKey || "queryForm");
					$(fk).find("select.combobox").val(null).trigger("change");
					$(fk).find("input,textarea").val("");
					//$("#queryForm")[0] && $("#queryForm")[0].reset();
				},//设置高级查询值，{"fieldKey":{val:"", oper:"", start:"", end:""}}
				doSetAdvanceQueryValue:function(valMap) {
					var opt = $("#queryFieldSel").data("_data_") || window._ad_query_opt_;
					if(opt) {
						$ES.initAdvanceQuery(opt);
						$ES.doInitQueryFieldSel(opt.data, valMap);
					}
				},
				doInitQueryFieldSel:function(data, valMap) {
					if(data && valMap) {
						var selKeyArr = $ES.getComboBoxVal("#queryFieldSel").split(",")||[];
						for(var key in valMap) {
							var mapVal = valMap[key];
							var rs = $ES.findInArray(data, "id", key);
							if(rs && rs.length) {
								var row = $.extend(true, {}, rs[0], mapVal);
								$("#queryFieldSel").trigger({
							        type: 'select2:select',
							        params: {
							            data: row
							        }
							    });
								selKeyArr.push(key);
							}
						}
						$ES.setComboBoxVal("#queryFieldSel", selKeyArr.join(","));
					}
				},
				doAddAQvToFavorite:function(callFunc) {
					var selKeyStr = $ES.getComboBoxVal("#queryFieldSel");
					var opt = $("#queryFieldSel").data("_data_");
					var dk = "_advf_" + opt["entityName"];
					if(selKeyStr) {
						var selKeyArr = selKeyStr.split(",");
						var data = {};
						selKeyArr.forEach(function(value, index) {
							data[value] = {};
						});
						store.set(dk, data);
						if(callFunc) callFunc(dk, data);
					} else {
						store.remove(dk);
						alert("已清空收藏！");
					}
				},
				findInArray:function(arr, key, keyVal) {
					return arr.filter(function(element, index, array) {
						return element[key] == keyVal;
					});
				}
			};	   