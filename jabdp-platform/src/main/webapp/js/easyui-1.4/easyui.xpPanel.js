/**
 * 初始化XP Panel
 */
function collapseAll() {
	$("#queryFormId .panel-title").each(function() {
		var title = $(this).text();
		$("#queryFormId").accordion("unselect", title);
	});
	$(this).removeClass("accordion-collapse");
	$(this).addClass("accordion-expand");
	$(this).attr("title", $.jwpf.system.button.expand);
}

function expandAll() {
	$("#queryFormId .panel-title").each(function() {
		var title = $(this).text();
		$("#queryFormId").accordion("select", title);
	});
	$(this).removeClass("accordion-expand");
	$(this).addClass("accordion-collapse");
	$(this).attr("title", $.jwpf.system.button.contract);
}

$(function() {
	$("#a_exp_clp").toggle(expandAll, collapseAll);
});