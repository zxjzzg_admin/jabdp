/**
 * 
 */
package com.qyxx.platform.sysmng.notice.dao;

import org.springframework.stereotype.Component;

import com.qyxx.platform.common.orm.hibernate.HibernateDao;
import com.qyxx.platform.sysmng.notice.entity.CommonType;

/**
 * @author yulq
 *
 */

/**
 * 
 * 系统公用分类Dao类
 *
 */
@Component
public class CommonTypeDao extends HibernateDao<CommonType, Long>{
	
	private static final String INSERT_COMMON_TYPE_TO_ROLE = "INSERT INTO SYS_COMMON_TYPE_TO_ROLE(TYPE_ID, ROLE_ID) VALUES(?, ?)";
	private static final String DELETE_COMMONTYPE_COMMONTYPEID = "DELETE FROM SYS_COMMON_TYPE_TO_ROLE WHERE TYPE_ID = ?";
	private static final String DELETE_COMMONTYPE_ROLE_ID = "DELETE FROM SYS_COMMON_TYPE_TO_ROLE WHERE ROLE_ID = ?";
	/**
	 * 往关系表中添加关联关系
	 * @param commonTypeId
	 * @param roleId
	 */
	public void saveCommonTypes(Long commonTypeId,Long roleId){
		 getSession().createSQLQuery(INSERT_COMMON_TYPE_TO_ROLE).setLong(0, commonTypeId).setLong(1,roleId).executeUpdate();
	}
	/**
	 * 根据CommonTypeId删除关系表中的记录
	 * @param CommonTypeId
	 */
	
	public void deleteCommonTypeId(Long cid){
		 getSession().createSQLQuery(DELETE_COMMONTYPE_COMMONTYPEID).setLong(0, cid).executeUpdate();
		 
	}
	/**
	 * 根据ROLE_ID删除关系表中的记录
	 * @param ROLE_ID
	 */
	
	public void deleteByRoleId(Long rid){
		 getSession().createSQLQuery(DELETE_COMMONTYPE_ROLE_ID).setLong(0, rid).executeUpdate();
		 
	}
}
