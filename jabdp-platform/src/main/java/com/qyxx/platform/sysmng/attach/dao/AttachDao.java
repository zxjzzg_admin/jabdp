package com.qyxx.platform.sysmng.attach.dao;

import org.springframework.stereotype.Component;

import com.qyxx.platform.common.orm.hibernate.HibernateDao;
import com.qyxx.platform.sysmng.attach.entity.Attach;

/**
 *  附件主表对象DAO类
 */

@Component
public class AttachDao extends HibernateDao<Attach, Long>{

}
