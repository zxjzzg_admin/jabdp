/*
 * @(#)ChangeLogManager.java
 * 2011-5-17 上午10:49:53
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.logmng.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.qyxx.platform.common.orm.Page;
import com.qyxx.platform.common.orm.PropertyFilter;
import com.qyxx.platform.sysmng.logmng.dao.ChangeLogDao;
import com.qyxx.platform.sysmng.logmng.entity.ChangeLog;



/**
 *  变更日志管理类
 *  
 *  @author gxj
 *  @version 1.0 2013年12月22日 下午2:41:27
 *  @since jdk1.6
 */
@Component
@Transactional
public class ChangeLogManager {

	private ChangeLogDao changeLogDao;
	
	/**
	 * @param changeLogDao
	 */
	@Autowired
	public void setChangeLogDao(ChangeLogDao changeLogDao) {
		this.changeLogDao = changeLogDao;
	}
	
	/**
	 * 使用属性过滤条件查询日志
	 */
	@Transactional(readOnly = true)
	public Page<ChangeLog> searchLog(final Page<ChangeLog> page, final List<PropertyFilter> filters) {
		return changeLogDao.findPage(page, filters);
	}

	


	//-- Log Manager --//
	@Transactional(readOnly = true)
	public ChangeLog getLog(Long id) {
		return changeLogDao.get(id);
	}
	
	/**
	 * 保存日志
	 * 
	 * @param entity
	 */
	public void saveLog(ChangeLog entity) {
		changeLogDao.save(entity);
	}

}
