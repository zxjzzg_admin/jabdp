/*
 * @(#)OnlineUserDao.java
 * 2011-5-16 下午11:56:17
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.accountmng.dao;

import org.springframework.stereotype.Component;

import com.qyxx.platform.common.orm.hibernate.HibernateDao;
import com.qyxx.platform.sysmng.accountmng.entity.OnlineUser;


/**
 *  在线用户
 *  @author gxj
 *  @version 1.0
 *  @since 1.6 2011-5-16 下午11:56:17
 */
@Component
public class OnlineUserDao extends HibernateDao<OnlineUser, Long>{
	
}
