/*
 * @(#)MailFactory.java
 * 2014-1-7 下午11:49:51
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.msg.service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.StringUtils;

import com.qyxx.platform.common.definition.Definition.DefinitionEntity;
import com.qyxx.platform.common.utils.mail.MailUtils;
import com.qyxx.platform.sysmng.dictmng.web.DefinitionCache;


/**
 *  邮件信息工厂类，负责获取服务邮箱
 *  @author gxj
 *  @version 1.0 2014-1-7 下午11:49:51
 *  @since jdk1.6 
 */

public class MailFactory {
	
	public static final String DICT_MAIL = "SYS_MAIL_DICT";//数据字典类型--邮箱
	
	public static final String SERVICE_MAIL_SMTP_HOST = "serviceMail.smtp.host";//服务邮箱smtp地址
	
	public static final String SERVICE_MAIL_NAME = "serviceMail.name";//服务邮箱名称
	
	public static final String SERVICE_MAIL_PASSWORD = "serviceMail.password";//服务邮箱密码

	private static Map<String, MailUtils> map = new ConcurrentHashMap<String, MailUtils>();//服务邮箱列表
	
	static {
		initServiceMail();
	}
	
	/**
	 * 获取服务邮箱，默认获取第一个
	 * 
	 * @return
	 */
	public synchronized static MailUtils getDefaultServiceMail() {
		return getServiceMail("");
	}
	
	/**
	 * 获取服务邮箱
	 * 
	 * @return
	 */
	public synchronized static MailUtils getServiceMail(String serviceMail) {
		if(StringUtils.isBlank(serviceMail)) {
			serviceMail = DefinitionCache.getEntityByValue(DICT_MAIL, SERVICE_MAIL_NAME).getDisplayName();
		}
		MailUtils sm = map.get(serviceMail);
		if(sm!=null) {
			return sm;
		} else {
			throw new RuntimeException(serviceMail+"对应的服务邮箱不存在");
		}
	}
	
	/**
	 * 更新服务邮箱
	 */
	public static void updateServiceMail(String suffix) {
		String sf = (suffix.length()>0) ? ("." + suffix) : suffix;
		DefinitionEntity df = DefinitionCache.getEntityByValue(DICT_MAIL, SERVICE_MAIL_NAME + sf);
		if(df!=null) {
			String userName = df.getDisplayName();
			if(StringUtils.isNotBlank(userName)) {
				String smtpHost = DefinitionCache.getEntityByValue(DICT_MAIL, SERVICE_MAIL_SMTP_HOST + sf).getDisplayName();
				String password = DefinitionCache.getEntityByValue(DICT_MAIL, SERVICE_MAIL_PASSWORD + sf).getDisplayName();
				MailUtils sm = new MailUtils(smtpHost, userName, password);
				map.put(userName, sm);
			}
		}
	}
	
	/**
	 * 默认初始化10个服务邮箱
	 */
	public static void initServiceMail() {
		String[] ss = new String[] {"","1","2","3","4","5","6","7","8","9"};
		for(String s : ss) {
			updateServiceMail(s);
		}
	}
	
	/**
	 * 重置邮箱设置
	 */
	public static void resetServiceMail() {
		map.clear();
		DefinitionCache.removeDefinition(DICT_MAIL);
		initServiceMail();
	}
	
}
