package com.qyxx.platform.sysmng.memo.dao;

import org.springframework.stereotype.Component;

import com.qyxx.platform.common.orm.hibernate.HibernateDao;
import com.qyxx.platform.sysmng.memo.entity.MemoToUser;

@Component
public class MemoToUserDao extends HibernateDao<MemoToUser, Long> {

	private static final String DELETE_BY_MEMO_ID = "delete from MemoToUser where memo.id= ?";
	
	/**
	 * 根据备忘录ID删除通知用户
	 * 
	 * @param memoId
	 */
	public void deleteByMemoId(Long memoId) {
		getSession().createQuery(DELETE_BY_MEMO_ID).setLong(0, memoId).executeUpdate();
	}
	
}
