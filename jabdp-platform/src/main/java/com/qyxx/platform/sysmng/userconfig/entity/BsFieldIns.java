/*
 * @(#)BsFieldIns.java
 * 2016年10月12日 下午4:36:29
 * 
 * Copyright (c) 2016 QiYunInfoTech - All Rights Reserved.
 * ====================================================================
 * The QiYunInfoTech License, Version 1.0
 *
 * This software is the confidential and proprietary information of QiYunInfoTech.
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into with QiYunInfoTech.
 */
package com.qyxx.platform.sysmng.userconfig.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.qyxx.jwp.bean.DataProperties;
import com.qyxx.jwp.bean.EditProperties;
import com.qyxx.platform.common.module.entity.BaseEntity;
import com.qyxx.platform.sysmng.utils.Constants;

/**
 *  业务字段实例表，存放字段值
 *  @author bobgao
 *  @version 1.0 2016年10月12日 下午4:36:29
 *  @since jdk1.7 
 */
@Entity
//表名与类名不相同时重新定义表名.
@Table(name = "SYS_BS_FIELD_INS")
//默认的缓存策略.
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class BsFieldIns extends BaseEntity {

	private Long id;//主键
	
	private Long masterId;//关联属性配置表 id
	
	private String tableKey;//编码
	
	private String entityName;//实体名
	
	private String entityId;//实体值
	
	private String fieldKey;//字段编码
	
	private String caption;//字段名称
	
	private String dataType = DataProperties.DATA_TYPE_STRING;//数据类型，默认字符串
	
	private String editType = EditProperties.EDIT_TYPE_TEXT_BOX;//编辑类型/控件类型，默认文本框
	
	private String status = Constants.STATUS_PASS;//状态--默认审批通过
	
	private String stringVal;//字符串值
	
	private Long longVal;//整数值
	
	private Double doubleVal;//小数值
	
	private String textVal;//文本值
	
	private Date dateVal;//日期值
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="SEQ_SYS_BS_FIELD_INS_ID")
	@SequenceGenerator(name="SEQ_SYS_BS_FIELD_INS_ID", sequenceName="SEQ_SYS_BS_FIELD_INS_ID")
    public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	
	/**
	 * @return masterId
	 */
	@Column(name="MASTER_ID")
	public Long getMasterId() {
		return masterId;
	}

	
	/**
	 * @param masterId
	 */
	public void setMasterId(Long masterId) {
		this.masterId = masterId;
	}

	
	/**
	 * @return tableKey
	 */
	@Column(name="TABLE_KEY", length=200)
	public String getTableKey() {
		return tableKey;
	}

	
	/**
	 * @param tableKey
	 */
	public void setTableKey(String tableKey) {
		this.tableKey = tableKey;
	}

	
	/**
	 * @return entityName
	 */
	@Column(name="ENTITY_NAME", length=400)
	public String getEntityName() {
		return entityName;
	}

	
	/**
	 * @param entityName
	 */
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	
	/**
	 * @return entityId
	 */
	@Column(name="ENTITY_ID", length=100)
	public String getEntityId() {
		return entityId;
	}

	
	/**
	 * @param entityId
	 */
	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	
	/**
	 * @return fieldKey
	 */
	@Column(name="FIELD_KEY", length=200)
	public String getFieldKey() {
		return fieldKey;
	}

	
	/**
	 * @param fieldKey
	 */
	public void setFieldKey(String fieldKey) {
		this.fieldKey = fieldKey;
	}

	
	/**
	 * @return caption
	 */
	@Column(name="CAPTION", length=200)
	public String getCaption() {
		return caption;
	}

	
	/**
	 * @param caption
	 */
	public void setCaption(String caption) {
		this.caption = caption;
	}

	
	/**
	 * @return dataType
	 */
	@Column(name="DATA_TYPE", length=100)
	public String getDataType() {
		return dataType;
	}

	
	/**
	 * @param dataType
	 */
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	/**
	 * @return editType
	 */
	@Column(name="EDIT_TYPE", length=100)
	public String getEditType() {
		return editType;
	}

	
	/**
	 * @param editType
	 */
	public void setEditType(String editType) {
		this.editType = editType;
	}
	
	/**
	 * @return status
	 */
	@Column(name="STATUS", length=100)
	public String getStatus() {
		return status;
	}

	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return stringVal
	 */
	@Column(name="STRING_VAL", length=4000)
	public String getStringVal() {
		return stringVal;
	}

	
	/**
	 * @param stringVal
	 */
	public void setStringVal(String stringVal) {
		this.stringVal = stringVal;
	}

	
	/**
	 * @return longVal
	 */
	@Column(name="LONG_VAL")
	public Long getLongVal() {
		return longVal;
	}

	
	/**
	 * @param longVal
	 */
	public void setLongVal(Long longVal) {
		this.longVal = longVal;
	}

	
	/**
	 * @return doubleVal
	 */
	@Column(name="DOUBLE_VAL")
	public Double getDoubleVal() {
		return doubleVal;
	}

	
	/**
	 * @param doubleVal
	 */
	public void setDoubleVal(Double doubleVal) {
		this.doubleVal = doubleVal;
	}

	
	/**
	 * @return textVal
	 */
	@Lob
	@Column(name="TEXT_VAL", columnDefinition = "ntext")
	public String getTextVal() {
		return textVal;
	}

	
	/**
	 * @param textVal
	 */
	public void setTextVal(String textVal) {
		this.textVal = textVal;
	}

	
	/**
	 * @return dateVal
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATE_VAL")
	public Date getDateVal() {
		return dateVal;
	}

	
	/**
	 * @param dateVal
	 */
	public void setDateVal(Date dateVal) {
		this.dateVal = dateVal;
	}
	
}
