package com.qyxx.platform.sysmng.memo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 *  @author tsd
 *  @version 1.0 2015-3-16 下午04:09:37
 *  @since jdk1.6 
 *  用户备忘录关联表
 */

@Entity
@Table(name = "SYS_MEMO_TO_USER")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class MemoToUser {
	public static final String STATUS_0="0";//0.等待提醒  1.正在提醒 2.过期提醒
	public static final String STATUS_1="1";
	public static final String STATUS_2="2";
	private Long id;
	private Memo memo;
	private Long userId;
	private String status;//0.未读  1.阅读
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_SYS_MEMO_TO_USER_ID")
	@SequenceGenerator(name = "SEQ_SYS_MEMO_TO_USER_ID", sequenceName = "SEQ_SYS_MEMO_TO_USER_ID")
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@ManyToOne
	@JoinColumn(name = "MEMO_ID")
	public Memo getMemo() {
		return memo;
	}
	
	public void setMemo(Memo memo) {
		this.memo = memo;
	}
	
	@Column(name = "USER_ID")
	public Long getUserId() {
		return userId;
	}
	
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	@Column(name = "STATUS", length = 1)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}
