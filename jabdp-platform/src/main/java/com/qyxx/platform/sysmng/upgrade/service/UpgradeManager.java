/*
 * @(#)UpgradeManager.java
 * 2012-12-19 上午10:35:08
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.upgrade.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.qyxx.platform.common.orm.Page;
import com.qyxx.platform.common.orm.PropertyFilter;
import com.qyxx.platform.sysmng.upgrade.dao.UpgradeDao;
import com.qyxx.platform.sysmng.upgrade.entity.Upgrade;


/**
 *  部署日志Service
 *  @author gxj
 *  @version 1.0 2012-12-19 上午10:35:08
 *  @since jdk1.6 
 */
@Service
@Transactional
public class UpgradeManager {
	
	private UpgradeDao upgradeDao;
	
	/**
	 * @return upgradeDao
	 */
	public UpgradeDao getUpgradeDao() {
		return upgradeDao;
	}

	
	/**
	 * @param upgradeDao
	 */
	@Autowired
	public void setUpgradeDao(UpgradeDao upgradeDao) {
		this.upgradeDao = upgradeDao;
	}
	
	/**
	 * 使用属性过滤条件查询日志
	 * 
	 * @param page
	 * @param filters
	 * @return
	 */
	@Transactional(readOnly = true)
	public Page<Upgrade> searchUpgradeLog(final Page<Upgrade> page, final List<PropertyFilter> filters) {
		return upgradeDao.findPage(page, filters);
	}

	//-- Log Manager --//
	@Transactional(readOnly = true)
	public Upgrade getUpgradeLog(Long id) {
		return upgradeDao.get(id);
	}
	
	/**
	 * 保存日志
	 * 
	 * @param entity
	 */
	public void saveUpgradeLog(Upgrade entity) {
		upgradeDao.save(entity);
	}

}
