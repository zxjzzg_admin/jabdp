/*
 * @(#)TaskSchedulerAction.java
 * 2013-2-25 下午03:00:53
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.task.web;

import java.util.List;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Namespace;
import org.springframework.beans.factory.annotation.Autowired;

import com.qyxx.platform.common.json.JsonOutput;
import com.qyxx.platform.common.module.web.CrudActionSupport;
import com.qyxx.platform.common.orm.PropertyFilter;
import com.qyxx.platform.common.utils.web.struts2.Struts2Utils;
import com.qyxx.platform.sysmng.accountmng.entity.User;
import com.qyxx.platform.sysmng.task.entity.TaskScheduler;
import com.qyxx.platform.sysmng.task.service.TaskSchedulerManager;
import com.qyxx.platform.sysmng.utils.Constants;


/**
 *  任务调度Action
 *  @author gxj
 *  @version 1.0 2013-2-25 下午03:00:53
 *  @since jdk1.6 
 */
@Namespace("/sys/task")
public class TaskSchedulerAction extends CrudActionSupport<TaskScheduler>{

	private static final long serialVersionUID = 8236823179720088429L;

	private TaskSchedulerManager taskSchedulerManager;
	
	/**
	 * @return taskSchedulerManager
	 */
	public TaskSchedulerManager getTaskSchedulerManager() {
		return taskSchedulerManager;
	}

	
	/**
	 * @param taskSchedulerManager
	 */
	@Autowired
	public void setTaskSchedulerManager(
			TaskSchedulerManager taskSchedulerManager) {
		this.taskSchedulerManager = taskSchedulerManager;
	}

	@Override
	public String list() throws Exception {
		// TODO Auto-generated method stub
		return SUCCESS;
	}
	
	@JsonOutput
	public String queryList() throws Exception {
		List<PropertyFilter> filters = PropertyFilter.buildFromHttpRequest(Struts2Utils.getRequest());
		pager = taskSchedulerManager.searchTaskScheduler(pager, filters);
		Struts2Utils.renderJson(pager);
		return null;
	}

	@Override
	public String input() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@JsonOutput
	public String save() throws Exception {
		User user = (User)Struts2Utils.getSessionAttribute(Constants.USER_SESSION_ID);
		taskSchedulerManager.saveTaskScheduler(entity, user.getId(), ServletActionContext.getServletContext());
		formatMessage.setMsg(entity.getId());
		Struts2Utils.renderJson(formatMessage);
		return null;
	}

	@Override
	@JsonOutput
	public String view() throws Exception {
		formatMessage.setMsg(entity);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}

	@Override
	@JsonOutput
	public String delete() throws Exception {
		taskSchedulerManager.deleteTaskScheduler(ids);
		formatMessage.setMsg(ids);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	
	/**
	 * 启动任务调度
	 * 
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
	public String startTaskScheduler() throws Exception {
		taskSchedulerManager.startTaskScheduler(id, ServletActionContext.getServletContext());
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	
	/**
	 * 停止任务调度
	 * 
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
	public String stopTaskScheduler() throws Exception {
		taskSchedulerManager.stopTaskScheduler(id, ServletActionContext.getServletContext());
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	
	@Override
	protected void prepareModel() throws Exception {
		try {
			if (id != null) {
				entity = taskSchedulerManager.getTaskScheduler(id);
			} else {
				entity = new TaskScheduler();
			}	
		} catch(Exception e) {
			logger.error("", e);
		}
	}

	

}
