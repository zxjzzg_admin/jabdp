/*
 * @(#)ExceptionManager.java
 * 2011-5-31 下午09:23:13
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.logmng.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.qyxx.platform.common.module.service.ServiceException;
import com.qyxx.platform.common.orm.Page;
import com.qyxx.platform.common.orm.PropertyFilter;
import com.qyxx.platform.sysmng.logmng.dao.ExceptionLogDao;
import com.qyxx.platform.sysmng.logmng.entity.ExceptionLog;


/**
 *  异常日志查询Manager
 *  @author gxj
 *  @version 1.0
 *  @since 1.6 2011-5-31 下午09:23:13
 */
@Service
@Transactional
public class ExceptionLogManager {
	
	private ExceptionLogDao exceptionLogDao;

	/**
	 * @return exceptionLogDao
	 */
	public ExceptionLogDao getExceptionLogDao() {
		return exceptionLogDao;
	}

	
	/**
	 * @param exceptionLogDao
	 */
	@Autowired
	public void setExceptionLogDao(ExceptionLogDao exceptionLogDao) {
		this.exceptionLogDao = exceptionLogDao;
	}
	
	/**
	 * 使用属性过滤条件查询
	 */
	@Transactional(readOnly = true)
	public Page<ExceptionLog> searchExpLog(final Page<ExceptionLog> page, final List<PropertyFilter> filters) {
		return exceptionLogDao.findPage(page, filters);
	}

	@Transactional(readOnly = true)
	public ExceptionLog getExpLog(Long id) {
		return exceptionLogDao.get(id);
	}

}
