package com.qyxx.platform.sysmng.attach.dao;

import org.springframework.stereotype.Component;

import com.qyxx.platform.common.orm.hibernate.HibernateDao;
import com.qyxx.platform.sysmng.attach.entity.AttachList;


/**
 *  附件明细表对象DAO类
 */
@Component
public class AttachListDao extends HibernateDao<AttachList, Long>{

}
