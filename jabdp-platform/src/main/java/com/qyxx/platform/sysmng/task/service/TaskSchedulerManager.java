/*
 * @(#)TaskSchedulerService.java
 * 2013-2-25 下午02:59:38
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.task.service;

import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.qyxx.platform.common.orm.Page;
import com.qyxx.platform.common.orm.PropertyFilter;
import com.qyxx.platform.sysmng.accountmng.dao.UserDao;
import com.qyxx.platform.sysmng.task.core.QuartzManager;
import com.qyxx.platform.sysmng.task.dao.TaskSchedulerDao;
import com.qyxx.platform.sysmng.task.entity.TaskScheduler;


/**
 *  任务调度Service
 *  @author gxj
 *  @version 1.0 2013-2-25 下午02:59:38
 *  @since jdk1.6 
 */
@Service
@Transactional
public class TaskSchedulerManager {

	private TaskSchedulerDao taskSchedulerDao;
	
	private UserDao userDao;
	
	/**
	 * @param userDao
	 */
	@Autowired
	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	/**
	 * @return taskSchedulerDao
	 */
	public TaskSchedulerDao getTaskSchedulerDao() {
		return taskSchedulerDao;
	}

	
	/**
	 * @param taskSchedulerDao
	 */
	@Autowired
	public void setTaskSchedulerDao(TaskSchedulerDao taskSchedulerDao) {
		this.taskSchedulerDao = taskSchedulerDao;
	}

	/**
	 * 分页查询任务调度
	 * 
	 * @param page
	 * @param filters
	 * @return
	 */
	@Transactional(readOnly = true)
	public Page<TaskScheduler> searchTaskScheduler(final Page<TaskScheduler> page, final List<PropertyFilter> filters) {
		return preHandlePage(taskSchedulerDao.findPage(page, filters));
	}
	
	/**
	 * 查询所有运行任务调度
	 * 
	 * @return
	 */
	@Transactional(readOnly = true)
	public List<TaskScheduler> findAllRunningTaskScheduler() {
		return taskSchedulerDao.findBy("status", TaskScheduler.STATUS_RUN);
	}

	/**
	 * 查询任务调度
	 * 
	 * @param id
	 * @return
	 */
	@Transactional(readOnly = true)
	public TaskScheduler getTaskScheduler(Long id) {
		return taskSchedulerDao.get(id);
	}
	
	/**
	 * 保存任务调度
	 * 
	 * @param entity
	 * @param userId
	 */
	public void saveTaskScheduler(TaskScheduler entity, Long userId, ServletContext sc) {
		if(null!=entity.getId()) {
			entity.setLastUpdateUser(userId);
			entity.setLastUpdateTime(new Date());
		} else {
			entity.setCreateUser(userId);
			entity.setCreateTime(new Date());
		}
		taskSchedulerDao.save(entity);
		//如果为运行状态，则需要直接启动任务调度
		if(TaskScheduler.STATUS_RUN.equals(entity.getStatus())) {
			QuartzManager.startTask(entity, sc);
		}
	}
	
	/**
	 * 批量删除任务调度
	 * 
	 * @param ids
	 */
	public void deleteTaskScheduler(Long[] ids) {
		if(null != ids) {
			for(Long id : ids) {
				taskSchedulerDao.delete(id);
			}
		}
	}
	
	/**
	 * 启动任务调度
	 * 
	 * @param id
	 * @param sc
	 */
	public void startTaskScheduler(Long id, ServletContext sc) {
		TaskScheduler ts = getTaskScheduler(id);
		ts.setStatus(TaskScheduler.STATUS_RUN);
		QuartzManager.startTask(ts, sc);
	}
	
	/**
	 * 停止任务调度
	 * 
	 * @param id
	 * @param sc
	 */
	public void stopTaskScheduler(Long id, ServletContext sc) {
		TaskScheduler ts = getTaskScheduler(id);
		ts.setStatus(TaskScheduler.STATUS_STOP);
		QuartzManager.stopTask(ts, sc);
	}
	
	/**
	 * 处理Page对象，翻译字段
	 * 
	 * @param page
	 * @return
	 */
	public Page<TaskScheduler> preHandlePage(Page<TaskScheduler> page) {
		List<TaskScheduler> list = page.getResult();
		if(list != null) {
			for(TaskScheduler n : list) {
				n.setCreateUserCaption(userDao.getUserNameById(n.getCreateUser()));
			}
		}
		return page;
	}
	
	
}
