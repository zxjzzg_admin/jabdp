/*
 * @(#)Organization.java
 * 2011-5-22 下午03:37:38
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.accountmng.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.common.collect.Lists;
import com.qyxx.platform.common.module.entity.BaseEntity;
import com.qyxx.platform.sysmng.notice.entity.News;


/**
 * 
 * @author YB
 * @version 1.0
 * @since 1.6 2011-5-22 下午03:37:38
 */
@Entity
// 表名与类名不相同时重新定义表名.
@Table(name = "SYS_ORGANIZATION")
// 默认的缓存策略.
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonPropertyOrder(value={"id", "text","state","icon","children"}) 
public class Organization extends BaseEntity implements Serializable {

	/**
	 * long
	 */
	private static final long serialVersionUID = -156347084563885457L;
	public static final Integer CODE_BIT_LEN = 5;//每级编码位数
	private Long id;
	private Organization organization;
	private String organizationName;
	private Long organizationNo;
	private String organizationDesc;
	private List<Organization> organizationList = Lists.newArrayList();
    private String icon="ok";
    private String state="closed";
    private String organizationCode;
    private String parentOrganizationCode;
    private List<News> newsList = Lists.newArrayList();
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_SYS_ORG_ID")
	@SequenceGenerator(name = "SEQ_SYS_ORG_ID", sequenceName = "SEQ_SYS_ORG_ID")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(targetEntity = Organization.class, fetch = FetchType.LAZY)
	@JoinColumn(name = "PARENT_ID")
	@JsonIgnore
	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	@Column(nullable = false)
	@JsonProperty(value="text")
	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}
	
	@JsonIgnore
	public Long getOrganizationNo() {
		return organizationNo;
	}

	public void setOrganizationNo(Long organizationNo) {
		this.organizationNo = organizationNo;
	}

	@Column(length = 500)
	@JsonIgnore
	public String getOrganizationDesc() {
		return organizationDesc;
	}

	public void setOrganizationDesc(String organizationDesc) {
		this.organizationDesc = organizationDesc;
	}

	@OneToMany(mappedBy="organization")
	@Fetch(FetchMode.SUBSELECT)
	@OrderBy("id")
	@JsonProperty(value="children")
	public List<Organization> getOrganizationList() {
		return organizationList;
	}

	
	public void setOrganizationList(List<Organization> organizationList) {
		this.organizationList = organizationList;
	}

	@Transient
	public String getIcon() {
		return icon;
	}

	
	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	@Transient
	public String getState() {
		if(this.getOrganization()==null||this.getOrganizationList()==null||this.getOrganizationList().size()==0)
		{
			state="open";	
		}
	   return state;
	}

	
	public void setState(String state) {
		this.state = state;
	}

	@Column(name="ORGANIZATION_CODE",length=2000)
	public String getOrganizationCode() {
		return organizationCode;
	}

	
	public void setOrganizationCode(String organizationCode) {
		this.organizationCode = organizationCode;
	}

	@Column(name="PARENT_ORGANIZATION_CODE",length=2000)
	public String getParentOrganizationCode() {
		return parentOrganizationCode;
	}

	
	public void setParentOrganizationCode(String parentOrganizationCode) {
		this.parentOrganizationCode = parentOrganizationCode;
	}

	@ManyToMany
	@JoinTable(name = "SYS_NEWS_TO_ORG", joinColumns = { @JoinColumn(name = "ORG_ID") }, inverseJoinColumns = { @JoinColumn(name = "NEWS_ID") })
	@Fetch(FetchMode.SUBSELECT)
	@OrderBy("id")
	@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
	@JsonIgnore
	public List<News> getNewsList() {
		return newsList;
	}

	
	public void setNewsList(List<News> newsList) {
		this.newsList = newsList;
	}
	@Transient
	public static List<String> getSplitCodeList(String code){
		List<String> sList = Lists.newArrayList();
		int len = code.length();
		for(int i=CODE_BIT_LEN; i<len; i=i+CODE_BIT_LEN) {  
			String s = code.substring(0, i);
			sList.add(s);			
		}
		sList.add(code);
		return sList;	
	}

}
