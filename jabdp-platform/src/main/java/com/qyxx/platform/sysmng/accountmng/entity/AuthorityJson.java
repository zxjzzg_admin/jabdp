package com.qyxx.platform.sysmng.accountmng.entity;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Lists;

/**
 * 权限Json对象
 */
@JsonAutoDetect(getterVisibility = Visibility.ANY)
public class AuthorityJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String resourceName;
	private Long parentId;
	private Boolean checked;
	private String iconSkin;//定义ztree的图标
	private String  resourceStatus;//资源状态
	private List<AuthorityJson> childs = Lists.newArrayList();//子资源的集合

	public AuthorityJson(Authority a) {
		super();
		this.id = a.getId();
		this.resourceName = a.getResourceName();
		if (null != a.getAuthority()) {
			this.parentId = a.getAuthority().getId();
		}
		 if(a.getResourceType().equals("0")){
			this.iconSkin = "iconMenu";
		}else if (a.getResourceType().equals("1")){
			this.iconSkin = "iconButton";
		}else if (a.getResourceType().equals("2")){
			this.iconSkin = "iconColumn";
		}else if(a.getResourceType().equals("3")){
			this.iconSkin = "iconData";
		}
		 this.resourceStatus = a.getResourceStatus();
		 
	}
	
	public AuthorityJson(Long id, String resourceName, Long pid, String resourceType, String resourceStatus) {
		super();
		this.id = id;
		this.resourceName = resourceName;
		this.parentId = pid;
		 if("0".equals(resourceType)){
			this.iconSkin = "iconMenu";
		}else if ("1".equals(resourceType)){
			this.iconSkin = "iconButton";
		}else if ("2".equals(resourceType)){
			this.iconSkin = "iconColumn";
		}else if("3".equals(resourceType)){
			this.iconSkin = "iconData";
		}
		this.resourceStatus = resourceStatus;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getChecked() {
		return checked;
	}

	public void setChecked(Boolean checked) {
		this.checked = checked;
	}

	@JsonProperty(value = "name")
	public String getResourceName() {
		return resourceName;
	}

	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	@JsonProperty(value = "pId")
	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public List<AuthorityJson> getChilds() {
		return childs;
	}

	public void setChilds(List<AuthorityJson> childs) {
		this.childs = childs;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getIconSkin() {
		return iconSkin;
	}

	public void setIconSkin(String iconSkin) {
		this.iconSkin = iconSkin;
	}

	public String getResourceStatus() {
		return resourceStatus;
	}

	
	public void setResourceStatus(String resourceStatus) {
		this.resourceStatus = resourceStatus;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((childs == null) ? 0 : childs.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((parentId == null) ? 0 : parentId.hashCode());
		result = prime
				* result
				+ ((resourceName == null) ? 0 : resourceName
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AuthorityJson other = (AuthorityJson) obj;
		if (childs == null) {
			if (other.childs != null)
				return false;
		} else if (!childs.equals(other.childs))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (parentId == null) {
			if (other.parentId != null)
				return false;
		} else if (!parentId.equals(other.parentId))
			return false;
		if (resourceName == null) {
			if (other.resourceName != null)
				return false;
		} else if (!resourceName.equals(other.resourceName))
			return false;
		return true;
	}

}
