/*
 * @(#)OrganizationJson.java
 * 2011-6-4 下午04:46:28
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.accountmng.entity;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 用来封装json格式和实体对象有所区别
 * 
 * @author YB
 * @version 1.0
 * @since 1.6 2011-6-4 下午04:46:28
 */
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class OrganizationJson implements Serializable {

	/**
	 * long
	 */
	private static final long serialVersionUID = -722597138724211444L;
	private Long id;
	private Long parentId;
	private Long pId;
	private String parentName;
	private String organizationName;
	private Long organizationNo;
	private String organizationDesc;
	private String iconSkin;
	private String name;
	private String organizationCode;
	private String parentOrganizationCode;
	private Boolean checked;
	private String text;
	
	public String getOrganizationCode() {
		return organizationCode;
	}
	
	public void setOrganizationCode(String organizationCode) {
		this.organizationCode = organizationCode;
	}
	
	public String getParentOrganizationCode() {
		return parentOrganizationCode;
	}
	
	public void setParentOrganizationCode(String parentOrganizationCode) {
		this.parentOrganizationCode = parentOrganizationCode;
	}
	public OrganizationJson() {
		super();
	}
	public OrganizationJson(Long id,Organization organization) {
		super();
		this.id = id;
		if(null!=organization.getOrganization()) {
			Long parentId = organization.getOrganization().getId();
			this.pId = parentId;
		}
		this.name = organization.getOrganizationName();
		this.text = this.name;
		this.iconSkin = "iconGroup";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getIconSkin() {
		return iconSkin;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
		this.text = name;
	}
	public Long getpId() {
		return pId;
	}

	public void setpId(Long pId) {
		this.pId = pId;
	}

	public void setIconSkin(String iconSkin) {
		this.iconSkin = iconSkin;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public Long getOrganizationNo() {
		return organizationNo;
	}

	public void setOrganizationNo(Long organizationNo) {
		this.organizationNo = organizationNo;
	}

	public String getOrganizationDesc() {
		return organizationDesc;
	}

	public void setOrganizationDesc(String organizationDesc) {
		this.organizationDesc = organizationDesc;
	}

	public Boolean getChecked() {
		return checked;
	}

	public void setChecked(Boolean checked) {
		this.checked = checked;
	}

	
	/**
	 * @return text
	 */
	public String getText() {
		return text;
	}

	
	/**
	 * @param text
	 */
	public void setText(String text) {
		this.text = text;
	}
	
	

}
