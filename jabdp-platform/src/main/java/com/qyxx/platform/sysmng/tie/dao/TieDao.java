package com.qyxx.platform.sysmng.tie.dao;

import org.springframework.stereotype.Component;

import com.qyxx.platform.common.orm.hibernate.HibernateDao;
import com.qyxx.platform.sysmng.tie.entity.Tie;

@Component
public class TieDao extends HibernateDao<Tie, Long> {

}
