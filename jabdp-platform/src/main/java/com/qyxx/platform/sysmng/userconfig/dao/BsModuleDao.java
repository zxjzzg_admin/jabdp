/*
 * @(#)BsModuleDao.java
 * 2017年4月15日 上午9:18:34
 * 
 * Copyright (c) 2017 QiYunInfoTech - All Rights Reserved.
 * ====================================================================
 * The QiYunInfoTech License, Version 1.0
 *
 * This software is the confidential and proprietary information of QiYunInfoTech.
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into with QiYunInfoTech.
 */
package com.qyxx.platform.sysmng.userconfig.dao;

import org.springframework.stereotype.Component;

import com.qyxx.platform.common.orm.hibernate.HibernateDao;
import com.qyxx.platform.sysmng.userconfig.entity.BsModule;

/**
 *  业务模块Dao
 *  @author bobgao
 *  @version 1.0 2017年4月15日 上午9:18:34
 *  @since jdk1.7 
 */
@Component
public class BsModuleDao extends HibernateDao<BsModule, Long>{

}
