/**
 * Copyright (c) 2005-20010 springside.org.cn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * 
 * $Id: PropertyFilter.java 1205 2010-09-09 15:12:17Z calvinxiu $
 */
package com.qyxx.platform.common.orm;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;

import com.google.common.collect.Lists;
import com.qyxx.platform.common.utils.encode.JsonBinder;
import com.qyxx.platform.common.utils.reflection.ConvertUtils;
import com.qyxx.platform.common.utils.web.ServletUtils;

/**
 * 与具体ORM实现无关的属性过滤条件封装类, 主要记录页面中简单的搜索过滤条件.
 * 
 * @author gxj
 */
public class PropertyFilter {

	/** 多个属性间OR关系的分隔符. */
	public static final String OR_SEPARATOR = "_OR_";
	
	/** IN条件值分隔符 */
	public static final String SPLIT_MARK = ",";

	/** 属性比较类型. */
	public enum MatchType {
		EQ, LIKE, LT, GT, LE, GE, IN, NE, USER, LIKEE, LIKEL, LIKER;
	}

	/** 属性数据类型. */
	public enum PropertyType {
		S(String.class), I(Integer.class), L(Long.class), N(Double.class), D(Date.class), B(Boolean.class), F(Float.class), C(BigDecimal.class), T(BigInteger.class);

		private Class<?> clazz;

		private PropertyType(Class<?> clazz) {
			this.clazz = clazz;
		}

		public Class<?> getValue() {
			return clazz;
		}
	}

	private MatchType matchType = null;
	private Object matchValue = null;
	private List<Object> values = Lists.newArrayList();

	private Class<?> propertyClass = null;
	private String[] propertyNames = null;
	
	private Boolean isInMatchType = false;
	
	//或 过滤条件
	private List<PropertyFilter> orPfList = Lists.newArrayList();
	
	private static JsonBinder jsonBinder = JsonBinder.getAlwaysMapper();

	public PropertyFilter() {
	}

	/**
	 * @param filterName 比较属性字符串,含待比较的比较类型、属性值类型及属性列表. 
	 *                   eg. LIKES_NAME_OR_LOGIN_NAME
	 * @param value 待比较的值.
	 */
	public PropertyFilter(final String filterName, final String value) {

		String firstPart = StringUtils.substringBefore(filterName, "_");
		String matchTypeCode = StringUtils.substring(firstPart, 0, firstPart.length() - 1);
		String propertyTypeCode = StringUtils.substring(firstPart, firstPart.length() - 1, firstPart.length());

		try {
			matchType = Enum.valueOf(MatchType.class, matchTypeCode);
		} catch (RuntimeException e) {
			throw new IllegalArgumentException("filter名称" + filterName + "没有按规则编写,无法得到属性比较类型.", e);
		}

		try {
			propertyClass = Enum.valueOf(PropertyType.class, propertyTypeCode).getValue();
		} catch (RuntimeException e) {
			throw new IllegalArgumentException("filter名称" + filterName + "没有按规则编写,无法得到属性值类型.", e);
		}

		String propertyNameStr = StringUtils.substringAfter(filterName, "_");
		Assert.isTrue(StringUtils.isNotBlank(propertyNameStr), "filter名称" + filterName + "没有按规则编写,无法得到属性名称.");
		propertyNames = StringUtils.splitByWholeSeparator(propertyNameStr, PropertyFilter.OR_SEPARATOR);

		switch(matchType) {
			case IN:
				isInMatchType = true;
				String[] vals = StringUtils.split(value, SPLIT_MARK);
				if(vals != null) {
					for(String val : vals) {
						Object obj = ConvertUtils.convertStringToObject(val, propertyClass);
						this.values.add(obj);
					}
				}
				break;
			default:
				this.matchValue = ConvertUtils.convertStringToObject(value, propertyClass);
		}
	}

	/**
	 * 从HttpRequest中创建PropertyFilter列表, 默认Filter属性名前缀为filter.
	 * 
	 * @see #buildFromHttpRequest(HttpServletRequest, String)
	 */
	public static List<PropertyFilter> buildFromHttpRequest(final HttpServletRequest request) {
		return buildFromHttpRequest(request, "filter");
	}

	/**
	 * 从HttpRequest中创建PropertyFilter列表
	 * PropertyFilter命名规则为Filter属性前缀_比较类型属性类型_属性名.
	 * 
	 * eg.
	 * filter_EQS_name
	 * filter_LIKES_name_OR_email
	 */
	public static List<PropertyFilter> buildFromHttpRequest(final HttpServletRequest request, final String filterPrefix) {
		List<PropertyFilter> filterList = new ArrayList<PropertyFilter>();
		//添加filter属性对应的参数
		filterList.addAll(buildFilterParamFromRequest(request, "filter"));
		
		//从request中获取含属性前缀名的参数,构造去除前缀名后的参数Map.
		Map<String, Object> filterParamMap = ServletUtils.getParametersStartingWith(request, filterPrefix + "_");

		//分析参数Map,构造PropertyFilter列表
		for (Map.Entry<String, Object> entry : filterParamMap.entrySet()) {
			String filterName = entry.getKey();
			String value = (String) entry.getValue();
			//如果value值为空,则忽略此filter.
			if (StringUtils.isNotBlank(value)) {
				PropertyFilter filter = new PropertyFilter(filterName, value);
				filterList.add(filter);
			}
		}

		return filterList;
	}
	
	/**
	 * 从HttpRequest中创建PropertyFilter列表，不过滤null、""值
	 * PropertyFilter命名规则为Filter属性前缀_比较类型属性类型_属性名.
	 * 
	 * eg.
	 * filter_EQS_name
	 * filter_LIKES_name_OR_email
	 */
	public static List<PropertyFilter> buildFromHttpRequestIncludeEmpty(final HttpServletRequest request, final String filterPrefix) {
		List<PropertyFilter> filterList = new ArrayList<PropertyFilter>();
		//添加filter属性对应的参数
		filterList.addAll(buildFilterParamFromRequest(request, "filter"));
		//从request中获取含属性前缀名的参数,构造去除前缀名后的参数Map.
		Map<String, Object> filterParamMap = ServletUtils.getParametersStartingWith(request, filterPrefix + "_");

		//分析参数Map,构造PropertyFilter列表
		for (Map.Entry<String, Object> entry : filterParamMap.entrySet()) {
			String filterName = entry.getKey();
			String value = (String) entry.getValue();
			PropertyFilter filter = new PropertyFilter(filterName, value);
			filterList.add(filter);
		}

		return filterList;
	}
	
	/**
	 * 从请求参数中获取过滤对象
	 * @param request
	 * @param filterParamName
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static List<PropertyFilter> buildFilterParamFromRequest(final HttpServletRequest request, final String filterParamName) {
		List<PropertyFilter> filterList = new ArrayList<PropertyFilter>();
		String jsonStr = request.getParameter(filterParamName);
		if(StringUtils.isNotBlank(jsonStr)) {
			try {
				Map<String, Object> filterParamMap = jsonBinder.fromJson(jsonStr, HashMap.class);
				for (Map.Entry<String, Object> entry : filterParamMap.entrySet()) {
					String filterName = entry.getKey();
					String value = (String) entry.getValue();
					//如果value值为空,则忽略此filter.
					if (StringUtils.isNotBlank(value)) {
						PropertyFilter filter = new PropertyFilter(filterName, value);
						filterList.add(filter);
					}
				}
			} catch(Exception e) {
				throw new IllegalArgumentException("请求参数" + filterParamName + "格式不正确", e);
			}
		}
		return filterList;
	}
	
	/**
	 * 从HttpRequest中创建PropertyFilter列表，不过滤null、""值
	 * 
	 * @param request
	 * @return
	 */
	public static List<PropertyFilter> buildFromHttpRequestIncludeEmpty(final HttpServletRequest request) {
		return buildFromHttpRequestIncludeEmpty(request,"filter");
	}

	/**
	 * 获取比较值的类型.
	 */
	public Class<?> getPropertyClass() {
		return propertyClass;
	}

	/**
	 * 获取比较方式.
	 */
	public MatchType getMatchType() {
		return matchType;
	}

	/**
	 * 获取比较值.
	 */
	public Object getMatchValue() {
		return matchValue;
	}

	/**
	 * 获取比较属性名称列表.
	 */
	public String[] getPropertyNames() {
		return propertyNames;
	}

	/**
	 * 获取唯一的比较属性名称.
	 */
	public String getPropertyName() {
		//Assert.isTrue(propertyNames.length == 1, "There are not only one property in this filter.");
		return propertyNames[0];
	}

	
	/**
	 * @return values
	 */
	public List<Object> getValues() {
		return values;
	}

	/**
	 * 是否比较多个属性.
	 */
	public boolean hasMultiProperties() {
		return (propertyNames.length > 1);
	}

	
	/**
	 * @return isInMatchType
	 */
	public Boolean getIsInMatchType() {
		return isInMatchType;
	}

	
	/**
	 * @param isInMatchType
	 */
	public void setIsInMatchType(Boolean isInMatchType) {
		this.isInMatchType = isInMatchType;
	}
	
	/**
	 * 是否存在or过滤条件
	 * 
	 * @return
	 */
	public boolean hasOrProperties() {
		return !orPfList.isEmpty();
	}
	
	/**
	 * 添加or条件
	 * 
	 * @param pf
	 */
	public void addOrPropertyFilter(PropertyFilter pf) {
		orPfList.add(pf);
	}
	
	/**
	 * 返回or条件属性
	 * 
	 * @return
	 */
	public List<PropertyFilter> getOrPropertyFilters() {
		return orPfList;
	}
	
}
