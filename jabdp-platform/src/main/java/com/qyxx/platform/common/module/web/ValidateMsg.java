/**
 * 
 */
package com.qyxx.platform.common.module.web;

/**
 * 
 * 后台校验消息
 * @author bobgao
 *
 */
public class ValidateMsg {
	
	private Boolean valid = true;
	
	private String message;

	private String fieldKey;


	/**
	 * @return the valid
	 */
	public Boolean getValid() {
		return valid;
	}

	/**
	 * @param valid the valid to set
	 */
	public void setValid(Boolean valid) {
		this.valid = valid;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}




	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	public String getFieldKey() {
		return fieldKey;
	}

	public void setFieldKey(String fieldKey) {
		this.fieldKey = fieldKey;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
