package com.qyxx.platform.gsmng.common.service;

import java.util.List;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.impl.pvm.delegate.ActivityExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.qyxx.platform.common.orm.PropertyFilter;
import com.qyxx.platform.common.utils.web.struts2.Struts2Utils;
import com.qyxx.platform.gsmng.common.dao.ProcessInfoDao;
import com.qyxx.platform.gsmng.common.entity.ProcessInfo;


/**
 *会签任务处理类
 */
@Service
@Transactional
public class SignComplete {
	
	private ProcessInfoDao processInfoDao;
	
	private RuntimeService runtimeService;
	
	@Autowired
	public void setRuntimeService(RuntimeService runtimeService) {
		this.runtimeService = runtimeService;
	}


	@Autowired
	public void setProcessInfoDao(ProcessInfoDao processInfoDao) {
		this.processInfoDao = processInfoDao;
	}
	
	
	public SignComplete() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	//有一票同意就通过
	public boolean isComplete(ActivityExecution execution) {
		String nodeId=execution.getActivity().getId();  
	    String pId=execution.getProcessInstanceId();  
	    Object times= runtimeService.getVariable(pId, "countVao");
		Long countVal = (Long)times;
		
	    //完成次数  
	    Integer completeCounter=(Integer)execution.getVariable("nrOfCompletedInstances");  
	    //总数  
	    Integer instanceOfNumbers=(Integer)execution.getVariable("nrOfInstances"); 
	    
	    //Integer agreeVotesCounts=0;
	    //没有会签实例  
	    // completeCounter<instanceOfNumbers
	    if(instanceOfNumbers==0){  
	        return false;  
	    }else if (completeCounter>=1 && completeCounter<=instanceOfNumbers){
	    	 //获取同意票数  
	    	List<PropertyFilter> filters = PropertyFilter
			.buildFromHttpRequest(Struts2Utils.getRequest());
			PropertyFilter propertyFilterParentOrg = new PropertyFilter(
				"EQS_processInstanceId",pId);
			
			PropertyFilter propertyFilterParentOrg2 = new PropertyFilter(
					"EQS_activitiId",nodeId);
			

			PropertyFilter propertyFilterParentOrg3 = new PropertyFilter(
					"EQL_countVal",countVal.toString());
			
				filters.add(propertyFilterParentOrg);
				filters.add(propertyFilterParentOrg2);
				filters.add(propertyFilterParentOrg3);
				
				List<ProcessInfo> processInfoList = processInfoDao.find(filters);
				for(int i = 0;i<processInfoList.size();i++){
					String isApprove =processInfoList.get(i).getIsApprove();
					if(isApprove.equals("1")){
					runtimeService. setVariable(pId, "CandidateUser", null);
						return true;  
					}
				}
	    }
	   
		return false;  
		
	}
	
	
	//部分或者全票通过
	public boolean isComplete(ActivityExecution execution,double percent) {
		
		String nodeId=execution.getActivity().getId();  
	    String pId=execution.getProcessInstanceId();  
	    Object times= runtimeService.getVariable(pId, "countVao");
		Long countVal = (Long)times;
	    //完成次数  
	    Integer completeCounter=(Integer)execution.getVariable("nrOfCompletedInstances");  
	    //总数  
	    Integer instanceOfNumbers=(Integer)execution.getVariable("nrOfInstances"); 
	    double agreeVotesCounts=0;
	    double allCounter = instanceOfNumbers;
	    
	    //没有会签实例  或者 
	    if(instanceOfNumbers==0){  
	        return false;  
	    }else if(completeCounter>=1 && completeCounter<=instanceOfNumbers){
	    	
	    	 //投同意票数  
	    	List<PropertyFilter> filters = PropertyFilter
			.buildFromHttpRequest(Struts2Utils.getRequest());
			PropertyFilter propertyFilterParentOrg = new PropertyFilter(
				"EQS_processInstanceId",pId);
			
			PropertyFilter propertyFilterParentOrg2 = new PropertyFilter(
					"EQS_activitiId",nodeId);
			
			PropertyFilter propertyFilterParentOrg3 = new PropertyFilter(
					"EQL_countVal",countVal.toString());
			
				filters.add(propertyFilterParentOrg);
				filters.add(propertyFilterParentOrg2);
				filters.add(propertyFilterParentOrg3);
				List<ProcessInfo> processInfoList = processInfoDao.find(filters);
				for(int i = 0;i<processInfoList.size();i++){
					String isApprove = processInfoList.get(i).getIsApprove();
					if("1".equals(isApprove)){
						agreeVotesCounts++;
					}		
				}
				
				double completePercent = agreeVotesCounts/allCounter ;
				if(completePercent>=percent){
					runtimeService. setVariable(pId, "CandidateUser", null);
					return true;
				}else{
					return false;  
				}
	    }
		return false;  
		
	}
}
