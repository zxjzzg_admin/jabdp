/*
 * @(#)SqlDataCache.java
 * 2018-7-5 上午11:35:59
 * 
 * Copyright (c) 2018 QiYunXinXi - All Rights Reserved.
 * ====================================================================
 * The QiYunXinXi License, Version 1.0
 *
 * This software is the confidential and proprietary information of QiYunXinXi.
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into with QiYunXinXi.
 */
package com.qyxx.platform.gsmng.common.cache;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.qyxx.platform.common.utils.spring.SpringContextHolder;
import com.qyxx.platform.gsmng.common.service.GsMngManager;
import com.qyxx.platform.sysmng.accountmng.entity.User;



/**
 *  SQL数据缓存
 *  @author gxj
 *  @version 1.0
 *  @since 1.6 2011-9-28 2018-7-5 上午11:35:59
 */

public final class SqlDataCache {
	
	private static SqlDataCache tec;
	
	private ConcurrentMap<String, Map<String, Object>> dataMap;//数据缓存
	
	private static Lock lock = new ReentrantLock(); 
	
	private static Logger logger = LoggerFactory.getLogger(SqlDataCache.class);
	
	private GsMngManager gmm = SpringContextHolder.getBean("gsMngManager");
	
	private SqlDataCache() {
		dataMap = new ConcurrentHashMap<String, Map<String, Object>>();
		User user = new User();
		gmm.setUser(user);//默认超级管理员所属数据用于缓存
	}
	
	public static synchronized SqlDataCache getInstance() {
		if(null==tec) {
			tec = new SqlDataCache();
		}
		return tec;
	}
	
	/**
	 * 加入数据缓存
	 * 
	 * @param key
	 * @param data
	 */
	public void put(String key, Map<String, Object> data) {
		this.dataMap.put(key, data);
	}
	
	/**
	 * 根据key获取数据缓存
	 * 
	 * @param sqlKey
	 * @param isExport
	 * @return
	 */
	public Map<String, Object> get(String sqlKey, Boolean isExport) {
		String key = sqlKey + "." + isExport;
		Map<String, Object> result = this.dataMap.get(key);
		logger.info("使用缓存：" + key + "," + (result!=null));
		if(result == null) {
			lock.lock();
			try {
				result = this.dataMap.get(key);
				if(result == null) {
					result = gmm.findDbDataByDsSqlKey(sqlKey, isExport);
					this.dataMap.put(key, result);
				}
			} finally {
				lock.unlock();
			}
		}
		return result;
	}
	
	/**
	 * 更新缓存
	 * 
	 * @param sqlKey
	 * @param isExport
	 * @return
	 */
	public Map<String, Object> update(String sqlKey, Boolean isExport) {
		Map<String, Object> result = null;
		String key = sqlKey + "." + isExport;
		lock.lock();
		try {
			result = gmm.findDbDataByDsSqlKey(sqlKey, isExport);
			this.dataMap.put(key, result);
			logger.info("更新缓存：" + key + "," + (result!=null));
		} finally {
			lock.unlock();
		}
		return result;
	}
	
	/**
	 * 更新缓存
	 * 
	 * @param key
	 */
	public void update(String key) {
		this.update(key, true);
		this.update(key, false);
	}
	
	/**
	 * 根据key移除数据
	 * 
	 * @param key
	 */
	public void remove(String key) {
		this.remove(key, true);
		this.remove(key, false);
	}
	
	/**
	 * 根据key移除缓存数据
	 * 
	 * @param key
	 */
	public void remove(String sqlKey, Boolean isExport) {
		String key = sqlKey + "." + isExport;
		this.dataMap.remove(key);
		logger.info("移除缓存：" + key);
	}
	

	public void destroy() {
		this.dataMap.clear();
	}

}