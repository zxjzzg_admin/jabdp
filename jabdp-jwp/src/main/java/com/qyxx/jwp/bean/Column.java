/*
 * @(#)Column.java
 * 2012-11-15 下午08:47:57
 * 
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.jwp.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;


/**
 *  字段映射
 *  @author gxj
 *  @version 1.0 2012-11-15 下午08:47:57
 *  @since jdk1.6 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Column {
	
	@XmlAttribute
	private String source;
	
	@XmlAttribute
	private String dest;
	
	@XmlAttribute
	private String rule;

	
	/**
	 * @return source
	 */
	public String getSource() {
		return source;
	}

	
	/**
	 * @param source
	 */
	public void setSource(String source) {
		this.source = source;
	}

	
	/**
	 * @return dest
	 */
	public String getDest() {
		return dest;
	}

	
	/**
	 * @param dest
	 */
	public void setDest(String dest) {
		this.dest = dest;
	}

	
	/**
	 * @return rule
	 */
	public String getRule() {
		return rule;
	}

	
	/**
	 * @param rule
	 */
	public void setRule(String rule) {
		this.rule = rule;
	}
	
	

}
