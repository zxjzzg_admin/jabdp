/*
 * @(#)OwnerProperties.java
 * 2012-5-8 下午04:23:44
 * 
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.jwp.bean;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 *  作者及修改者信息节点
 *  
 *  @author gxj
 *  @version 1.0 2012-5-8 下午04:23:44
 *  @since jdk1.6
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class OwnerProperties {

	@XmlAttribute
	private String creator;
	
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@XmlAttribute
	@XmlJavaTypeAdapter(DateAdapter.class)
	private Date createTime;
	
	@XmlAttribute
	private String lastModifiedBy;
	
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@XmlAttribute
	@XmlJavaTypeAdapter(DateAdapter.class)
	private Date lastModifiedTime;
	
	/**
	 * @return creator
	 */
	public String getCreator() {
		return creator;
	}
	
	/**
	 * @param creator
	 */
	public void setCreator(String creator) {
		this.creator = creator;
	}
	
	/**
	 * @return createTime
	 */
	public Date getCreateTime() {
		return createTime;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	/**
	 * @return lastModifiedBy
	 */
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}
	
	/**
	 * @param lastModifiedBy
	 */
	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
	
	/**
	 * @return lastModifiedTime
	 */
	public Date getLastModifiedTime() {
		return lastModifiedTime;
	}
	
	/**
	 * @param lastModifiedTime
	 */
	public void setLastModifiedTime(Date lastModifiedTime) {
		this.lastModifiedTime = lastModifiedTime;
	}

}
