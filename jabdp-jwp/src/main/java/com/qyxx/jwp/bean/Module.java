/*
 * @(#)Module.java
 * 2012-5-8 下午04:05:11
 * 
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.jwp.bean;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 *  模块节点
 *  
 *  @author gxj
 *  @version 1.0 2012-5-8 下午04:05:11
 *  @since jdk1.6
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="module")
public class Module {
	
	/**
	 * 模块属性
	 */
	@XmlElement
	private ModuleProperties moduleProperties = new ModuleProperties();
	/**
	 * 数据源
	 */
	@XmlElement
	private DataSource dataSource = new DataSource();
	
	@JsonIgnoreProperties
	@XmlTransient
	private String entityName;
	
	@XmlAttribute
	private Integer maxIndex;
	
	@XmlElementWrapper(name = "flows")
	@XmlElement(name="flow")
	private List<Flow> flowList = new ArrayList<Flow>();
	
	@XmlElementWrapper(name = "rules")
	@XmlElement(name="rule")
	private List<Rule> ruleList = new ArrayList<Rule>();
	
	/**
	 * @return moduleProperties
	 */
	public ModuleProperties getModuleProperties() {
		return moduleProperties;
	}
	
	/**
	 * @param moduleProperties
	 */
	public void setModuleProperties(ModuleProperties moduleProperties) {
		this.moduleProperties = moduleProperties;
	}
	
	/**
	 * @return dataSource
	 */
	public DataSource getDataSource() {
		return dataSource;
	}
	
	/**
	 * @param dataSource
	 */
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	
	/**
	 * @return entityName
	 */
	public String getEntityName() {
		return entityName;
	}

	
	/**
	 * @param entityName
	 */
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	/**
	 * @return the maxIndex
	 */
	public Integer getMaxIndex() {
		return maxIndex;
	}

	/**
	 * @param maxIndex the maxIndex to set
	 */
	public void setMaxIndex(Integer maxIndex) {
		this.maxIndex = maxIndex;
	}

	
	/**
	 * @return flowList
	 */
	public List<Flow> getFlowList() {
		return flowList;
	}

	
	/**
	 * @param flowList
	 */
	public void setFlowList(List<Flow> flowList) {
		this.flowList = flowList;
	}

	
	/**
	 * @return ruleList
	 */
	public List<Rule> getRuleList() {
		return ruleList;
	}

	
	/**
	 * @param ruleList
	 */
	public void setRuleList(List<Rule> ruleList) {
		this.ruleList = ruleList;
	}

}
