/*
 * @(#)EditPage.java
 * 2012-11-6 上午11:15:23
 * 
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.jwp.bean;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;


/**
 *  编辑页面
 *  @author gxj
 *  @version 1.0 2012-11-6 上午11:15:23
 *  @since jdk1.6 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class EditPage {
	
	@XmlElementWrapper(name = "buttons")
	@XmlElement(name="button")
	private List<Button> buttonList = new ArrayList<Button>();
	
	@XmlElementWrapper(name = "events")
	@XmlElement(name="event")
	private List<Event> eventList = new ArrayList<Event>();

	@XmlElementWrapper(name = "statistics")
	@XmlElement(name="statistic")
	private List<Item> stsList = new ArrayList<Item>();
	
	
	/**
	 * @return buttonList
	 */
	public List<Button> getButtonList() {
		return buttonList;
	}

	
	/**
	 * @param buttonList
	 */
	public void setButtonList(List<Button> buttonList) {
		this.buttonList = buttonList;
	}


	
	/**
	 * @return eventList
	 */
	public List<Event> getEventList() {
		return eventList;
	}


	
	/**
	 * @param eventList
	 */
	public void setEventList(List<Event> eventList) {
		this.eventList = eventList;
	}


	/**
	 * @return stsList
	 */
	public List<Item> getStsList() {
		return stsList;
	}


	/**
	 * @param stsList
	 */
	public void setStsList(List<Item> stsList) {
		this.stsList = stsList;
	}
	

}
