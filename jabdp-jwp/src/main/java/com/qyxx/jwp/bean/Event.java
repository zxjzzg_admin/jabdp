/*
 * @(#)Event.java
 * 2012-11-5 下午04:26:16
 * 
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.jwp.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;


/**
 *  自定义事件
 *  @author gxj
 *  @version 1.0 2012-11-5 下午04:26:16
 *  @since jdk1.6 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Event {
	
	@XmlAttribute
	private String key;
	
	@XmlAttribute
	private String caption;
	
	@XmlAttribute
	private String params;
	
	@XmlElement
	private String content;

	
	/**
	 * @return key
	 */
	public String getKey() {
		return key;
	}

	
	/**
	 * @param key
	 */
	public void setKey(String key) {
		this.key = key;
	}

	
	/**
	 * @return caption
	 */
	public String getCaption() {
		return caption;
	}


	/**
	 * @param caption
	 */
	public void setCaption(String caption) {
		this.caption = caption;
	}
	

	/**
	 * @return params
	 */
	public String getParams() {
		return params;
	}


	
	/**
	 * @param params
	 */
	public void setParams(String params) {
		this.params = params;
	}


	/**
	 * @return content
	 */
	public String getContent() {
		return content;
	}

	
	/**
	 * @param content
	 */
	public void setContent(String content) {
		this.content = content;
	}

}
