/*
 * @(#)DataProperties.java
 * 2012-6-25 下午06:04:50
 * 
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.qyxx.jwp.menu;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *  树形三级子节点
 *  
 *  @author thy
 *  @version 1.0 2012-6-25 下午04:05:11
 *  @since jdk1.6
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class Module {
	
	/**
	 * 普通模块
	 */
	public static final String TYPE_MODULE = "module";
	
	/**
	 * 普通模块集合
	 */
	public static final String TYPE_MODULES = "modules";
	
	/**
	 * 业务字典
	 */
	public static final String TYPE_DICT = "dict";
	
	/**
	 * 自定义表单
	 */
	public static final String TYPE_FORM = "form";
	
	/**
	 * 移动端模块集合
	 */
	public static final String TYPE_APPS = "apps";
	
	/**
	 * app模块
	 */
	public static final String TYPE_APP = "app";
	
	@XmlAttribute
	private String id;
	
	@XmlAttribute
	private String type;
	
	@XmlAttribute
	private String name;
	
	@XmlAttribute
	private String key;
	
	@XmlAttribute
	private String iconSkin;
	
	@XmlTransient
	private String entityName;
	
	/**
	 * 增加模块集合节点，支持无限级树形菜单
	 */
	@JsonProperty("children")
	@XmlElement
	private List<Module> module = new ArrayList<Module>();

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @return the iconSkin
	 */
	public String getIconSkin() {
		return iconSkin;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param key the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @param iconSkin the iconSkin to set
	 */
	public void setIconSkin(String iconSkin) {
		this.iconSkin = iconSkin;
	}
	
	/**
	 * @return entityName
	 */
	public String getEntityName() {
		return entityName;
	}

	
	/**
	 * @param entityName
	 */
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	
	/**
	 * @return module
	 */
	public List<Module> getModule() {
		return module;
	}

	
	/**
	 * @param module
	 */
	public void setModule(List<Module> module) {
		this.module = module;
	}

}
