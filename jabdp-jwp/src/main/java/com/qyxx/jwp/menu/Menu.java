/*
 * @(#)DataProperties.java
 * 2012-6-25 下午06:04:50
 * 
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.qyxx.jwp.menu;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *  树形根节点
 *  
 *  @author thy
 *  @version 1.0 2012-6-25 下午04:05:11
 *  @since jdk1.6
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="menu")
public class Menu {
	
	/**
	 * 菜单
	 */
	public static final String TYPE_MENU = "menu";
	
	@XmlAttribute
	private String id;
	
	@XmlAttribute
	private String type;
	
	@XmlAttribute
	private String name;
	
	@XmlAttribute
	private String key;
	
	@XmlAttribute
	private String iconSkin;
	
	@JsonProperty("children")
	@XmlElement
	private List<Business> business = new ArrayList<Business>();

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @param key the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @return the iconSkin
	 */
	public String getIconSkin() {
		return iconSkin;
	}

	/**
	 * @param iconSkin the iconSkin to set
	 */
	public void setIconSkin(String iconSkin) {
		this.iconSkin = iconSkin;
	}

	/**
	 * @return the business
	 */
	public List<Business> getBusiness() {
		return business;
	}

	/**
	 * @param business the business to set
	 */
	public void setBusiness(List<Business> business) {
		this.business = business;
	}
	
	
}
