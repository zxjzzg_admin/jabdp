<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>iDesigner</title>
	<%@ include file="/common/meta.jsp" %>
	
	<script type="text/javascript" src="${ctx}/js/editor/ckeditor.js"></script>
	<script type="text/javascript" src="${ctx}/js/editorboost/jquery.webkitresize.min.js"></script>
	<script type="text/javascript" src="${ctx}/js/editorboost/jquery.wysiwyg-resize.min.js"></script>
	<script type="text/javascript" src="${ctx}/js/editor/plugins/tabledrag/redips-drag-source.js"></script>
	<link rel="stylesheet" type="text/css" href="${ctx}/js/easyui/themes/blue/easyui-module.css" colorTitle="blue"/>
	<script type="text/javascript" src="${ctx}/js/ueditor/ueditor.config.js"></script>
	<script type="text/javascript" src="${ctx}/js/ueditor/editor_api.js"></script>
	<script type="text/javascript" src="${ctx}/js/ueditor/lang/zh-cn/zh-cn.js"></script>
	<script type="text/javascript">
   	var titleObj = {};
	var titToCols = {};
	var autoIndex = 1;
	var nullForm = [];
	var nullFieldRows;
	var nullFieldCols;
	var mainTableIndex;
	//var newFormIndex;
	var nullFormMap = {};
	//var maxFieldIndex = parent.$.fn.module.getModule().maxIndex;
	/* console.log(parent.$);
		var module = parent.$.fn.module.getModule();
		console.log(module);
	console.log("parent:");
	console.log(parent); */
	$(function() {
		CKEDITOR.on('instanceReady', function(evt) {
			if($.browser.webkit) {
				$(".cke_editor iframe")
				    /*.wysiwygResize({
				        selector: "img",
				        afterResize: function(img){
							var imgWidth = $(img).width();
							var imgHeight = $(img).height();
							var tableIndex = $(img).parent().attr("tabIndex");
							var fieldIndex = $(img).parent().attr("id");
							parent.$.fn.module.setEditProp(tableIndex,fieldIndex,{"width":imgWidth,"height":imgHeight});
				        }
				    });*/
				.webkitimageresize({afterResize: function(img){
					var imgWidth = $(img).width();
					var imgHeight = $(img).height();
					var tableIndex = $(img).parent().attr("tabIndex");
					var fieldIndex = $(img).parent().attr("id");
					parent.$.fn.module.setEditProp(tableIndex,fieldIndex,{"width":imgWidth,"height":imgHeight});
					parent.showLineInfo(tableIndex,fieldIndex);
				}}); 
			}else {
				var name = evt.editor.name;
				$("#"+ name).next().find("iframe").wysiwygResize({
					selector: "img,label",
					afterResize: function(img){
						var imgWidth = $(img).width();
						var imgHeight = $(img).height();
						var tableIndex = $(img).parent().attr("tabIndex");
						var fieldIndex = $(img).parent().attr("id");
						parent.$.fn.module.setEditProp(tableIndex,fieldIndex,{"width":imgWidth,"height":imgHeight});
						parent.showLineInfo(tableIndex,fieldIndex);
					}
				});
			}
		});
		var module = parent.$.fn.module.getModule();
		/* console.log("module:");
		console.log(module); */
		var formLength = module.dataSource.formList.length;
		var tabsList = module.dataSource.tabsList;
		
		for(var i=0; i<tabsList.length; i++) {
			var tabList = tabsList[i].tabList;
			var rows = tabsList[i].rows;
			var tabsId = "edit_tabs_"+rows;
			$("#edit_page_tabs").append("<div><div id='"+tabsId+"'  border='false' ></div>");
			
			//初始化ckeditor
			initCkeditor(tabsId);
			
			//根据tabs分组添加对应tab页
			for(var j=0; j<tabList.length; j++) {
				//nullFieldRows = 0;
				//nullFieldCols = 0;
				var tab = tabList[j];
				var form = parent.$.fn.module.getFormByKey(tab.form);
				var cols = form.cols;
				var rows = form.fieldList.length;
				titleObj[tab.caption] = tabsId;
				//titToCols[tab.caption] = tabList[j].cols;
				if(form){
					if(form.isMaster || form.listType == "Form"){//主表显示或Form表单显示
					    if(form.isMaster) mainTableIndex = form.index;
						var editors = "editor"+tab.rows+"_"+tab.cols;
						titToCols[tab.caption] = editors;
						var tableCols = tab.tableCols;
						if(!tableCols) {
							tableCols = cols;
							tab.tableCols = cols;
						}
						//nullFieldRows = tabList[j].rows;
						//nullFieldCols = tabList[j].cols;
						if(tab.layout=="table" || !tab.layout){
							createNullFormOrTabField(form);
							var tabStr = autoCreateTabs(rows,tableCols,form,tab,editors);
							content = "<textarea id='"+editors+"' name='"+editors+"' >"+tabStr+"</textarea>";
						}else if(tab.layout=="absolute") {
							var divHt = tab.height;
							if(!divHt){
								divHt = 200;
							}
							content="<div id='"+editors+"' style ='height:"+divHt+"px;'><iframe src='${ctx}/jsp/absolute.jsp?tRows="
									+tab.rows+"&tCols="+tab.cols+"&editors="+editors
									+"' frameborder='0' style='width:100%;height:100%'></iframe></div>";
							createNullFormOrTabField(form, tab);
						 }else if(tab.layout=="excel") {
							content = "<div class='excelForm' id='" + editors + "' style='"
								+ "padding-top:" + tab.content.paddingTop
								+ "px;padding-bottom:" + tab.content.paddingBottom
								+ "px;padding-left:" + tab.content.paddingLeft
								+ "px;padding-right:" + tab.content.paddingRight
								+ "px' excel='true'>" + tab.content.html + "</div>";
							createNullFormOrTabField(form, tab);	
						 }
						 $("#"+tabsId).tabs("add",{
							"title":tab.caption,
							"content":content,
							"cols":tab.cols,
							"rows":tab.rows,
							//打开编辑器界面按钮
							tools:[{
								iconCls:'icon-mini-edit',
								handler:function(){
									var title = $(this).parent().prev().find("span.tabs-title").text();
									var tabId = $(this).parents("div.tabs-container").attr("id");
									var pp = $('#' + tabId).tabs('getTab', title);
									var ppopt = pp.panel("options");
									if(parent.$.fn.module.getTabList(ppopt.rows,ppopt.cols).layout=='excel') {
										window.open("ueditor.jsp?rows=" + ppopt.rows + "&cols=" + ppopt.cols);
									}
									else {
										alert("目前编辑器只对表格布局有效");
									}
								}
							}]
						});
					}
					else{
						if(form.visible){
							var dgId = "tab_dg_"+tab.rows+"_"+tab.cols;
							titToCols[tab.caption] = dgId;			
							$("#"+tabsId).tabs("add",{
								"title":tab.caption,
								"content":"<div name='" + dgId + "'><table id='" + dgId + "'></table></div>"
							});
							var tableCols = tab.tableCols;
							if(!tableCols){
								tableCols = cols;
								tab.tableCols = cols;
							}
							autoCreateTabs(rows,tableCols,form,tab,dgId);
					 	}
					}
				}
			}
		} 
		
		for(var newFormIndex in nullFormMap) {
			parent.$.fn.module.getFormProp(newFormIndex).fieldList = nullFormMap[newFormIndex];
		}
		//添加启动流程按钮
		var data = {"caption":'启动流程',"key":'bt_process',"iconCls":"icon-flowStart"};
		parent.$.fn.module.setButtonProp(mainTableIndex,'bt_process','editPage',data);
	  	//先在buttonList中添加“报表按钮”	
		var data = {"caption":'报表',"key":'bt_report',"iconCls":"icon-report"};
		parent.$.fn.module.setButtonProp(mainTableIndex,'bt_report','editPage',data);
		//初始化时添加按钮
		var buttonList = parent.$.fn.module.getEditPage(mainTableIndex).buttonList;
		for(var i=0;i<buttonList.length;i++){
			var key = buttonList[i].key;
			var caption = buttonList[i].caption;
			var iconCls = buttonList[i].iconCls || "icon-flowStart";
			var buttonStr = "<a href='#' class='easyui-linkbutton addBt' plain='true' id='"+key+"' name='editPage'  onclick='parent.showButtonProp(mainTableIndex,this.id,this.name)' title='"+caption+"' iconCls='" + iconCls + "'>"+caption+"</a>";
			$("#editbuttons").append(buttonStr);
		   	$('#'+key).linkbutton({});
		}
		$("a.addBt").live('contextmenu',function(e){
			e.preventDefault();
			$("#delete_menu").data("obj",this);
    		$("#delete_menu").menu('show', {						
				left: e.pageX,
				top: e.pageY
			});
		});
		$("div.excelForm img").live("click",function() {
			var formindex = parseInt($(this).attr("formindex"));
			var fieldindex = parseInt($(this).attr("fieldindex"));
			parent.$.fn.module.showLineInfo(formindex, fieldindex);//form.index;field.index
		});
	});
   	//修改titleObj对象内的值
  	/* function revampTitlteObj(oldTitle,newTitle){
	   var titValue = titleObj[oldTitle];
	   delete titleObj[oldTitle];
	   titleObj[newTitle] = titValue;
   	}*/
   	//创建NullForm或者添加非table布局字段至NullForm
   	function createNullFormOrTabField(form,tab) {
   		var newFormIndex = form.index;
		if(!nullFormMap[newFormIndex]) {
			nullFormMap[newFormIndex] = [];
		}
		if(tab) {
			var data = form.fieldList;
 			for(var m=0;m<data.length;m++){
				//过滤表头字段
				var field = data[m];
				if(!form.isMaster || (field.tabRows==tab.rows && field.tabCols==tab.cols)){
					if(field.key) copyFiledToNewForm(field,form.index);
				}
 			}
		}
   	}
   	
   	//初始化ckeditor
   	function initCkeditor(tabsId){
		$("#"+tabsId).tabs({
			tools:[{
				"title":"新增tab",
				iconCls:'icon-add',  
				handler:function(){  
					createTabs(this);
				}
			},{
				title:'删除tabs分组',
			    iconCls:'icon-remove',  
				handler:function(){  
					deleteTabsGroup(this); 
				}
		    }],
		    //添加右键菜单
			onContextMenu:function(e, title){
				var tablesId = titleObj[title];
				var tab = $("#"+tablesId).tabs("getTab",title);
				$("#"+tablesId).tabs("select",title);
				$("#edit_page_tabs").data("title",title);
				//var editorId = tab.find("textarea")[0].name;
				var editors = tab.find("textarea")[0];
				if(editors) {
					var editorId = editors.name;
			  		//if(editorId) {
			  		//$("#edit_page_tabs").data("title",title);
					e.preventDefault();
					$("#del_tabs").menu('show', {						
						left: e.pageX,
						top: e.pageY
					});
			  	}else{
			  		//$("#edit_page_tabs").data("title",title);
					e.preventDefault();
			  		$("#grid_menu").menu('show', {						
						left: e.pageX,
						top: e.pageY
					});
				}
			},
			//选中
			onSelect:function(title){
				var tablesId = titleObj[title];
				var editOrtabId = titToCols[title];
				var tableRows =tablesId.replace(/[^0-9]/ig, "");
				// var tableArr = tablesId.split("_");
				// var tableRows = tableArr[tableArr.length-1];
				var tab = $("#"+tablesId).tabs("getTab",title);
				var colsArr = editOrtabId.split("_");
				var cols = colsArr[colsArr.length-1];
				
				//----------
				/* console.log('tableRows:'+tableRows);
				console.log('cols:'+cols);
				console.log('editOrtabId:'+editOrtabId);
				console.log('tab'+tablesId+':');
				console.log(tab);
				console.log('$("#"+tablesId):');
				console.log($("#"+tablesId)); */
				
				if(tab.find("textarea")[0]){
					var editor = CKEDITOR.instances[editOrtabId];
					/* console.log(editor); */
					//若没有初始化过ueditor
					if(!editor) {
						//UE.getEditor(editOrtabId);
						
						CKEDITOR.replace(editOrtabId, {
							extraPlugins: 'tabledrag',
							width: 800
						});
					}else {
						initTableDrag("drag", editor.window.$);
					} 
					// var colsArr = editOrtabId.split("_");
					// var cols = editOrtabId.charAt(editOrtabId.length-1);
					// var cols = colsArr[colsArr.length-1];
					parent.changeLayoutProp(tableRows,cols);
				}else if(tab.find("iframe")[0]){
					parent.changeLayoutProp(tableRows,cols);
				}else if(tab.find("div[excel]")[0]) {
					parent.changeLayoutProp(tableRows,cols);
				}else{
				 	// var cols =  editOrtabId.charAt(editOrtabId.length-1);
					parent.changeLayoutProp(tableRows,cols,true);
				}  
			}  
		});
	}
		
    //复制主表元素到新的对象中
    function copyFiledToNewForm(data, newFormIndex){
		if(data.index){
			//nullForm.push(data);   
			nullFormMap[newFormIndex].push(data);
		}
    }
    
    //新增占位符到fieldList
	function addFields(fieldIndex,newArry,tableIndex,insertBefore){
	   var startIndex = 0;
	   var nullForm = nullFormMap[tableIndex];
	   var len = nullForm.length-1;
	   for(var i = 0 ;i<=len;i++){
		   if(nullForm[i].index==fieldIndex){
			  startIndex = i;
			  break;
		   }
	   }	
	   if(!insertBefore) {//向后插入
		   startIndex++;
	   }
	   for(var j = newArry.length-1 ;j>=0;j--){
			nullForm.splice(startIndex,0,newArry[j]);
	   }	
	}
    
   //创建新tabs分组
   function createNewTabsGroup(){
	  var tabsList = parent.$.fn.module.getModule().dataSource.tabsList;
	  var rows = 0;
	  /*  for(var i=0;i<tabsList.length;i++){
		  rows = tabsList[i].rows>rows?tabsList[i].rows:rows;
	  } */
	  $.each(tabsList,function(k,v){
		  rows = v.rows>rows?v.rows:rows;
	  });
	  rows =rows+1;
	  var tabsId = "edit_tabs_"+rows;
	  parent.$.fn.module.addTabsList(rows);
	  $("#edit_page_tabs").append("<div><div id='"+tabsId+"'  border='false' ></div></div>");
	  // $("#"+tabsId).tabs({}); 
	  initCkeditor(tabsId);
   }
   
   //删除tabs分组
   function deleteTabsGroup(obj){
	   var titleId = $(obj).closest("div[id]").attr("id");
	   var rows = titleId.replace(/[^0-9]/ig, "");
	   var selectTab = $("#"+titleId).tabs("getSelected");
	   if(selectTab){
		   alert("请先删除所有的表格后再删除！");
	   }else{
		   $.messager.confirm('警告', 'tabs分组一经删除不能恢复，确定删除？', function(r){
				if (r){
					 var flag = parent.$.fn.module.deleteTabsList(rows);
					  if(flag){
						  $("#"+titleId).remove();
					  }
				}
			});
		}
	}
   
   //刷新当前页面
   function doRefreshCurrentPage() {
	   parent.doRefreshTab('编辑页面设计','${ctx}/jsp/edit.jsp', true);
   }
   
   //表格左移
	function moveTabLeft(){
		var title =  $("#edit_page_tabs").data("title");
		var tabsListId = titleObj[title];
		var tabListId = titToCols[title];
	    var rows = tabsListId.replace(/[^0-9]/ig, "");
	    var colsArr = tabListId.split("_");
	    var cols = colsArr[colsArr.length-1];
	    var flag = parent.$.fn.module.moveTabPlace(rows,cols,"left");
	    if(!flag){
		   alert("已经是边界位置不能实现该操作!");
	    }else{
	    	doRefreshCurrentPage();
	    }
	}
   
   //表格右移
   function moveTabRight(){
	   	   var title =  $("#edit_page_tabs").data("title");
		   var tabsListId = titleObj[title];
		   var tabListId = titToCols[title];
		   var rows = tabsListId.replace(/[^0-9]/ig, "");
		   var colsArr = tabListId.split("_");
		   var cols = colsArr[colsArr.length-1];
		  // alert(rows+"  : "+cols);
		   var flag = parent.$.fn.module.moveTabPlace(rows,cols,"right");
		   if(!flag){
			   alert("已经是边界位置不能实现该操作!");
		   }else{
			   doRefreshCurrentPage();
		   }
		  
   }
   //表格上移
   function moveTabUp(){
	   var title =  $("#edit_page_tabs").data("title");
	   var tabsListId = titleObj[title];
	   var tabListId = titToCols[title];
	   var rows = tabsListId.replace(/[^0-9]/ig, "");
	   var colsArr = tabListId.split("_");
	   var cols = colsArr[colsArr.length-1];
	   var flag = parent.$.fn.module.moveTabUpDown(rows,cols,"up");
	   if(!flag){
		   alert("已经是边界位置不能实现该操作!");
	   } else{
		   doRefreshCurrentPage();
	   }
	  
   }
   //表格下移
   function moveTabDown(){
	   var title =  $("#edit_page_tabs").data("title");
	   var tabsListId = titleObj[title];
	   var tabListId = titToCols[title];
	   var rows = tabsListId.replace(/[^0-9]/ig, "");
	   var colsArr = tabListId.split("_");
	   var cols = colsArr[colsArr.length-1];
	   var flag = parent.$.fn.module.moveTabUpDown(rows,cols,"down");
	   if(!flag){
		   alert("已经是边界位置不能实现该操作!");
	   }else{
		   doRefreshCurrentPage();
	   }
   }
   
   /**
    *根据form生成对应的表格
    *@param rows 生成的表格的行数
    *@param cols 生成表格的列数
    *@param form 表格一个表格数据
    *@param tab 单个布局tab属性
    *@param tabId 生成的tab或者datagrid的id
    *@returns null*/
    function autoCreateTabs(rows,cols,form,tab,tabId){
 	   	var str = [];
 		var colCount=1;
 		var rowCount=0;
 		var colsArr =[];
 		for(var i=0;i<rows;i++){
 			colsArr[i] = 0;//每一行占用列数
 		}
 		if(form.isMaster || form.listType == "Form"){ //主表或表单显示类型
	 			var data = form.fieldList;
	 		    var tmpCols = cols;
	 			for(var m=0;m<data.length;m++){
 					//过滤表头字段
 					var field = data[m];
 					if(!form.isMaster || (field.tabRows==tab.rows && field.tabCols==tab.cols)){
 						if(field.isCaption || !field.editProperties.visible) { //表头字段/隐藏字段
 							copyFiledToNewForm(field,form.index);
 						} else {
 							if(colCount == 1) { //首列
 								str.push("<tr>");
 								tmpCols = tmpCols - colsArr[rowCount];
 							}
 							var oldTmpCols = tmpCols;
 							tmpCols = tmpCols - field.editProperties.cols;
 							if(tmpCols < 0) { //换行
 								
 								genNullField(str, oldTmpCols, tab, tabId, form);//占位符
                                
                                rowCount=rowCount+1;
                                tmpCols=cols;
                                tmpCols=tmpCols-field.editProperties.cols-colsArr[rowCount];
                                str.push("<tr>");
 							}
 							if(tmpCols >=0) {
 								var rowspan=field.editProperties.rows;
 								var colspan=field.editProperties.cols;
 								
 								str.push("<td rowspan='"+rowspan+"' colspan='"+ colspan +"'>");
								str.push(covInputType(field,form.index));
								str.push("</td>");
 								
								copyFiledToNewForm(field,form.index);
								
								if(rowspan > 1) {//行数占用多行
 									var rowStart = rowCount+1;
 									var rowEnd = rowCount+rowspan-1;
 									for(var i = rowStart;i<=rowEnd;i++) {
 										colsArr[i] = colsArr[i]+colspan;
 									}
 								}
                                if(tmpCols == 0) {//一行最后列
                                	str.push("</tr>");
                                	rowCount=rowCount+1;
                                    colCount=1;
                                    tmpCols=cols;
                                } else {
                                	colCount++;
                                }
 							}
 						}
 					}
 				}
	 			if(tmpCols != cols) {
					genNullField(str, tmpCols, tab, tabId, form);//占位符
				}
 				//$("#"+tabId).html("<div id='drag'><table id='ck_tab"+tab.cols+"' border='0' cellspacing='0' cellpadding='0' style='width:100%;height:100%;border-left: 1px solid #99BBE8; border-bottom: 1px solid #99BBE8;'>"+str+"</table></div>");
 				return "<div id='drag'><table id='ck_tab"+tab.cols+"' border='0' cellspacing='0' cellpadding='0' style='width:100%;height:100%;'>"+str.join("")+"</table></div>"
 		 }else{
 			 	var field = {};
 				var col=[[]];
 				var dataObj = {};
 				for(var i=0;i<form.fieldList.length;i++){
 					field = form.fieldList[i];
 					if(field.queryProperties.showInGrid) {
 						if(field.editProperties.layer - col.length > 0) {
 							for(var nn = 0; nn < field.editProperties.layer - col.length; nn++) {
 								col.push([]);
 							}
 						}
 						if(field.isCaption) {
 							field.editProperties.layer === null ? field.editProperties.layer = 1 : field.editProperties.layer;
 							col[field.editProperties.layer-1].push({title:field.caption,rowspan:field.editProperties.rowspan,colspan:field.editProperties.colspan});
 						} else {
 							field.editProperties.layer === null ? field.editProperties.layer = 1 : field.editProperties.layer;
 							col[field.editProperties.layer-1].push({field:field.key,width:field.queryProperties.width,title:field.caption,rowspan:field.editProperties.rowspan,colspan:field.editProperties.colspan});
 						}
 						if(!field.isCaption) {
 							var value = "<div onclick='parent.$.fn.module.showLineInfo(this.tabIndex,this.id)' tabIndex="+form.index+" id="+field.index+"><lable>"+field.dataProperties.colName+"</lable></div>"
 							dataObj[field.key] = value;
 						}
 					}
 				}
 				var hei = form.height || 330;
 				$('#'+tabId).edatagrid({
 					//width: 800,
 					//fitColumns : true,
 					nowrap : true,
 					height: hei,
 					columns:col
 				});
 				$('#'+tabId).edatagrid("addRow",dataObj); 
 		 }
    }
   
   /**
          生成占位符 
   */
   function genNullField(str, cols, tab, tabId, form) {
	   for(var i=1;i<=cols;i++) { //生成多个空白占位符
		   str.push("<td colspan='" + (i * 2) + "'>");
		   //生成占位符字段
		   var newField = parent.$.fn.module.getNullField({"tabRows":tab.rows,"tabCols":tab.cols});
		   str.push(covInputType(newField,form.index));//空白占位符
		   str.push("</td>");
	       copyFiledToNewForm(newField,form.index);   
	   }
	   str.push("</tr>");
   }
  
   /**
   */
   function initTableDrag(dragId, win){
	   var	rd = REDIPS.drag;
		// initialization
		rd.init("drag", win);
		// set hover color
		rd.hover.color_td = '#E7C7B2';
		// set drop option to 'shift'
		rd.drop_option = 'switch';
		// set shift mode to vertical2
		rd.shift_option = 'vertical2';
		// enable animation on shifted elements
		rd.animation_shift = true;
		// set animation loop pause
		rd.animation_pause = 20;
   }
 //创建新tab页
	//var arr =[]; 
	//var str;
	//var col_Count;
	//var row_Count; 
   function createTabs(obj){
	   var cols = 0;
	   var tab={};
	  // alert(obj.id);
	 /*  var totleId= $(obj).closest("div[id]").attr("id");
	  var titleId =$("#"+totleId).parents("div[id]").attr("id"); */
	  var titleId = $(obj).closest("div[id]").attr("id");
	  var rows = titleId.replace(/[^0-9]/ig, "");
	// var rowArr = titleId.split("_");
	// var cols = rowArr[]
	  // var tabsList = parent.$.fn.module.getTabsList();
	   var tabList = parent.$.fn.module.getTabsList(rows).tabList;
	   var MasterKey = "";
		 var form = parent.$.fn.module.getModule().dataSource.formList;
	 	$.each(form,function(k,v){
	 		if(v.isMaster){
	 			MasterKey = v.key;
	 		}
	 	});
	   if(tabList){
		   for(var i =0;i<tabList.length;i++){
				   cols = cols>tabList[i].cols?cols:tabList[i].cols;
		   }
	   }
	  
	  	cols =cols+1;
	  	var autoId = rows+""+cols;
		tab = $.extend({},{"rows":rows,"cols":cols,"form":MasterKey,"caption":"补充信息"+autoId,"languageText":""});
		var editId = "editor"+rows+"_"+cols;
		titToCols['补充信息'+autoId] = editId;
		titleObj['补充信息'+autoId] = titleId;
	   parent.$.fn.module.setTabList(tab); 
	   $('#'+titleId).tabs('add',{  
		    title:'补充信息'+autoId,  
		    content:"<textarea id=" + editId + " name=" + editId + " ><div id='drag'><table table id='ck_tab" + cols
		    + "' border='0' cellspacing='0' cellpadding='0' style='width:100%;height:100% ;'></table></div></textarea>",  
		    "cols":cols,
			"rows":rows,
		    tools:[{
				iconCls:'icon-mini-edit',
				handler:function(){
					var title = $(this).parent().prev().find("span.tabs-title").text();
					var tabId = $(this).parents("div.tabs-container").attr("id");
					var pp = $('#' + tabId).tabs('getTab', title);
					var ppopt = pp.panel("options");
					window.open("ueditor.jsp?rows=" + ppopt.rows + "&cols=" + ppopt.cols);
				}
			}]
		    
	   }); 
	 //  autoIndex++;
	   /*var editors = "editor"+cols;
	    CKEDITOR.replace( editors, {
			extraPlugins : 'tableresize'
		});  */
	   
   }
   /**
   	*判断ckeditor内容是否为空来执行删除tabs分页方法
   */
   function delele_tabs(){
	   var title = $("#edit_page_tabs").data("title");
	   var tableId = titleObj[title];
	   var tab = $("#"+tableId).tabs("getTab",title);
	   var editorId = $(tab[0]).find("textarea")[0].name;
	   var isEmpty = true;
	   var editor=CKEDITOR.instances[editorId];
	   if(editor) {
		   var imgs = $(editor.document.$).find("img");
		   var labels = $(editor.document.$).find("label");
		   isEmpty = (imgs.length == 0) && (labels.length == 0);
	   }	
	   var rows = tableId.replace(/[^0-9]/ig, "");
	 // var rowsArr = tableId.split("_");
	 // var rows = rowsArr[rowsArr.length-1];
	   //var cols = editorId.charAt(editorId.length-1);
	   var colsArr = editorId.split("_");
	   var cols = colsArr[colsArr.length-1];
	   if(isEmpty){
		  // alert("功能研发中.......");
		   $("#"+tableId).tabs("close",title);
		   delete titToCols[title];
		   delete titleObj[title];
		   parent.$.fn.module.deleteTabList(rows,cols);
	   }else{
		   $.messager.alert("提示","tab分页中还存在内容请移动到其他tab分页中再删除!",'warning');
	   } 
	   
   }
   /**
   *动态修改表头
   */
   function changeTitle(oldTitle,newTitle){
	   var titValue = titleObj[oldTitle];
	   var gridValue = titToCols[oldTitle];
	   delete titleObj[oldTitle];
	   delete titToCols[oldTitle];
	   titleObj[newTitle] = titValue;
	   titToCols[newTitle] = gridValue;
		var tab = $("#"+titValue).tabs('getTab',oldTitle);
		tab.panel("setTitle", newTitle);
		var spans = $("#"+titValue).find("li span.tabs-title");
		for(var i=0;i<spans.length;i++){
			if($(spans[i]).text()==oldTitle){
				$(spans[i]).text(newTitle);
					break;
				}
			}
	}


   /**
   	*把不同的编辑类型转化为不同的标签
    *@param data tabId  字段  表编号
    *@returns tdstr 单元格内元素*/
   function covInputType(data,tabId){
	   return parent.$.fn.module.coverCellType(data, tabId);
   }
   
	   
	   
	   //编辑界面添加按钮
	   function addbutton(caption,key){
		   var data = {"caption":caption,"key":key};
	  		parent.$.fn.module.setButtonProp(mainTableIndex,key,'editPage',data);
		   var buttonStr = "<a href='#' class='addBt' plain='true' id='"+key+"' name='editPage'  onclick='parent.showButtonProp(mainTableIndex,this.id,this.name)' title='"+caption+"'>"+caption+"</a>";
	   		$("#editbuttons").append(buttonStr);
	   		$('#'+key).linkbutton({});
	   }
	   //编辑界面删除按钮
	     function deletebutton(){
	    	 var buttonObj = $("#delete_menu").data("obj");
	    	 var key = $(buttonObj).attr("id");
	    	 var name = $(buttonObj).attr("title");
	    	 var data = {"caption":name,"key":key};
	    	 parent.$.fn.module.deleteButton(mainTableIndex,key,'editPage');    	 
	    	 $('#'+key).remove();
	    }
	   function addMenu(mainKey,jsonData){
		  if( $("#menu"+mainKey).length != 0){
			  $("#menu"+mainKey).remove();
		  }
			var menu=$("<div id='menu"+mainKey+"' style='width:100px;'></div>");
  			var len = jsonData.length;
  			var div = [];
  				for(var i=0;i<len;i++){
  					div.push('<div id="');
  					div.push(jsonData[i].key);
  					div.push('">');
  					div.push(jsonData[i].caption);
  					div.push('</div>');
  				}
  		menu.append(div.join(""));
  		menu.appendTo("body");
  		$('#'+mainKey).html("");	
		$('#'+mainKey).menubutton({   //('#'+key,win.document.body)查找win的页面中的key元素
			menu: '#menu'+mainKey,
			text :"报表"
		}); 
		  
	   }
	   function changeButtonType(key){	   
		   var bt = parent.$.fn.module.getButtonProp(mainTableIndex,key,'editPage');
		   bt.buttonType="normal";
		   bt.buttonList= [];
		   parent.$.fn.module.setButtonProp(mainTableIndex,key,'editPage',bt);
		   $('#'+key).html("");	
		   $("#menu"+key).remove();
			$('#'+key).linkbutton({ 
				text : bt.caption
			}); 
	   }
function replaceTabs(tabObject,type){//转换布局类型
	var rows = tabObject.rows;
	var cols = tabObject.cols;
	var editors = "editor"+rows+"_"+cols;
	var formKey = tabObject.form;
	var tableCols = tabObject.tableCols;
	var tabsId = "edit_tabs_"+rows;
	var tab = $("#"+tabsId).tabs("getSelected");
	if(type=="table") {
		var form = parent.$.fn.module.getFormByKey(formKey);
		var rowNum = form.fieldList.length;
		//过滤表头字段行数
		for(var nn = 0; nn < form.fieldList.length; nn++) {
			if(form.fieldList[nn.isCaption]) {
				rowNum--;
			}
		}
		//nullForm = [];
		nullFormMap[form.index] = [];
		var tabStr = autoCreateTabs(rowNum,tableCols,form,tabObject,editors);
		content ="<textarea id='"+editors+"' name='"+editors+"' >"+tabStr+"</textarea>";
		$("#"+editors).replaceWith(content);
		$("#"+editors).hide();
		var editor = CKEDITOR.instances[editors];
		if(!editor) {
			CKEDITOR.replace(editors, {
				extraPlugins : 'tabledrag',
				width: 800
			});
		}else {
			$("#cke_"+editors).show();
			//initTableDrag("drag", editor.window.$);
		} 
	}
	else if(type=="absolute"){
		var divHt =  tabObject.height;
		if(!divHt){
			divHt = 300;
		}
		content = "<div id='"+editors+"' style ='height:"+divHt+"px;'><iframe  src='${ctx}/jsp/absolute.jsp?tRows="+rows+"&tCols="+cols
				+"&editors="+editors+"' frameborder='0' style='width:100%;height:100%'></iframe></div>"
		$("#"+editors).replaceWith(content);
		$("#cke_"+editors).hide();
	}
	else if(type=="excel"){
		var x = parent.$.fn.module.getTabList(rows,cols);
		if(x.content) {
			if(!x.content.html) {
				x.content = {html:""
					,paddingTop:0
					,paddingBottom:0
					,paddingLeft:0
					,paddingRight:0};
			}
		} else {
			x.content = {html:""
			,paddingTop:0
			,paddingBottom:0
			,paddingLeft:0
			,paddingRight:0};
		}
		html = "<div class='excelForm' id='" + editors + "' style='"
		+ "padding-top:" + x.content.paddingTop
		+ "px;padding-bottom:" + x.content.paddingBottom
		+ "px;padding-left:" + x.content.paddingLeft
		+ "px;padding-right:" + x.content.paddingRight
		+ "px' excel='true'>" + x.content.html + "</div>";
		$("#"+editors).replaceWith(html);
		/* $("img").bind('onclick', function() {
		    alert("a");
		}); */
		/* var myimgs = $("img");
		/* console.log(myimgs);
		myimgs.click(function () {
		    alert(a);
		});
		for(var i=0; i<myimgs.length; i++) {
			myimgs[i].onclick = function() {
				
			};
		} */
		
		$("#cke_"+editors).hide();
	}
	/*  $("#"+tabsId).tabs("update",{
			tab: tab,
			options:{
				"content":content
			}
	}); */
}

function changeHeight(tId,height){//修改绝对布局页面的高
	$("#"+tId).css("height",height);
} 
   </script>
</head> 
<body class="easyui-layout">
	<div region="north" style="height:56px;overflow:hidden;" border="false">
		<div id="editbuttons" style="height:56px;padding:2px;background:#E0ECFF;border-bottom:1px solid #8DB2E3;border-top: 1px solid #8DB2E3;">
			<a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-cancel"  title="关闭">关闭</a>
			<a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-first"  title="第一条">第一条</a>
			<a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-prev" title="上一条">上一条</a>
			<a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-next" title="下一条">下一条</a>
		 	<a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-last" title="最后一条">最后一条</a>
		 	<a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-add" title="新增">新增</a>
			<a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-edit" title="修改">修改</a>
		 	<a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-copy" title="复制">复制</a>
		 	<a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-attach" title="附件">附件</a>
		</div>	
	</div>
	<div region="center" id="edit_page_tabs" border="false"> 
			
		<!-- <div>
			<div id="edit_tabs"  border="false"  tools="#tab-tool">
			</div> 
		</div>
		 <div width="100%">
			<div id="childTabInfo"   border="false">
			</div> 
		</div> -->
	</div>
	<!-- <div region="south" split="true" style="height:100px" >  
		<div id="childTabInfo" fit="true" border="false">
		</div> 
	</div> --> 

<div id="delete_menu" class="easyui-menu" style="width:100px;" border="false">
		<div onclick="deletebutton();">删除按钮</div>
</div>
<div id="del_tabs" class="easyui-menu" style="width:110px;" border="false">
		
		<div onclick="moveTabLeft();">左移tab分页</div>
		<div onclick="moveTabRight();">右移tab分页</div>
		<div onclick="moveTabUp();">上移tab分页</div>
		<div onclick="moveTabDown();">下移tab分页</div>
		<div onclick="delele_tabs();">删除tab分页</div>
</div>
<div id="grid_menu" class="easyui-menu" style="width:110px;" border="false">
		<div onclick="moveTabLeft();">左移tab分页</div>
		<div onclick="moveTabRight();">右移tab分页</div>
		<div onclick="moveTabUp();">上移tab分页</div>
		<div onclick="moveTabDown();">下移tab分页</div>
</div>
<div id="tab-tools">
	<a href="/jwds/jsp/ueditor.jsp" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-add'" onClick="javascript:alert('edit')"></a>
</div>
<div id="tab-tool" border="false" >
			<!-- <a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-cancel"  title="关闭">关闭</a>
			<a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-add" title="新增">新增</a>
			<a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-edit" title="修改">修改</a>
		 	<a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-copy" title="复制">复制</a>
			<a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-first"  title="第一条">第一条</a>
			<a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-prev" title="上一条">上一条</a>
			<a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-next" title="下一条">下一条</a>
		 	<a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-last" title="最后一条">最后一条</a>
		 	<a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-attach" title="附件">附件</a>
		 	<a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-report" title="报表">报表</a> -->
		 	<a href="#" onclick="createTabs(this)" class="easyui-linkbutton" plain="true" iconCls="icon-add" title="新增tab"></a>
		 	<!-- <a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-save" onclick="javascript:saveModule();" title="保存"></a> -->
</div>
</body>
</html>