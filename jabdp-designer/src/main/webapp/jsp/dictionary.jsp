<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>iDesigner</title>
	<%@ include file="/common/meta.jsp" %>
	<script type="text/javascript" src="${ctx}/js/editor/ckeditor.js"></script>
	<script type="text/javascript" src="${ctx}/js/editor/plugins/tabledrag/redips-drag-source.js"></script>
	<link href="${ctx}/js/easyui/themes/blue/panel.css" rel="stylesheet" type="text/css"/>
	 <script type="text/javascript" src="${ctx}/js/easyui/easyui.xpPanel.js"></script>	 
<script type="text/javascript">
    $(function(){
    	
    	loadQueryInfo();
    //	 $("#a_exp_clp").toggle(expandAll, collapseAll);
    		//$("#addEditorWin").window('colse');
   }); 
    
  /*   function collapseAll() {
		$(".xpstyle-panel .xpstyle").panel("collapse");
		$(this).removeClass("icon-collapse");
		$(this).addClass("icon-expand");
		$(this).attr("title", "全部展开");
	}

	function expandAll() {
		$(".xpstyle-panel .xpstyle").panel("expand");
		$(this).removeClass("icon-expand");
		$(this).addClass("icon-collapse");
		$(this).attr("title", "全部收缩");
	} */
    
	 function showLineInfo(tableIndex,fieldIndex){
		   //alert(tabIndex+" : "+fieldIndex);
		 //  var data = parent.$.fn.module.getFieldPropJson(tableIndex, fieldIndex);
		 //  $("#module_table_propGrid").propertygrid("loadData",data);
		   parent.showLineInfo(tableIndex,fieldIndex);
	   }
	
   function loadQueryInfo(){
	   var module = parent.$.fn.module.getModule();
	   var formLength = module.dataSource.formList.length;
	   for(var i =0;i<formLength;i++){
			  var form = module.dataSource.formList[i];
			  if(form.isMaster){
				  showQueryView(form);
			  }
	   }
   }
   
   
   
   function initTableDrag(dragId, win){
	   var	rd = REDIPS.drag;
		// initialization
		rd.init("drag", win);
		// set hover color
		rd.hover.color_td = '#E7C7B2';
		// set drop option to 'shift'
		rd.drop_option = 'switch';
		// set shift mode to vertical2
		rd.shift_option = 'vertical2';
		// enable animation on shifted elements
		rd.animation_shift = true;
		// set animation loop pause
		rd.animation_pause = 20;
   }
   
   
   var arr =[]; 
	var str;
	var col_Count;
	var row_Count; 
  	function initEditorWin(){
  		$("#addEditorWin").window('open');
  		var forms = parent.$.fn.module.getModule().dataSource.formList;
  		var cols = "2";
  		str = "";
		col_Count=0;
		row_Count=0;
		
 	   if(forms[0]){
 		   if(forms[0].cols){
 			   cols = forms[0].cols;
 		   }
 		   var filedList  =forms[0].fieldList;
 		   var rows = filedList.length;
	 		  for(var i=0;i<rows;i++){
	 				arr[i] = new Array();
	 					for(var j=0;j<cols;j++){
	 					arr[i][j]=0;
	 					}
	 				}
 		   for(var i=0;i<rows;i++){
 			  var data=parent.$.fn.module.autoCreateTable(filedList[i],cols,forms[0].index,arr,row_Count,col_Count,str);
 			  str = data["str"];
 			  col_Count = data["col_Count"];
 			  row_Count = data["row_Count"];
 			//  arr = data["arr"];
 			 	if(i == rows-1){
					var datajson = {"caption":"","key":"","languageText":"","isVirtual":null,"index":"","tabRows":1,"tabCols":1,"dataProperties":{"dataType":"dtString","size":50,"scale":0,"key":"CeShi","colName":"CeShi","notNull":false,"unique":false},"editProperties":{"editType":"","multiple":true,"visible":true,"readOnly":false,"charCase":"normal","defaultValue":"","isPassword":false,"align":"left","rows":1,"cols":1,"maxSize":2000,"dtFormat":"","formula":null,"width":null,"height":null,"top":null,"left":null},"queryProperties":""};
					var num =cols-col_Count;
					//alert(num);
					if(num!=cols){
						for(var n=0;n<num;n++){
							//alert(n);
							 var data = parent.$.fn.module.autoCreateTable(datajson,cols,forms[0].index,arr,row_Count,col_Count,str);
							str = data["str"];
				 			col_Count = data["col_Count"];
				 			  row_Count = data["row_Count"];
				 			  arr = data["arr"];
				 			// alert(str);
						}
					}
					
				}
 			
 			// alert("页面："+row_Count+":"+col_Count);
 			//alert(str);
 		   }
 	   }
 	    
 		$("#useEditor").val("<div id='drag'><table style='width:100%;height:100%;border-left: 1px solid #99BBE8; border-bottom: 1px solid #99BBE8;'>"+str+"</table></div>");
		
			
 		var editor = CKEDITOR.instances["useEditor"];
 		if(!editor){
 			CKEDITOR.replace( "useEditor", {
 				extraPlugins : 'tabledrag',
 				width: 550
 			});
 		}else{
 			initTableDrag("drag", editor.window.$);
 		}
  		
  	}
  	
  	
   function showQueryView(form){
	   var fieldLength = form.fieldList.length;
	   var module = parent.$.fn.module.getModuleProp().ownerProperties;
	   var colType = [[]];
	   var fieldObj = {};
	//   var j=0;
	   var divs=[];
	 //  var el = document.getElementById("queryFormId");
	   for(var i=0;i<fieldLength;i++){
		   var field = form.fieldList[i];
		   if(field.key) {
			 //如果查询属性为真转化后显示到查询条件中
			   if(field.queryProperties.showInSearch){
				 	 var str = parent.$.fn.module.covQueryType(field,form.index,true);
				 	divs.push(str);
			   }
			   //如果显示属性为真则生成datagrid显示到对应的表格中
			   if(field.queryProperties.showInGrid){
			   		colType[0].push({field:field.key,width:80,title:field.caption});
			   		var value = parent.$.fn.module.covQueryType(field,form.index,false);
			   		fieldObj[field.key] = value;
			   }
		   }
			//   j++;
		  // }
	   }
   		//alert("length:"+divs.length);
   		divs.push("<div class='easyui-panel  xpstyle' style='width:180px;' collapsible='true' collapsed='true' title='所有者'><input type='text'  value="+module.creator+"></input></div>");
   		colType[0].push({field:"creator",width:80,title:"所有者"});
   		colType[0].push({field:"operating",width:100,title:"操作",formatter:function(value,rec){
			return "<a href='#' onclick="+"javascript:$('#addEditorWin').window('open');"+" >查看</a> <a href='#' onclick="+"javascript:$('#addEditorWin').window('open');"+">修改</a> <a href='#'>删除</a>";
		}});
   	//	var value = covQueryType(field,form.index,false);
   		fieldObj["creator"] = module.creator;
   		
   		
   		
   		
   		$("#queryFormId").html(divs.join(""));
   		$("#queryFormId .easyui-panel").panel({});
	   $("#logInfo").edatagrid({
			width: 1000,
			height: 'auto',
			columns:colType,
			toolbar:[ {
				text:'新增',
				iconCls:'icon-add',
				handler:function(){
					initEditorWin();
				}
			},'-',{
				text:'删除',
				iconCls:'icon-remove'
			} ,'-',{
				text:'复制',
				iconCls:'icon-copy',
				handler:function(){
					$("#addEditorWin").window('open');
				}
			}/*,'-',{
				text:'查看',
				iconCls:'icon-search',
			} ,'-',{
				text:'复制',
				iconCls:'icon-copy',
			},'-',{
				text:'报表',
				iconCls:'icon-report',
			},'-',{
				text:'附件',
				iconCls:'icon-attach',
			} */
			]
		});
	 //  $.each(fieldObj,function(k,v){alert(k+":"+v);})
		$("#logInfo").edatagrid("addRow",fieldObj);
		
   }
</script>
</head>
<body  class="easyui-layout" fit="true">
<!-- <div class="easyui-layout" fit="true"> -->
		<div region="west" title="查询条件" split="true" style="width:200px;padding:0px;background-color: #E0ECFF;" iconCls="icon-search" tools="#pl_tt">
				
					<div class="xpstyle-panel" id="queryFormId">
						
					</div>
				
		</div>
		<div region="center" title="查询显示">
				<table id="logInfo" fit="true">
				</table>
		</div>
	<!-- </div> -->
	<div id="pl_tt">
					<a href="#" id="a_exp_clp" class="icon-expand" title="全部展开"></a>
					<a href="#" id="a_switch_query" class="icon-menu" title="切换查询方式" style="display:none;"></a>
	</div>



	<div id="addEditorWin" class="easyui-window" title="用户详情" style="width:600px;height:400px"  closed=true
	        iconCls='icon-save' >  
	        <div  class="easyui-layout" style="width:580px;height:360px;">  
			    <div region="center" border="false" style="padding:10px;background:#fff;border:1px solid #ccc;">
			    	 <textarea id="useEditor" name="useEditor" style="width:90%;height:100%"></textarea>
			    </div>  
			    <div region="south" border="false" style="text-align:right;padding:5px 0;;">
			    		<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="javascript:$('#addEditorWin').window('close');">保存</a>
						<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="javascript:$('#addEditorWin').window('close');">关闭</a>
			    </div>
			</div> 
   
 </div>
</body>
</html>
