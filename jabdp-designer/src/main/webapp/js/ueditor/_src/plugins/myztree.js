/*使ueditor能够接收从ztree拖动进来的节点*/
UE.plugin.register('ztree', function() {
    return {
        commands: {
            'ztree': {
            	execCommand: function(cmd, node) {
            		var me = this;//ueditor
            		var mynode = node;//从ztree拖入的节点
            		//editor中所有td元素加上mouseover事件
            		var tds = me.document.getElementsByTagName('td');
                	for(var i=0; i<tds.length; i++) {
                		UE.dom.domUtils.on(tds[i],"mouseover",ztreeEvt);
                	}
                	//editor的html元素加上mouseover事件
                	var myueditor_htmls = me.document.getElementsByTagName('html');
                	//如果鼠标停留于heml元素上，将从ztree传入的节点数据释放，移除td元素的事件
                	//目的是当鼠标没拖到td就释放掉时，不会向td中写入节点
                	UE.dom.domUtils.on(myueditor_htmls[0],"mouseover",function(evt) {
                		mynode = null;
                		for(var i=0; i<tds.length; i++) {
            				UE.dom.domUtils.un(tds[i],"mouseover",ztreeEvt);
            	    	}
                	});
                	//当鼠标在td元素上释放，将节点写入对应的td
                	function ztreeEvt(evt) {
                		//ueditor光标插入鼠标下方的td元素
                		var tds = me.document.getElementsByTagName('td');
                		me.selection.getRange().setStartAtFirst(evt.target).setCursor();
                		//在光标位置写入节点
                		editor.execCommand('inserthtml', mynode);
                		//在对应td写入节点之后，将节点数据释放并移除td元素的事件
                		for(var i=0; i<tds.length; i++) {
                			UE.dom.domUtils.un(tds[i],"mouseover",ztreeEvt);
                	    }
                	    mynode = null;
                	}
                }
            }
        }
    }
});

/*在表格相应位置插入节点*/
UE.plugin.register('ztreeauto', function() {
    return {
        commands: {
            'ztreeauto': {
            	execCommand: function(cmd, node) {
            		var me = this;//ueditor
            		var tds = me.document.getElementsByTagName('td');
            		//移动光标
                	me.selection.getRange().setStartAtFirst(tds[node.index]).setCursor();
            		//在光标位置写入节点
            		editor.execCommand('inserthtml',node.html);
                }
            }
        }
    }
});

/*图片宽高复制*/
UE.plugin.register('ztreecopy', function() {
    return {
        commands: {
            'ztreecopy': {
            	execCommand: function(cmd, module) {
            		var me = this;//ueditor
            		$("img", me.document).each(function() {
            			var tableIndex = $(this).attr("formindex");
            			var fieldIndex = $(this).attr("fieldindex");
            			if(tableIndex && fieldIndex && module) {
            				var field = module.getFieldProp(tableIndex, fieldIndex);
            				if(field) {
            					field.editProperties.height = $(this).height();
            					field.editProperties.width = $(this).width();
            					//console.log(tableIndex + "_" + fieldIndex);
            				}
            			}
            		});
            		/*var tds = me.document.getElementsByTagName("td");
            		//console.log(tds);
            		for(var i=0; i<tds.length; i++) {
            			var intds = tds[i].children;
            			//console.log(intds);
            			for(var j=0; j<intds.length; j++) {
//            				console.log(intds[j]);
            				var fieldid = '_' + field.key + '_';
            				if(intds[j].localName == "img" && intds[j].id.indexOf(fieldid) >= 0) {
//            					console.log(intds[j].id);
            					field.editProperties.height = intds[j].offsetHeight - 4;
            					field.editProperties.width = intds[j].offsetWidth - 4;
            				}
            			}
            		}*/
//                    if (ut && ut.selectedTds.length) {
//                    	
//                    }
//                    else {
//                    	var cell = UE.UETable.getTableItemsByRange(this).cell;
//                    	//console.log(cell);
//                    }
//            		var tb = UE.UETable.getUETableBySelected(this).table.children[0];
//                	for(var i=0; i<tb.childElementCount; i++) {
//        				var trs = tb.children[i];
//        				for(var j=0; j<trs.childElementCount; j++) {
//        					var tds = trs.children[j];
//        				}
//        			}
//            		field.editProperties.height = 20;
//            		field.editProperties.width = 30;
                }
            }
        }
    }
});














