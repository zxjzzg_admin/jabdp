/*边框功能
 *
 */

/*上边框*/
UE.plugin.register('topborder', function() {
    return {
        shortcutkey: {
            "topborder": "ctrl+t" //上边框
        },
        commands: {
            'topborder': {
            	execCommand: function() {
                	var ut = UE.UETable.getUETableBySelected(this);
                	//若有多个单元格被同时选中
                    if (ut && ut.selectedTds.length) {
                    	//选区左起位置，单位像素
                    	var colstar = ut.selectedTds[0].offsetLeft;
                    	//选区右末位置，末单元格不一定在最右位置
            			var colend = 0;
            			for(var i=0; i<ut.selectedTds.length; i++) {
                    		if(colend < ut.selectedTds[i].offsetLeft + ut.selectedTds[i].offsetWidth) {
                    			colend = ut.selectedTds[i].offsetLeft + ut.selectedTds[i].offsetWidth;
                    		}
                    	}
            			//选区上起位置
            			var rowstar = ut.selectedTds[0].offsetTop;
                    	for(var i=0; i<ut.selectedTds.length; i++) {
                    		var cell = ut.selectedTds[i];
                    		if(cell.offsetTop==rowstar) {
                    			$(cell).css("border-top-style","solid");
                    			$(cell).css("border-top-width","1px");
                    			$(cell).css("border-top-color","black");
                    		}
                    	}
                    	//修改上边单元格下边框,边框宽度为1px
            			//遍历table所有单元格
                    	var tb = ut.table.children[0];
                    	for(var i=0; i<tb.childElementCount; i++) {
            				var trs = tb.children[i];
            				for(var j=0; j<trs.childElementCount; j++) {
            					var tds = trs.children[j];
            					if(tds.offsetTop + tds.offsetHeight == rowstar
            							&& tds.offsetLeft >= colstar
            							&& tds.offsetLeft + tds.offsetWidth <= colend) {
            						$(tds).css("border-bottom-style","solid");
                        			$(tds).css("border-bottom-width","1px");
                        			$(tds).css("border-bottom-color","black");
            					}
            				}
            			}
                    }
                    //若只有单个单元格被同时选中
                    else {
                    	var cell = UE.UETable.getTableItemsByRange(this).cell;
                    	//ut包括选中td,所在tr与table
                    	var ut = UE.UETable.getTableItemsByRange(this);
                    	var tb = ut.table.children[0];
                    	//修改本单元格上边框
                    	$(cell).css("border-top-style","solid");
                		$(cell).css("border-top-width","1px");
                    	$(cell).css("border-top-color","black");
                    	//修改上方单元格下边框,下边框宽度为1px
                    	var colstar = cell.offsetLeft;
                    	var colend = cell.offsetLeft + cell.offsetWidth;
                    	var rowstar = cell.offsetTop;
                    	for(var i=0; i<tb.childElementCount; i++) {
            				var trs = tb.children[i];
            				for(var j=0; j<trs.childElementCount; j++) {
            					var tds = trs.children[j];
            					if(tds.offsetTop + tds.offsetHeight == rowstar
            							&& tds.offsetLeft >= colstar
            							&& tds.offsetLeft + tds.offsetWidth <= colend) {
            						$(tds).css("border-bottom-style","solid");
                            		$(tds).css("border-bottom-width","1px");
                                	$(tds).css("border-bottom-color","black");
            					}
            				}
            			}
                    }
                }
            }
        }
    }
});

/*下边框*/
UE.plugin.register('bottomborder', function() {
    return {
        shortcutkey: {
            "bottomborder": "ctrl+b" //下边框
        },
        commands: {
            'bottomborder': {
            	execCommand: function() {
                	var ut = UE.UETable.getUETableBySelected(this);
                	if (ut && ut.selectedTds.length) {
                    	var colstar = ut.selectedTds[0].offsetLeft;
            			var colend = 0;
            			for(var i=0; i<ut.selectedTds.length; i++) {
                    		if(colend < ut.selectedTds[i].offsetLeft + ut.selectedTds[i].offsetWidth) {
                    			colend = ut.selectedTds[i].offsetLeft + ut.selectedTds[i].offsetWidth;
                    		}
                    	}
            			var rowend = ut.selectedTds[ut.selectedTds.length-1].offsetTop + ut.selectedTds[ut.selectedTds.length-1].offsetHeight;
                    	for(var i=0; i<ut.selectedTds.length; i++) {
                    		var cell = ut.selectedTds[i];
                    		if(cell.offsetTop + cell.offsetHeight==rowend) {
                    			$(cell).css("border-bottom-style","solid");
                    			$(cell).css("border-bottom-width","1px");
                    			$(cell).css("border-bottom-color","black");
                    		}
                    	}
                    	//修改下边单元格上边框,边框宽度为1px
            			//遍历table所有单元格
                    	var tb = ut.table.children[0];
                    	for(var i=0; i<tb.childElementCount; i++) {
            				var trs = tb.children[i];
            				for(var j=0; j<trs.childElementCount; j++) {
            					var tds = trs.children[j];
            					if(tds.offsetTop == rowend
            							&& tds.offsetLeft >= colstar
            							&& tds.offsetLeft + tds.offsetWidth <= colend) {
            						$(tds).css("border-top-style","solid");
                        			$(tds).css("border-top-width","1px");
                        			$(tds).css("border-top-color","black");
            					}
            				}
            			}
                    }
                	//若只有单个单元格被同时选中
                    else {
                    	var cell = UE.UETable.getTableItemsByRange(this).cell;
                    	//ut包括选中td,所在tr与table
                    	var ut = UE.UETable.getTableItemsByRange(this);
                    	var tb = ut.table.children[0];
                    	//修改本单元格上边框
                    	$(cell).css("border-bottom-style","solid");
                		$(cell).css("border-bottom-width","1px");
                    	$(cell).css("border-bottom-color","black");
                    	//修改上方单元格下边框,下边框宽度为1px
                    	var colstar = cell.offsetLeft;
                    	var colend = cell.offsetLeft + cell.offsetWidth;
                    	var rowend = cell.offsetTop + cell.offsetHeight;
                    	for(var i=0; i<tb.childElementCount; i++) {
            				var trs = tb.children[i];
            				for(var j=0; j<trs.childElementCount; j++) {
            					var tds = trs.children[j];
            					if(tds.offsetTop== rowend
            							&& tds.offsetLeft >= colstar
            							&& tds.offsetLeft + tds.offsetWidth <= colend) {
            						$(tds).css("border-top-style","solid");
                            		$(tds).css("border-top-width","1px");
                                	$(tds).css("border-top-color","black");
            					}
            				}
            			}
                    }
                }
            }
        }
    }
});

/*左边框*/
UE.plugin.register('leftborder', function() {
    return {
        shortcutkey: {
            "leftborder": "ctrl+l" //左边框
        },
        commands: {
            'leftborder': {
            	execCommand: function() {
                	var ut = UE.UETable.getUETableBySelected(this);
                    if (ut && ut.selectedTds.length) {
                    	//offsetLeft单元格左边框距离表格左边的绝对偏移量
                    	var colstar = ut.selectedTds[0].offsetLeft;
            			var rowstar = ut.selectedTds[0].offsetTop;
            			var rowend = ut.selectedTds[ut.selectedTds.length-1].offsetTop + ut.selectedTds[ut.selectedTds.length-1].offsetHeight;
                    	for(var i=0; i<ut.selectedTds.length; i++) {
                    		var cell = ut.selectedTds[i];
                    		if(cell.offsetLeft==colstar) {
                    			$(cell).css("border-left-style","solid");
                    			$(cell).css("border-left-width","1px");
                    			$(cell).css("border-left-color","black");
                    		}
                    	}
                    	var tb = ut.table.children[0];
                    	//修改左边单元格右边框,边框宽度为0px
            			//遍历table所有单元格
            			for(var i=0; i<tb.childElementCount; i++) {
            				var trs = tb.children[i];
            				for(var j=0; j<trs.childElementCount; j++) {
            					var tds = trs.children[j];
            					if(tds.offsetLeft + tds.offsetWidth == colstar
            							&& tds.offsetTop >= rowstar
            							&& tds.offsetTop + tds.offsetHeight <= rowend) {
            						$(tds).css("border-right-style","solid");
                        			$(tds).css("border-right-width","1px");
                        			$(tds).css("border-right-color","black");
            					}
            				}
            			}
                    }
                    else {
                    	var cell = UE.UETable.getTableItemsByRange(this).cell;
                    	var ut = UE.UETable.getTableItemsByRange(this);
                    	var tb = ut.table.children[0];
            			var colstar = cell.offsetLeft;
            			var rowstar = cell.offsetTop;
            			var rowend = cell.offsetTop + cell.offsetHeight;
                    	$(cell).css("border-left-style","solid");
            			$(cell).css("border-left-width","1px");
            			$(cell).css("border-left-color","black");
            			//修改左边单元格右边框,边框宽度为0px
            			//遍历table所有单元格
            			for(var i=0; i<tb.childElementCount; i++) {
            				var trs = tb.children[i];
            				for(var j=0; j<trs.childElementCount; j++) {
            					var tds = trs.children[j];
            					if(tds.offsetLeft + tds.offsetWidth == colstar
            							&& tds.offsetTop >= rowstar
            							&& tds.offsetTop + tds.offsetHeight <= rowend) {
            						$(tds).css("border-right-style","solid");
                        			$(tds).css("border-right-width","1px");
                        			$(tds).css("border-right-color","black");
            					}
            				}
            			}
                    }
                }
            }
        }
    }
});

/*右边框*/
UE.plugin.register('rightborder', function() {
    return {
        shortcutkey: {
            "rightborder": "ctrl+r" //右边框
        },
        commands: {
            'rightborder': {
            	execCommand: function() {
                	var ut = UE.UETable.getUETableBySelected(this);
                    if (ut && ut.selectedTds.length) {
                    	//offsetRight距离表格底边的绝对偏移量
                    	var colend = 0;
                    	for(var i=0; i<ut.selectedTds.length; i++) {
                    		if(colend < ut.selectedTds[i].offsetLeft + ut.selectedTds[i].offsetWidth) {
                    			colend = ut.selectedTds[i].offsetLeft + ut.selectedTds[i].offsetWidth;
                    		}
                    	}
                    	var rowstar = ut.selectedTds[0].offsetTop;
            			var rowend = ut.selectedTds[ut.selectedTds.length-1].offsetTop + ut.selectedTds[ut.selectedTds.length-1].offsetHeight;
            			for(var i=0; i<ut.selectedTds.length; i++) {
                    		var cell = ut.selectedTds[i];
                    		if(cell.offsetLeft + cell.offsetWidth==colend){
                    			$(cell).css("border-right-style","solid");
                    			$(cell).css("border-right-width","1px");
                    			$(cell).css("border-right-color","black");
                    		}
                    	}
            			var tb = ut.table.children[0];
                    	//修改右边单元格左边框,边框宽度为1px
            			//遍历table所有单元格
            			for(var i=0; i<tb.childElementCount; i++) {
            				var trs = tb.children[i];
            				for(var j=0; j<trs.childElementCount; j++) {
            					var tds = trs.children[j];
            					if(tds.offsetLeft == colend
            							&& tds.offsetTop >= rowstar
            							&& tds.offsetTop + tds.offsetHeight <= rowend) {
            						$(tds).css("border-left-style","solid");
                        			$(tds).css("border-left-width","1px");
                        			$(tds).css("border-left-color","black");
            					}
            				}
            			}
                    }
                    else {
                    	var cell = UE.UETable.getTableItemsByRange(this).cell;
                    	var ut = UE.UETable.getTableItemsByRange(this);
                    	var tb = ut.table.children[0];
            			var colend = cell.offsetLeft + cell.offsetWidth;
            			var rowstar = cell.offsetTop;
            			var rowend = cell.offsetTop + cell.offsetHeight;
                    	$(cell).css("border-right-style","solid");
            			$(cell).css("border-right-width","1px");
            			$(cell).css("border-right-color","black");
            			//修改右边单元格左边框,边框宽度为1px
            			//遍历table所有单元格
            			for(var i=0; i<tb.childElementCount; i++) {
            				var trs = tb.children[i];
            				for(var j=0; j<trs.childElementCount; j++) {
            					var tds = trs.children[j];
            					if(tds.offsetLeft == colend
            							&& tds.offsetTop >= rowstar
            							&& tds.offsetTop + tds.offsetHeight <= rowend) {
            						$(tds).css("border-left-style","solid");
                        			$(tds).css("border-left-width","1px");
                        			$(tds).css("border-left-color","black");
            					}
            				}
            			}
                    }
                }
            }
        }
    }
});

/*外边框*/
UE.plugin.register('outborder', function() {
    return {
        shortcutkey: {
            "outborder": "ctrl+o" //外边框
        },
        commands: {
            'outborder': {
            	execCommand: function() {
            		this.execCommand('topborder');
            		this.execCommand('bottomborder');
            		this.execCommand('leftborder');
					this.execCommand('rightborder');
                }
            }
        }
    }
});

/*垂直边框*/
UE.plugin.register('verticalborder', function() {
    return {
        shortcutkey: {
            "verticalborder": "ctrl+v" //垂直边框
        },
        commands: {
            'verticalborder': {
            	execCommand: function() {
            		var ut = UE.UETable.getUETableBySelected(this);
            		var colstar = ut.selectedTds[0].offsetLeft;
            		var colend = 0;
                	for(var i=0; i<ut.selectedTds.length; i++) {
                		if(colend < ut.selectedTds[i].offsetLeft + ut.selectedTds[i].offsetWidth) {
                			colend = ut.selectedTds[i].offsetLeft + ut.selectedTds[i].offsetWidth;
                		}
                	}
                    if(ut && ut.selectedTds.length) {
                    	//内部画左右边框
                    	for(var i=0; i<ut.selectedTds.length; i++) {
                    		var cell = ut.selectedTds[i];
                    		if(cell.offsetLeft != colstar && cell.offsetLeft + cell.offsetWidth != colend) {
                    			$(cell).css("border-left-style","solid");
                    			$(cell).css("border-left-width","1px");
                    			$(cell).css("border-left-color","black");
                    			$(cell).css("border-right-style","solid");
                    			$(cell).css("border-right-width","1px");
                    			$(cell).css("border-right-color","black");
                    		}
                    		if(cell.offsetLeft != colstar) {
                    			$(cell).css("border-left-style","solid");
                    			$(cell).css("border-left-width","1px");
                    			$(cell).css("border-left-color","black");
                    		}
                    		if(cell.offsetLeft + cell.offsetWidth != colend) {
                    			$(cell).css("border-right-style","solid");
                    			$(cell).css("border-right-width","1px");
                    			$(cell).css("border-right-color","black");
                    		}
                    	}
                    }
                }
            }
        }
    }
});

/*水平边框*/
UE.plugin.register('horizontalborder',function(){
    return {
        shortcutkey:{
            "horizontalborder":"ctrl+h" //水平边框
        },
        commands:{
            'horizontalborder':{
            	execCommand : function(){
            		var ut = UE.UETable.getUETableBySelected(this);
            		var rowstar = ut.selectedTds[0].offsetTop;
        			var rowend = ut.selectedTds[ut.selectedTds.length-1].offsetTop + ut.selectedTds[ut.selectedTds.length-1].offsetHeight;
                    if (ut && ut.selectedTds.length) {
                    	//内部画上下边框
                    	for(var i=0; i<ut.selectedTds.length; i++) {
                    		var cell = ut.selectedTds[i];
                    		if(cell.offsetTop != rowstar && cell.offsetTop + cell.offsetHeight != rowend) {
                    			$(cell).css("border-top-style","solid");
                    			$(cell).css("border-top-width","1px");
                    			$(cell).css("border-top-color","black");
                    			$(cell).css("border-bottom-style","solid");
                    			$(cell).css("border-bottom-width","1px");
                    			$(cell).css("border-bottom-color","black");
                    		}
                    		if(cell.offsetTop != rowstar) {
                    			$(cell).css("border-top-style","solid");
                    			$(cell).css("border-top-width","1px");
                    			$(cell).css("border-top-color","black");
                    		}
                    		if(cell.offsetTop + cell.offsetHeight != rowend) {
                    			$(cell).css("border-bottom-style","solid");
                    			$(cell).css("border-bottom-width","1px");
                    			$(cell).css("border-bottom-color","black");
                    		}
                    	}
                    }
                }
            }
        }
    }
});

/*内部边框*/
UE.plugin.register('inborder',function(){
    return {
        shortcutkey:{
            "inborder":"ctrl+i" //内部边框
        },
        commands:{
            'inborder':{
            	execCommand : function(){
            		this.execCommand('verticalborder');
            		this.execCommand('horizontalborder');
                }
            }
        }
    }
});

/*所有边框*/
UE.plugin.register('allborder',function(){
    return {
        shortcutkey:{
            "allborder":"ctrl+a" //所有边框
        },
        commands:{
            'allborder':{
            	execCommand : function(){
            		this.execCommand('outborder');
            		this.execCommand('inborder');
                }
            }
        }
    }
});

/*清除边框*/
UE.plugin.register('clearborder',function(){
    return {
        shortcutkey:{
            "clearborder":"ctrl+c" //清除边框
        },
        commands:{
            'clearborder':{
            	execCommand : function(){
            		var ut = UE.UETable.getUETableBySelected(this);
                    if (ut && ut.selectedTds.length) {
                    	var offsetBottom = ut.table.clientHeight;
                    	var offsetRight = ut.table.clientWidth;
                    	for(var i=0; i<ut.selectedTds.length; i++){
                    		var cell = ut.selectedTds[i];
                    		if(cell.offsetTop + cell.offsetHeight == offsetBottom
                    				&& cell.offsetLeft + cell.offsetWidth == offsetRight) {
                    			$(cell).css("border-left-style","");
        						$(cell).css("border-left-width","");
        						$(cell).css("border-left-color","");
        						$(cell).css("border-right-style","");
        						$(cell).css("border-right-width","");
        						$(cell).css("border-right-color","");
        						$(cell).css("border-top-style","");
        						$(cell).css("border-top-width","");
        						$(cell).css("border-top-color","");
        						$(cell).css("border-bottom-style","");
        						$(cell).css("border-bottom-width","");
        						$(cell).css("border-bottom-color","");
                    		}
                    		else if(cell.offsetTop + cell.offsetHeight == offsetBottom) {
                    			$(cell).css("border-left-style","");
        						$(cell).css("border-left-width","");
        						$(cell).css("border-left-color","");
        						$(cell).css("border-right-style","");
        						$(cell).css("border-right-width","");
        						$(cell).css("border-right-color","");
        						$(cell).css("border-top-style","");
        						$(cell).css("border-top-width","");
        						$(cell).css("border-top-color","");
        						$(cell).css("border-bottom-style","");
        						$(cell).css("border-bottom-width","");
        						$(cell).css("border-bottom-color","");
                    		}
                    		else if(cell.offsetLeft + cell.offsetWidth == offsetRight) {
                    			$(cell).css("border-left-style","");
        						$(cell).css("border-left-width","");
        						$(cell).css("border-left-color","");
        						$(cell).css("border-right-style","");
        						$(cell).css("border-right-width","");
        						$(cell).css("border-right-color","");
        						$(cell).css("border-top-style","");
        						$(cell).css("border-top-width","");
        						$(cell).css("border-top-color","");
        						$(cell).css("border-bottom-style","");
        						$(cell).css("border-bottom-width","");
        						$(cell).css("border-bottom-color","");
                    		}
                    		else {
                    			$(cell).css("border-left-style","");
                    			$(cell).css("border-left-width","");
                    			$(cell).css("border-left-color","");
                    			$(cell).css("border-right-style","");
                    			$(cell).css("border-right-width","");
                    			$(cell).css("border-right-color","");
                    			$(cell).css("border-top-style","");
                    			$(cell).css("border-top-width","");
                    			$(cell).css("border-top-color","");
                    			$(cell).css("border-bottom-style","");
                    			$(cell).css("border-bottom-width","");
                    			$(cell).css("border-bottom-color","");
                    		}	
                    	}
                    	//修改左边单元格右边框与上边单元格下边框,边框宽度为0px
                    	var colstar = ut.selectedTds[0].offsetLeft;
            			var colend = 0;
            			for(var i=0; i<ut.selectedTds.length; i++){
                    		if(colend < ut.selectedTds[i].offsetLeft + ut.selectedTds[i].offsetWidth) {
                    			colend = ut.selectedTds[i].offsetLeft + ut.selectedTds[i].offsetWidth;
                    		}
                    	}
            			var rowstar = ut.selectedTds[0].offsetTop;
            			var rowend = ut.selectedTds[ut.selectedTds.length-1].offsetTop + ut.selectedTds[ut.selectedTds.length-1].offsetHeight;
            			var tb = ut.table.children[0];
            			//遍历table所有单元格
            			for(var i=0; i<tb.childElementCount; i++) {
            				var trs = tb.children[i];
            				for(var j=0; j<trs.childElementCount; j++) {
            					var tds = trs.children[j];
            					if(tds.offsetLeft == colend
            							&& tds.offsetTop >= rowstar
            							&& tds.offsetTop + tds.offsetHeight <= rowend) {
            						$(tds).css("border-left-style","");
            						$(tds).css("border-left-width","");
            						$(tds).css("border-left-color","");
            					}
            					if(tds.offsetLeft + tds.offsetWidth == colstar
            							&& tds.offsetTop >= rowstar
            							&& tds.offsetTop + tds.offsetHeight <= rowend) {
            						$(tds).css("border-right-style","");
            						$(tds).css("border-right-width","");
            						$(tds).css("border-right-color","");
            					}
            					if(tds.offsetTop == rowend
            							&& tds.offsetLeft >= colstar
            							&& tds.offsetLeft + tds.offsetWidth <= colend) {
            						$(tds).css("border-top-style","");
            						$(tds).css("border-top-width","");
            						$(tds).css("border-top-color","");
            					}
            					if(tds.offsetTop + tds.offsetHeight == rowstar
            							&& tds.offsetLeft >= colstar
            							&& tds.offsetLeft + tds.offsetWidth <= colend) {
            						$(tds).css("border-bottom-style","");
            						$(tds).css("border-bottom-width","");
            						$(tds).css("border-bottom-color","");
            					}
            				}
            			}
                    }
                    else{
                    	var cell = UE.UETable.getTableItemsByRange(this).cell;
                    	var tr = UE.UETable.getTableItemsByRange(this).tr;
                    	var tb = UE.UETable.getTableItemsByRange(this).table.children[0];
                    	var offsetBottom = tb.clientHeight;
                    	var offsetRight = tb.clientWidth;
                    	if(cell.offsetTop + cell.offsetHeight == offsetBottom
                				&& cell.offsetLeft + cell.offsetWidth == offsetRight) {
                			$(cell).css("border-left-style","");
    						$(cell).css("border-left-width","");
    						$(cell).css("border-left-color","");
    						$(cell).css("border-right-style","");
    						$(cell).css("border-right-width","");
    						$(cell).css("border-right-color","");
    						$(cell).css("border-top-style","");
    						$(cell).css("border-top-width","");
    						$(cell).css("border-top-color","");
    						$(cell).css("border-bottom-style","");
    						$(cell).css("border-bottom-width","");
    						$(cell).css("border-bottom-color","");
                		}
                    	else if(cell.offsetLeft + cell.offsetWidth == offsetRight) {
                			$(cell).css("border-left-style","");
    						$(cell).css("border-left-width","");
    						$(cell).css("border-left-color","");
    						$(cell).css("border-right-style","");
    						$(cell).css("border-right-width","");
    						$(cell).css("border-right-color","");
    						$(cell).css("border-top-style","");
    						$(cell).css("border-top-width","");
    						$(cell).css("border-top-color","");
    						$(cell).css("border-bottom-style","");
    						$(cell).css("border-bottom-width","");
    						$(cell).css("border-bottom-color","");
                		}
                    	else if(cell.offsetTop + cell.offsetHeight == offsetBottom) {
                			$(cell).css("border-left-style","");
    						$(cell).css("border-left-width","");
    						$(cell).css("border-left-color","");
    						$(cell).css("border-right-style","");
    						$(cell).css("border-right-width","");
    						$(cell).css("border-right-color","");
    						$(cell).css("border-top-style","");
    						$(cell).css("border-top-width","");
    						$(cell).css("border-top-color","");
    						$(cell).css("border-bottom-style","");
    						$(cell).css("border-bottom-width","");
    						$(cell).css("border-bottom-color","");
                		}
                    	else {
                    		$(cell).css("border-left-style","");
                    		$(cell).css("border-left-width","");
                    		$(cell).css("border-left-color","");
                    		$(cell).css("border-right-style","");
                    		$(cell).css("border-right-width","");
							$(cell).css("border-right-color","");
							$(cell).css("border-top-style","");
							$(cell).css("border-top-width","");
							$(cell).css("border-top-color","");
							$(cell).css("border-bottom-style","");
							$(cell).css("border-bottom-width","");
							$(cell).css("border-bottom-color","");
                    	}
                		//修改左边单元格右边框与上边单元格下边框,边框宽度为0px
            			var colstar = cell.offsetLeft;
            			var colend = cell.offsetLeft + cell.offsetWidth;
            			var rowstar = cell.offsetTop;
            			var rowend = cell.offsetTop + cell.offsetHeight;
            			//遍历table所有单元格
            			for(var i=0; i<tb.childElementCount; i++) {
            				var trs = tb.children[i];
            				for(var j=0; j<trs.childElementCount; j++) {
            					var tds = trs.children[j];
            					if(tds.offsetLeft == colend
            							&& tds.offsetTop >= rowstar
            							&& tds.offsetTop + tds.offsetHeight <= rowend) {
            						$(tds).css("border-left-style","");
            						$(tds).css("border-left-width","");
            						$(tds).css("border-left-color","");
            					}
            					if(tds.offsetLeft + tds.offsetWidth == colstar
            							&& tds.offsetTop >= rowstar
            							&& tds.offsetTop + tds.offsetHeight <= rowend) {
            						$(tds).css("border-right-style","");
            						$(tds).css("border-right-width","");
            						$(tds).css("border-right-color","");
            					}
            					if(tds.offsetTop == rowend
            							&& tds.offsetLeft >= colstar
            							&& tds.offsetLeft + tds.offsetWidth <= colend) {
            						$(tds).css("border-top-style","");
            						$(tds).css("border-top-width","");
            						$(tds).css("border-top-color","");
            					}
            					if(tds.offsetTop + tds.offsetHeight == rowstar
            							&& tds.offsetLeft >= colstar
            							&& tds.offsetLeft + tds.offsetWidth <= colend) {
            						$(tds).css("border-bottom-style","");
            						$(tds).css("border-bottom-width","");
            						$(tds).css("border-bottom-color","");
            					}
            				}
            			}
                    }
                }
            }
        }
    }
});