<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<c:if test="${empty LOCALE}">
	<c:set var="LOCALE" scope="session" value="zh_CN"></c:set>
</c:if>
<c:if test="${empty THEME_STYLE}">
	<c:set var="THEME_STYLE" scope="session" value="themes/default"></c:set>
</c:if>
<c:if test="${empty THEME_COLOR}">
	<c:set var="THEME_COLOR" scope="session" value="blue"></c:set>
</c:if>
<c:set var="locale" value="${LOCALE}"/>
<c:set var="themeStyle" value="${THEME_STYLE}"/>
<c:set var="themeColor" value="${THEME_COLOR}"/>
<c:set var="imgPath" value="${ctx}/${themeStyle}/${themeColor}/images"/>
<c:set var="cssPath" value="${ctx}/${themeStyle}/${themeColor}/css"/>
