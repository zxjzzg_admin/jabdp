package com.qyxx.designer.miniweb.dao.account;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.qyxx.designer.miniweb.entity.account.Group;

/**
 * 权限组对象的Dao interface.
 * 
 * @author calvin
 */

public interface GroupDao extends PagingAndSortingRepository<Group, Long>, GroupDaoCustom {
}
