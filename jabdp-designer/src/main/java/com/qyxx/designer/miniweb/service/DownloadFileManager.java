package com.qyxx.designer.miniweb.service;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DownloadFileManager {
	/**
	 * 下载帐套实现类
	 * @param request
	 * @param response
	 * @param contentType 设置编码类型
	 * @param realName  模块文件名称
	 * @param path 模块所在路径
	 * @return null*/
	public static void download(HttpServletRequest request,	HttpServletResponse response, String contentType,
			String realName,String path) throws Exception {
	//	response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		BufferedInputStream bis = null;
		BufferedOutputStream bos = null;
		
	/*	File storeFile = new File(path);
		FileOutputStream  bos= new FileOutputStream(storeFile);
		InputStream bis = new FileInputStream(path);*/
		
		long fileLength = new File(path).length();

		response.setContentType(contentType);
		response.setHeader("Content-disposition", "attachment; filename="
				+ new String(realName.getBytes("utf-8"), "ISO8859-1"));
		response.setHeader("Content-Length", String.valueOf(fileLength));

		bis = new BufferedInputStream(new FileInputStream(path));
		bos = new BufferedOutputStream(response.getOutputStream());
		byte[] buff = new byte[2048];
		int bytesRead;
		while (-1 != (bytesRead = bis.read(buff, 0, buff.length))) {
			bos.write(buff, 0, bytesRead);
		}
		bis.close();
		bos.close();
	}
}
